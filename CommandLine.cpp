/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#include "CommandLine.h"

#include <algorithm>
#include <assert.h>

void CommandLine::OptionBase::registerIn(Parser& parser) {
    if (m_shortName) {
        parser.shortNames.insert(std::make_pair(m_shortName, this));
    }
    parser.longNames.insert(std::make_pair(m_longName, this));
}

template <>
void CommandLine::Option<bool>::parseLong(size_t position, const std::string& argument) {
    std::string arg = argument;
    for (size_t i = 0, S = argument.size(); i < S; ++i) {
        arg[i] = toupper(arg[i]);
    }
    
    if (arg == "0" ||
        arg == "NO" ||
        arg == "FALSE" ||
        arg == "OFF") {

        m_value = false;
        return;
    }

    if (arg == "1" ||
        arg == "YES" ||
        arg == "TRUE" ||
        arg == "ON") {

        m_value = true;
        return;
    }

    throw WrongOption(position, this->m_longName, argument);
}

template <>
void CommandLine::Option<std::string>::parseLong(size_t position, const std::string& argument) {
    m_value = argument;
}

template <typename Type>
void CommandLine::Option<Type>::parseShort(size_t position) {
    assert(!"Shouldn't be here, hey!");
}

template <>
void CommandLine::Option<bool>::parseShort(size_t position) {
    m_value = true;
}

void CommandLine::Parser::parse() {
    //HACK erase all '"', so APITrace works correctly
    {
        std::string& l = const_cast<std::string&> (line);
        l.erase(std::remove(l.begin(), l.end(), '"'), l.end());
    }
    //HACK end

    size_t position = 0, position2, position3;
    size_t size = line.size();

    while (position < size) {
        position = line.find_first_not_of(" \n\t", position);
        if (position == std::string::npos) {
            break;
        }

        if (line[position] != '-' || position == size-1) {
            throw UnknownArgument(position);
        }
        ++position;
        if (line[position] == '-') {
            // Long option.
            if (++position == size) {
                throw UnknownArgument(position-2);
            }

            position2 = line.find_first_of("= \n\t", position);
            if (position2 == std::string::npos) {
                throw EmptyOption(position, line.substr(position));
            }
            if (position2 == position) {
                throw UnknownArgument(position-2);
            }
            std::string longName(line, position, position2-position);

            LongNames::const_iterator it = longNames.find(longName);
            if (it == longNames.cend()) {
                throw UnknownOption(position, longName);
            }

            if (line[position2] == '=') {
                position2 = position2+1;
            } else {
                position2 = line.find_first_not_of(" \n\t", position2);
                if (position2 == std::string::npos) {
                    throw EmptyOption(position, longName);
                }
            }

            position3 = line.find_first_of(" \n\t", position2);
            if (position3 == std::string::npos) {
                position3 = size;
            }
        
            std::string argument(line, position2, position3-position2);
            it->second->parseLong(position, argument);

            position = position3;
            continue;
        }

        // Short option.
        while (true) {
            char opt = line[position];
            ShortNames::const_iterator it = shortNames.find(opt);
            if (it == shortNames.cend()) {
                throw UnknownOption(position, opt);
            }

            it->second->parseShort(position);
            if (++position == size) {
                break;
            }
            if (line[position] == ' ' ||
                line[position] == '\n' ||
                line[position] == '\t') {
                
                break;
            }
        }
    }
}

void CommandLine::ParserException::describeWhere(std::ostream& os) const {
    os << "CommandLine: " << position << ": ";
}

void CommandLine::UnknownArgument::describe(std::ostream& os) const {
    describeWhere(os);
    os << "Unknown argument.";
}

void CommandLine::EmptyOption::describe(std::ostream& os) const {
    describeWhere(os);
    os << "Empty long option: --" << longName << ".";
}

void CommandLine::WrongOption::describe(std::ostream& os) const {
    describeWhere(os);
    os << "Wrong long option --" << longName << " argument: " << argument << ".";
}

void CommandLine::UnknownOption::describe(std::ostream& os) const {
    describeWhere(os);
    if (isShort()) {
        os << "Unknown short option: -" << shortName << ".";
    } else {
        os << "Unknown long option: --" << longName << ".";
    }
}

template class CommandLine::Option<bool>;
template class CommandLine::Option<std::string>;
