/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#ifndef Log_h
#define Log_h

#include <fstream>
#include <sstream>

class Log {
public:
    void init(const char* location);

    class Line : public std::ostringstream {
        friend class Log;

        Log& log;

        Line(Log& log) : log(log) {}

    public:
        ~Line() {
            log.output << str() << std::endl;
        }
    };

    Line operator() () {
        return Line(*this);
    }

private:
    std::ofstream output;
};

extern Log global;

#endif
