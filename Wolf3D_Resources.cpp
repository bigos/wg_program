/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#include "Wolf3D_Resources.h"

#include "FileSystem.h"

#include <string.h>

namespace {

class CarmackDecompress {
public:
    static const uint8_t NearPtrTag = 0xA7;
    static const uint8_t FarPtrTag = 0xA8;

    CarmackDecompress(const uint16_t* _data, uint32_t _length) :
        realLength((*_data)/2), data(_data + 1), length(_length - 2) {

        assert(_length > 2);
        assert((*_data % 2) == 0);
    }

    const uint32_t realLength;

    void decompress(uint16_t* buffer);

private:
    const uint16_t* const data;
    const uint32_t length;
};

void CarmackDecompress::decompress(uint16_t* buffer) {
    uint32_t bufferPos = 0;
    union {
        const uint16_t* dataPtr;
        const uint8_t* dataPtr8;
    };
    dataPtr = data;
    const uint8_t* const dataEnd = dataPtr8 + length;
    while (dataEnd - dataPtr8 >= 2) {
        assert(bufferPos < realLength);

        uint16_t word = *dataPtr++;
        if ((word & 0xFF00) == (NearPtrTag << 8)) {
            unsigned count = word & 0xFF;
            if (count > 0) {
                assert(dataPtr8 < dataEnd);
                unsigned dist = *dataPtr8++;
                assert(dist > 0);
                assert(dist <= bufferPos);
                assert(bufferPos + count <= realLength);

                memmove(buffer + bufferPos, buffer + bufferPos - dist, 2*count);
                bufferPos += count;
                continue;
            }
            assert(dataPtr8 < dataEnd);
            word = (word & 0xFF00) | (*dataPtr8++);
        } else if ((word & 0xFF00) == (FarPtrTag << 8)) {
            unsigned count = word & 0xFF;
            if (count > 0) {
                assert(dataPtr8 + 1 < dataEnd);
                unsigned off = *dataPtr++;
                assert(off < bufferPos);
                assert(bufferPos + count <= realLength);

                memmove(buffer + bufferPos, buffer + off, 2*count);
                bufferPos += count;
                continue;
            }
            assert(dataPtr8 < dataEnd);
            word = (word & 0xFF00) | (*dataPtr8++);
        }

        buffer[bufferPos++] = word;
    }

    assert(dataPtr8 == dataEnd);
    assert(bufferPos == realLength);
}

class RLEWDecompress {
public:
    RLEWDecompress(const uint16_t* _data, uint32_t _length, uint16_t magicWord) :
        realLength((*_data)/2), data(_data + 1), length((_length/2) - 1),
        magicWord(magicWord) {

        assert(_length > 2);
        assert((_length % 2) == 0);
        assert((*_data % 2) == 0);
    }

    const uint32_t realLength;

    void decompress(uint16_t* buffer);

private:
    const uint16_t* const data;
    const uint32_t length;
    const uint16_t magicWord;
};

void RLEWDecompress::decompress(uint16_t* buffer) {
    uint32_t bufferPos = 0;
    const uint16_t* dataPtr = data;
    const uint16_t* const dataEnd = dataPtr + length;
    while (dataEnd - dataPtr >= 1) {
        assert(bufferPos < realLength);

        uint16_t word = *dataPtr++;
        if (word == magicWord) {
            assert(dataEnd - dataPtr >= 2);

            unsigned count = *dataPtr++;
            assert(count > 0);
            assert(bufferPos + count <= realLength);

            word = *dataPtr++;

            for (unsigned i = 0; i < count; ++i) {
                buffer[bufferPos++] = word;
            }
            continue;
        }

        buffer[bufferPos++] = word;
    }

    assert(dataPtr == dataEnd);
    assert(bufferPos == realLength);
}

class HuffmanDecompress {
public:
    static const uint8_t RootNode = 0xFE;
    struct Node {
        uint16_t bit0, bit1;

        bool ends0() const { return bit0 < 256; }
        bool ends1() const { return bit1 < 256; }

        uint8_t node0() const { return bit0 - 256; }
        uint8_t node1() const { return bit1 - 256; }
        
        uint8_t byte0() const { return uint8_t(bit0); }
        uint8_t byte1() const { return uint8_t(bit1); }
    };

    HuffmanDecompress(const uint8_t* data, uint32_t length,
                      uint32_t realLength, Node dictionary[255]) :
        realLength(realLength), data(data), dictionary(dictionary),
        length(length), cache(nullptr) {

        populateCache();
    }

    ~HuffmanDecompress() {
        delete[] cache;
    }

    void decompress(uint8_t* buffer);
    
    const uint32_t realLength;

private:
    const uint8_t* const data;
    Node* dictionary;
    const uint32_t length;

    struct CacheEntry {
        uint8_t byte;
        uint8_t bitsUsed;

        bool isComplete() const { return bitsUsed > 0; }
    };

    static const unsigned CacheBits = 12;
    static const unsigned CacheSize = (1 << CacheBits);

    CacheEntry* cache;

    void populateCache();
    void cacheNode(uint8_t num, unsigned offset, unsigned bits);

    class BitReader {
    public:
        BitReader(const uint8_t* data, uint32_t length) :
            buffer(0), data(data), length(length),
            pos(0), bufferBits(0) {
            
            fillBuffer();
        }
        
        uint32_t buffer;

        uint32_t peek(unsigned bits) const;
        uint32_t peekAtMost(unsigned bits) const;
        uint32_t eat(unsigned bits);

        bool empty() const {
            return pos == length;
        }

    private:
        const uint8_t* const data;
        const uint32_t length;
        uint32_t pos;
        unsigned bufferBits;

        void fillBuffer();
    };
};

void HuffmanDecompress::populateCache() {
    //TODO check for circular dependency in dictionary.

    cache = new CacheEntry[CacheSize];
    memset(cache, 0, CacheSize * sizeof(CacheEntry));
    cacheNode(RootNode, 0, 0);
}

void HuffmanDecompress::cacheNode(uint8_t num, unsigned offset, unsigned bits) {
    if (bits == CacheBits) {
        cache[offset].byte = num;
        return;
    }

    const unsigned halfSize = (1 << (CacheBits - 1 - bits));

    if (dictionary[num].ends0()) {
        for (unsigned i = 0; i < halfSize; ++i) {
            cache[offset+i].byte = dictionary[num].byte0();
            cache[offset+i].bitsUsed = bits + 1;
        }
    } else {
        cacheNode(dictionary[num].node0(), offset, bits + 1);
    }

    offset += halfSize;
    if (dictionary[num].ends1()) {
        for (unsigned i = 0; i < halfSize; ++i) {
            cache[offset+i].byte = dictionary[num].byte1();
            cache[offset+i].bitsUsed = bits + 1;
        }
    } else {
        cacheNode(dictionary[num].node1(), offset, bits + 1);
    }
}

void HuffmanDecompress::BitReader::fillBuffer() {
    while (bufferBits <= 24) {
        if (empty()) {
            return;
        }

        unsigned byte = data[pos++];
        buffer |= (byte << (24 - bufferBits));
        bufferBits += 8;
    }
}

uint32_t HuffmanDecompress::BitReader::peek(unsigned bits) const {
    assert(bits <= bufferBits);
    return (buffer >> (32 - bits));
}

uint32_t HuffmanDecompress::BitReader::peekAtMost(unsigned bits) const {
    return (buffer >> (32 - bits));
}

uint32_t HuffmanDecompress::BitReader::eat(unsigned bits) {
    uint32_t ret = peek(bits);
    buffer <<= bits;
    bufferBits -= bits;
    fillBuffer();

    return ret;
}

void HuffmanDecompress::decompress(uint8_t* buffer) {
    BitReader reader(data, length);
    uint32_t bufferPos = 0;

    while (bufferPos < realLength) {
        uint32_t val = reader.peekAtMost(CacheBits);
        assert(val < CacheSize);

        const CacheEntry& c = cache[val];
        if (c.isComplete()) {
            buffer[bufferPos++] = c.byte;
            reader.eat(c.bitsUsed);
            continue;
        }
        reader.eat(CacheBits);

        Node* node = &dictionary[c.byte];
        
        while (true) {
            unsigned i;
            unsigned mask = 0x80;
            val = reader.peekAtMost(8);
            for (i = 0; i < 8; ++i) {
                if (val & mask) {
                    if (node->ends0()) {
                        reader.eat(i+1);
                        buffer[bufferPos++] = node->byte0();
                        break;
                    }
                    node = &dictionary[node->node0()];
                } else {
                    if (node->ends1()) {
                        reader.eat(i+1);
                        buffer[bufferPos++] = node->byte1();
                        break;
                    }
                    node = &dictionary[node->node1()];
                }

                mask >>= 1;
            }
            if (i < 8) {
                break;
            }
            reader.eat(8);
        }
    }

    assert(reader.empty());
}

static std::string Wolf3D_Name(const char* name) {
    return fs.root() + "Wolf3D/" + name;
}

} // end of anynomous

void Wolf3D::Palette::init() {
    const std::string file = readFile(Wolf3D_Name("Wolfenstein3D.pal"));
    assert(file.size() == 256 * 3);
    m_colors.resize(256);
    for (unsigned i = 0; i < 256; ++i) {
        uint32_t color = (uint8_t(file[3*i+0]) << 24) |
                         (uint8_t(file[3*i+1]) << 16) |
                         (uint8_t(file[3*i+2]) << 8 ) |
                         (uint8_t(0xFF)        << 0);
        m_colors[i] = color;
    }
}

void Wolf3D::Palette::clear() {
    m_colors.clear();
}

Wolf3D::Map::~Map() {
    for (unsigned i = 0, S = planes.size(); i < S; ++i) {
        delete[] planes[i];
    }
}

void Wolf3D::Map::loadPlane(unsigned num, Tile* plane) {
    assert(num < planes.size());
    if (planes[num]) {
        delete[] planes[num];
    }
    planes[num] = plane;
}

Wolf3D::TexturePosition<Wolf3D::WallCatalog> Wolf3D::Map::Wall::tpos() const {
    assert(isWall());
    return (tile - 1)*2;
}

Wolf3D::TexturePosition<Wolf3D::DoorCatalog> Wolf3D::Map::Wall::doorTpos() const {
    assert(isDoor());
    switch (tile) {
    case 90:
    case 91:
        return 0;
    case 92:
    case 93:
    case 94:
    case 95:
        return 4;
    case 100:
    case 101:
        return 6;
    default:
        assert(!"Impossible");
        return 0;
    }
}

Wolf3D::Orientation Wolf3D::Map::Wall::doorOrientation() const {
    assert(isDoor());
    switch (tile & 1) {
    case 0:
        return FaceWest;
    case 1:
        return FaceNorth;
    default:
        assert(!"LOL? MSVC IS SHIT!!!");
        return FaceNone;
    }
}

Wolf3D::Orientation Wolf3D::Map::Object::spawnOrientation() const {
    switch (tile) {
    case 19:
        return FaceNorth;
    case 20:
        return FaceEast;
    case 21:
        return FaceSouth;
    case 22:
        return FaceWest;
    default:
        assert(!"Impossible");
        return FaceNone;
    }
}

void Wolf3D::GameMaps::init() {
    m_maps.clear();
    m_maps.resize(100);

    loadHead();

    gameMaps = readFile(Wolf3D_Name("GAMEMAPS.WL1"));

    for (unsigned level = 0; level < 100; ++level) {
        if (pointers[level]) {
            loadMap(m_maps[level], pointers[level]);
        }
    }

    gameMaps.clear();
}

void Wolf3D::GameMaps::clear() {
    m_maps.clear();
}

static const unsigned NumPlanes = 3;

void Wolf3D::GameMaps::loadHead() {
    const std::string head = readFile(Wolf3D_Name("MAPHEAD.WL1"));
    assert(head.size() == 2 + 100*4);
    rlewMagic = *(const uint16_t*)(&head[0]);
    for (unsigned i = 0; i < 100; ++i) {
        pointers[i] = *(const uint32_t*)(&head[2 + 4*i]);
    }
}

void Wolf3D::GameMaps::loadMap(Map& map, uint32_t pointer) {
    struct MapHeader {
        uint32_t planeOffsets[NumPlanes];
        uint16_t planeLengths[NumPlanes];
        uint16_t width, height;
        char name[16];
    };
    
    assert(pointer + sizeof(MapHeader) <= gameMaps.size());

    const MapHeader* header = (const MapHeader*)(&gameMaps[pointer]);
    assert(header->width == 64);
    assert(header->height == 64);
    assert(header->name[15] == '\0');

    map = Map(header->width, header->height, NumPlanes);

    for (unsigned i = 0; i < NumPlanes; ++i) {
        Map::Tile* plane = loadPlane(header->planeOffsets[i], header->planeLengths[i]);
        map.loadPlane(i, plane);
    }
}

Wolf3D::Map::Tile* Wolf3D::GameMaps::loadPlane(uint32_t pointer, uint32_t length) {
    assert(pointer + length <= gameMaps.size());
    uint16_t* half;
    uint16_t* plane;

    {
        CarmackDecompress carmack((const uint16_t*)&gameMaps[pointer], length);
        length = carmack.realLength*2;
        half = new uint16_t[length/2];
        carmack.decompress(half);
    }
    {
        RLEWDecompress rlew(half, length, rlewMagic);
        length = rlew.realLength*2;
        assert(length == 64*64*2);
        plane = new uint16_t[length/2];
        rlew.decompress(plane);
    }
    delete[] half;

    return (Map::Tile*)plane;
}

namespace Wolf3D {
    struct SpriteStruct {
        uint16_t left, right;
        uint16_t dataOffsets[64];
    };

    struct SpriteSpan {
        uint16_t lastY;
        uint16_t pointer;
        uint16_t firstY;
    };
        
    static_assert(sizeof(SpriteSpan) == 6, ":(");
}

void Wolf3D::Graphics::Texture::loadTexture(const uint8_t* data) {
    if (!m_data) {
        m_data = new Color[Width*Height];
    }

    for (unsigned y = 0; y < Height; ++y) {
        for (unsigned x = 0; x < Width; ++x) {
            m_data[y*Width+x] = Color(data[x*Height + (Height - 1 - y)]);
        }
    }
}

void Wolf3D::Graphics::Texture::resolve(uint32_t* buffer, uint32_t stride, const Palette& palette) const {
    if (!m_data) {
        return;
    }
    Color* d = m_data;
    for (unsigned i = 0; i < Height; ++i) {
        for (unsigned j = 0; j < Width; ++j) {
            buffer[j] = palette.resolve(d[j]);
        }
        buffer += stride;
        d += Width;
    }
}

void Wolf3D::Graphics::Sprite::loadSprite(const SpriteStruct* head) {
    if (!m_data) {
        m_data = new Color[Width*Height];
    }
    if (!m_mask) {
        m_mask = new uint8_t[Width*Height];
        memset(m_mask, 0, Width*Height);
    }

    assert(head->left < Width);
    assert(head->right < Width);
    assert(head->left <= head->right);

    for (unsigned col = head->left; col <= head->right; ++col) {
        const SpriteSpan* span = (const SpriteSpan*)(&(((const uint8_t*)head)[head->dataOffsets[col - head->left]]));

        for (; span->lastY; ++span) {
            unsigned lastY = span->lastY >> 1;
            unsigned firstY = span->firstY >> 1;
            const uint8_t* pointer = &((const uint8_t*)head)[span->pointer];

            for (unsigned row = firstY; row < lastY; ++row) {
                m_mask[(Height - 1 - row)*Width + col] = 0xFF;
                m_data[(Height - 1 - row)*Width + col] = Color(pointer[row]);
            }
        }
    }
}

void Wolf3D::Graphics::Sprite::resolve(uint32_t* buffer, uint32_t stride, const Palette& palette) const {
    if (!m_data) {
        return;
    }
    Color* d = m_data;
    uint8_t* m = m_mask;
    for (unsigned i = 0; i < Height; ++i) {
        for (unsigned j = 0; j < Width; ++j) {
            if (m[j]) {
                buffer[j] = palette.resolve(d[j]);
            } else {
                buffer[j] = 0x00000000;
            }
        }
        buffer += stride;
        d += Width;
        m += Width;
    }
}

void Wolf3D::Graphics::init() {
    m_textures.clear();
    m_sprites.clear();

    loadHead();

    for (unsigned i = 0; i < spritesOffset; ++i) {
        if (pointers[i]) {
            m_textures[i].loadTexture((const uint8_t*)&vswap[pointers[i]]);
        }
    }

    for (unsigned i = spritesOffset; i < soundsOffset; ++i) {
        if (pointers[i]) {
            m_sprites[i - spritesOffset].loadSprite((const SpriteStruct*)&vswap[pointers[i]]);
        }
    }

    vswap.clear();
}

void Wolf3D::Graphics::clear() {
    m_textures.clear();
    m_sprites.clear();
    delete[] pointers;
}

void Wolf3D::Graphics::loadHead() {
    vswap = readFile(Wolf3D_Name("VSWAP.WL1"));
    assert(vswap.size() > 6);
    chunks = *(const uint16_t*)(&vswap[0]);
    assert(chunks > 0);
    spritesOffset = *(const uint16_t*)(&vswap[2]);
    assert(chunks > spritesOffset);
    soundsOffset = *(const uint16_t*)(&vswap[4]);
    assert(soundsOffset > spritesOffset);
    assert(chunks > soundsOffset);

    pointers = new uint32_t[chunks];

    for (unsigned i = 0; i < chunks; ++i) {
        pointers[i] = *(const uint32_t*)(&vswap[6 + 4*i]);
    }

    m_textures.resize(spritesOffset);
    m_sprites.resize(soundsOffset - spritesOffset);
}

void Wolf3D::WallCatalog::init(const Graphics& g, const Palette& p) {
    uint32_t* buffer = (uint32_t*)malloc(Width*(MaxX+1)*Height*(MaxY+1) * 4);

    for (unsigned y = 0; y <= MaxY; ++y) {
        for (unsigned x = 0; x <= MaxX; ++x) {
            uint32_t* tile = buffer + Width*(MaxX+1)*Height*y + Width*x;

            const Graphics::Texture& gt = g.texture(y*(MaxX+1)+x);
            gt.resolve(tile, Width*(MaxX+1), p);
        }
    }

    {
        using namespace Texture;
        Bind2D tex(m_texture);
        Data data(GL_RGBA, GL_UNSIGNED_INT_8_8_8_8, buffer);
        tex.allocate(Size<2>(Width*(MaxX+1), Height*(MaxY+1)), GL_RGBA, data);

        tex.setMinFilter(GL_NEAREST);
        tex.setMagFilter(GL_NEAREST);
    }
    free(buffer);
}

void Wolf3D::SpriteCatalog::init(const Graphics& g, const Palette& p) {
    uint32_t* buffer = (uint32_t*)malloc(Width*(MaxX+1)*Height*(MaxY+1) * 4);

    for (unsigned y = 0; y <= MaxY; ++y) {
        for (unsigned x = 0; x <= MaxX; ++x) {
            uint32_t* tile = buffer + Width*(MaxX+1)*Height*y + Width*x;

            if (y < 11 || (y == 11 && x <= 11)) {
                const Graphics::Sprite& gs = g.sprite(y*(MaxX+1)+x);
                gs.resolve(tile, Width*(MaxX+1), p);
            } else if (y == 11 && x > 11) {
                // Nothing.
            } else if (y == 12 && x <= 11) {
                const Graphics::Sprite& gs = g.sprite(296+x);
                gs.resolve(tile, Width*(MaxX+1), p);
            } else if (y == 12 && x == 11) {
                // Nothing.
            } else if (y == 12 && x >= 12) {
                const Graphics::Sprite& gs = g.sprite(416+(x-12)*5);
                gs.resolve(tile, Width*(MaxX+1), p);
            } else if (y == 13 && x < 4) {
                const Graphics::Sprite& gs = g.sprite(417+x);
                gs.resolve(tile, Width*(MaxX+1), p);
            } else if (y == 13 && x < 8) {
                const Graphics::Sprite& gs = g.sprite(422+(x-4));
                gs.resolve(tile, Width*(MaxX+1), p);
            } else if (y == 13 && x < 12) {
                const Graphics::Sprite& gs = g.sprite(427+(x-8));
                gs.resolve(tile, Width*(MaxX+1), p);
            } else if (y == 13 && x < 16) {
                const Graphics::Sprite& gs = g.sprite(432+(x-12));
                gs.resolve(tile, Width*(MaxX+1), p);
            } else if (y == 14 && x < 8) {
                const Graphics::Sprite& gs = g.sprite(408+x);
                gs.resolve(tile, Width*(MaxX+1), p);
            } else {
                // Nothing.
            }
        }
    }

    {
        using namespace Texture;
        Bind2D tex(m_texture);
        Data data(GL_RGBA, GL_UNSIGNED_INT_8_8_8_8, buffer);
        tex.allocate(Size<2>(Width*(MaxX+1), Height*(MaxY+1)), GL_RGBA, data);

        tex.setMinFilter(GL_NEAREST);
        tex.setMagFilter(GL_NEAREST);
    }
    free(buffer);
}

void Wolf3D::DoorCatalog::init(const Graphics& g, const Palette& p) {
    uint32_t* buffer = (uint32_t*)malloc(Width*(MaxX+1)*Height*(MaxY+1) * 4);

    for (unsigned y = 0; y <= MaxY; ++y) {
        for (unsigned x = 0; x <= MaxX; ++x) {
            uint32_t* tile = buffer + Width*(MaxX+1)*Height*y + Width*x;

            const Graphics::Texture& gt = g.texture(98 + y*(MaxX+1)+x);
            gt.resolve(tile, Width*(MaxX+1), p);
        }
    }

    {
        using namespace Texture;
        Bind2D tex(m_texture);
        Data data(GL_RGBA, GL_UNSIGNED_INT_8_8_8_8, buffer);
        tex.allocate(Size<2>(Width*(MaxX+1), Height*(MaxY+1)), GL_RGBA, data);

        tex.setMinFilter(GL_NEAREST);
        tex.setMagFilter(GL_NEAREST);
    }
    free(buffer);
}
