/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#include "Vertex.h"

void Vertex::Array::destroy() {
    assert(m_array);
    glDeleteVertexArrays(1, &m_array);
}

void Vertex::Array::bind() {
    create();
    glBindVertexArray(m_array);
}

void Vertex::Array::unbind() {
    glBindVertexArray(0);
}

void Vertex::Array::create() {
    if (!m_array) {
        glGenVertexArrays(1, &m_array);
    }
}

void Vertex::Attribute::enable() {
    glEnableVertexAttribArray(m_index);
}

void Vertex::Attribute::disable() {
    glDisableVertexAttribArray(m_index);
}

void Vertex::Attribute::setPointer(GLenum type, bool normalized, int size,
                                   ptrdiff_t stride, Buffer::Buffer buffer, size_t offset) {
    Buffer::Array a(buffer);
    setPointer(type, normalized, size, stride, offset);
}

void Vertex::Attribute::setPointer(GLenum type, bool normalized, int size,
                                   ptrdiff_t stride, size_t offset) {
    glVertexAttribPointer(m_index, size, type, normalized, (int)stride, (char*)0 + offset);
}

void Vertex::Attribute::setIPointer(GLenum type, int size, ptrdiff_t stride,
                                    Buffer::Buffer buffer, size_t offset) {
    Buffer::Array a(buffer);
    setIPointer(type, size, stride, offset);
}

void Vertex::Attribute::setIPointer(GLenum type, int size, ptrdiff_t stride, size_t offset) {
    glVertexAttribIPointer(m_index, size, type, (int)stride, (char*)0 + offset);
}

void Vertex::Attribute::setDivisor(GLuint div) {
    glVertexAttribDivisor(m_index, div);
}

void Vertex::Bind::setPrimitiveRestartIndex(GLuint index) {
    glPrimitiveRestartIndex(index);
}

void Vertex::Bind::enable(GLuint index) {
    glEnableVertexAttribArray(index);
}

void Vertex::Bind::disable(GLuint index) {
    glDisableVertexAttribArray(index);
}
