/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#ifndef Z1_18_03_h
#define Z1_18_03_h

#include "Application.h"
#include "Buffer.h"
#include "GL.h"
#include "Matrix.h"
#include "Scene.h"
#include "Shader.h"
#include "Vertex.h"

class Z1_18_03 : public Scene {
public:
    Z1_18_03() : Scene("Z1 18.03.2013"), nextScene(0) {}

    virtual void load(Application&);
    virtual void unload(Application&);
    virtual void activate(Application&);
    virtual void suspend(Application&);
    virtual void resized(Application&);
    virtual void keyPressed(Application&, int which);
    virtual void keyReleased(Application&, int which);
    virtual void update(Application&, long diff);
    virtual void repaint(Application&);

    Scene* nextScene;

private:
    float cameraX, cameraY, cameraScale;
    Matrix<float, 4, 4> projectionMatrix;
    void computeProjectionMatrix();

    class FlatColorProgram {
    public:
        void init(bool interpolate);
        
        Shader::Defines defines;
        Shader::Object vs, fs;
        Shader::Program p;
        Shader::Uniform transformMatrix;

        static const GLuint position = 0;
        static const GLuint color = 1;
    } flatColor, interpolatedColor;

    bool interpolate;
    FlatColorProgram* program;
    void chooseProgram();

    class CirclePrimitive {
    public:
        void init();
        void draw();

        Vertex::Array a;
        Buffer::Buffer b;
        GLenum mode;
        
        Matrix<float, 4, 4> modelMatrix;
        float x, y;
        float rotX, rotY, rotZ;
        void computeModelMatrix();

        static const size_t Size = 16;
    } circle;

    class Axis {
    public:
        void init(float x, float y);
        void draw();
        
        Vertex::Array a;
        Buffer::Buffer b;

        static const size_t Size = 6;
    } xAxis, yAxis;
};

#endif
