/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#ifndef GL_h
#define GL_h

#include "Exceptions.h"
#include "System.h"
#include "GL/glew.h"
#include "GL/wglew.h"

class GlewError : public Exception {
public:
    GlewError(GLenum err) :
        Exception("GLEW Error"),
        error(err) {}

    virtual void describe(std::ostream& os) const throw();

    const GLenum error;
};

#define M_PI 3.14159265359f

#endif
