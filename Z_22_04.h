/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#ifndef Z_22_04_h
#define Z_22_04_h

#include "Application.h"
#include "Camera.h"
#include "GL.h"
#include "Matrix.h"
#include "Scene.h"

class Z_22_04 : public Scene {
public:
    enum DragType {
        DragNone,
        DragRotation,
        DragPosition
    };

    class Camera : public ::Camera {
    public:
        void init();
        
        const Matrix<float, 4, 4>& getProjectionMatrix() const { return projectionMatrix; }
        const Matrix<float, 4, 4>& getViewMatrix() const { return viewMatrix; }

        void recomputeMatrices();
        
        void rotateX(float degrees);
        void rotateY(float degrees);
        void zoomZ(float meters);
        
        float getRotX() const { return rotX; }
        float getRotY() const { return rotY; }
        float getZoomZ() const { return trZ; }

    private:
        float scale, rotX, rotY, trZ;
    };

    class Light {
    public:
        void init(int number);
        void updateLight();
        
        void rotateX(float degrees);
        void rotateY(float degrees);
        void zoomZ(float meters);

        int number;
        float rotX, rotY, trZ;
        Matrix<float, 1, 3> attenuation;
        Matrix<float, 1, 4> ambient, diffuse, specular;
        float spotCutoff, spotExponent;
    };

public:
    class Spheric {
    public:
        void init(float radius) {
            slices = stacks = 8;
            this->radius = radius;
        }

        Matrix<float, 1, 3> spheric(float alpha, float betha) const {
            return spheric(alpha, betha, radius);
        }

        static Matrix<float, 1, 3> spheric(float alpha, float betha, float radius) {
            float ca = cos(alpha), cb = cos(betha);
            float sa = sin(alpha), sb = sin(betha);
            return CMatrix<float, 1, 3>::create(
                radius*ca*cb,
                radius*sb,
                radius*sa*cb
            );
        }

        float slices, stacks, radius;
    };

    class Cylindric {
    public:
        void init(float radius, float height) {
            slices = stacks = 8;
            this->radius = radius;
            this->height = height;
        }

        static Matrix<float, 1, 3> cylindric(float alpha, float radius, float height) {
            float c = cos(alpha), s = sin(alpha);
            return CMatrix<float, 1, 3>::create(
                radius*c,
                height,
                radius*s
            );
        }

        float slices, stacks, radius, height;
    };

    class Floor {
    public:
        void init(const Matrix<float, 1, 3>& color, float width) {
            this->color = color;
            this->width = width;
        }
            
        Matrix<float, 1, 3> color;
        float width;
    };

    class Sphere : public Spheric {
    public:
        void init(const Matrix<float, 1, 3>& color, float radius) {
            Spheric::init(radius);
            this->color = color;
            selected = false;
        }

        Matrix<float, 1, 3> color;
        bool selected;
    };

    class Cylinder : public Cylindric {
    public:
        void init(const Matrix<float, 1, 3>& color, float radius, float height) {
            Cylindric::init(radius, height);
            this->color = color;
            selected = false;
        }
        
        Matrix<float, 1, 3> color;
        bool selected;
    };

    class Cone : public Cylindric {
    public:
        void init(const Matrix<float, 1, 3>& color, float radius, float height) {
            Cylindric::init(radius, height);
            this->color = color;
            selected = false;
        }
        
        Matrix<float, 1, 3> color;
        bool selected;
    };

    class Cube {
    public:
        void init(const Matrix<float, 1, 3>& color, float size) {
            this->color = color;
            this->size = size;
            slices = 8;
            selected = false;
        }
        
        Matrix<float, 1, 3> color;
        float slices, size;
        bool selected;
    };

    class HUD {
    public:
        Camera* camera;

        void init(Camera* camera) { this->camera = camera; }
    };

    struct Objects {
        enum Names {
            SphereName = 1,
            CylinderName,
            ConeName,
            CubeName
        };

        Floor floor;
        Sphere sphere;
        Cylinder cylinder;
        Cone cone;
        Cube cube;
        HUD hud;
        Light* light;
        
        Matrix<float, 1, 4> ambient, diffuse, specular;
        float shininess;

        void init(Light* light, Camera* camera);
        void select(Names name);
        void invertSelection(Names name);
        void deselectAll();
    };

    Z_22_04() : Scene("Z 22.04.2013") {}

    virtual void load(Application&);
    virtual void unload(Application&);
    virtual void activate(Application&);
    virtual void suspend(Application&);
    virtual void resized(Application&);
    virtual void keyPressed(Application&, int which);
    virtual void keyReleased(Application&, int which);
    virtual void mouseButtonPressed(Application&, int which, int x, int y);
    virtual void mouseButtonReleased(Application&, int which, int x, int y);
    virtual void mouseMoved(Application&, int x, int y);
    virtual void update(Application&, long diff);
    virtual void repaint(Application&);

private:
    Camera camera;
    Light light;
    Objects objects;
    Renderer<Objects>* renderer;
    DragType dragType;
    Matrix<int, 1, 2> dragPosition;
    bool dragSnapped;

    void mouseRotate(int x, int y);
    void mouseZoom(int x, int y);
    void mouseClick(int x, int y);
};

template <>
class Renderer<Z_22_04::Objects> {
public:
    struct Context {
        static const size_t BufferSize = 512;

        Camera* camera;
        bool select;
        Matrix<float, 4, 4> selectMatrix;
        GLuint buffer[BufferSize];
        GLint hits;
    };

    virtual ~Renderer() {};

    virtual void init(const Z_22_04::Objects* object, size_t num = 1) = 0;
    virtual void draw(Context& context) = 0;
};

#endif
