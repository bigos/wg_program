/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#ifndef Matrix_h
#define Matrix_h

#include <assert.h>
#include <cmath>
#include <string.h>

// Data is stored in column-major order.

template <typename Scalar, size_t W, size_t H>
class Matrix;

template <typename _Scalar, size_t W, size_t H>
class Transpose {
public:
    typedef _Scalar Scalar;

private:
    static_assert(W >= 1 && W <= 4, "Wrong width");
    static_assert(H >= 1 && H <= 4, "Wrong height");
    static_assert(W > 1 || H > 1, "Scalars are not supported");
    
    typedef const Matrix<Scalar, W, H> M;
public:
    Transpose(M& m) : link(m) {}
    
    const Scalar& operator() (size_t x, size_t y) const;
    
    const Scalar& operator[] (size_t i) const;
    
    // NOTE This operator means to transpose.
    M& operator* () const { return link; }
    
    M& link;
};

template <typename _Scalar, size_t W, size_t H>
class Matrix {
public:
    typedef _Scalar Scalar;

private:
    static_assert(W >= 1 && W <= 4, "Wrong width");
    static_assert(H >= 1 && H <= 4, "Wrong height");
    static_assert(W > 1 || H > 1, "Scalars are not supported");
    
    typedef Transpose<Scalar, W, H> T;
    typedef Transpose<Scalar, H, W> Tran;
public:
    Matrix();
    Matrix(const Scalar& s);
    Matrix(const Scalar* m);
    
    Scalar& operator() (size_t x, size_t y);
    const Scalar& operator() (size_t x, size_t y) const;

    Scalar& operator[] (size_t i);
    const Scalar& operator[] (size_t i) const;
    
    // NOTE This operator means to transpose.
    T operator* () const { return T(*this); }
    
    Scalar data[W*H];
    
// ----------------------------------------------------------------------------

    Matrix& operator+= (const Matrix& m) {
        for (size_t i = 0; i < W*H; ++i) {
            data[i] += m.data[i];
        }
        return *this;
    }

    Matrix& operator+= (const Tran& m) {
        for (size_t i = 0; i < W*H; ++i) {
            data[i] += m.link.data[i];
        }
        return *this;
    }

// ----------------------------------------------------------------------------

    Matrix& operator-= (const Matrix& m) {
        for (size_t i = 0; i < W*H; ++i) {
            data[i] -= m.data[i];
        }
        return *this;
    }

    Matrix& operator-= (const Tran& m) {
        for (size_t i = 0; i < W*H; ++i) {
            data[i] -= m.link.data[i];
        }
        return *this;
    }
    
// ----------------------------------------------------------------------------

    Matrix& operator*= (const Scalar& s) {
        for (size_t i = 0; i < W*H; ++i) {
            data[i] *= s;
        }
        return *this;
    }

    Matrix& operator/= (const Scalar& s) {
        Scalar m = 1 / s;
        for (size_t i = 0; i < W*H; ++i) {
            data[i] *= m;
        }
        return *this;
    }
    
// ----------------------------------------------------------------------------

    Matrix& operator*= (const Matrix& m2) {
        static_assert(W == H, "Matrix is not square");

        Matrix m1 = *this;
        for (size_t x = 0; x < W; ++x) {
            for (size_t y = 0; y < W; ++y) {
                Scalar s(0);
                for (size_t i = 0; i < W; ++i) {
                    s += m1(i, y) * m2(x, i);
                }
                (*this)(x, y) = s;
            }
        }
        return *this;
    }

    Matrix& operator*= (const Tran& m2) {
        static_assert(W == H, "Matrix is not square");

        Matrix m1 = *this;
        for (size_t x = 0; x < W; ++x) {
            for (size_t y = 0; y < W; ++y) {
                Scalar s(0);
                for (size_t i = 0; i < W; ++i) {
                    s += m1(i, y) * m2(x, i);
                }
                (*this)(x, y) = s;
            }
        }
        return *this;
    }
    
// ----------------------------------------------------------------------------

    Scalar det() const {
        static_assert(W == H, "Matrix is not square");
        return ::det(*this);
    }

    Scalar length() const {
        static_assert(W == 1 || H == 1, "Matrix is not a vector");
        return ::length(*this);
    }
    
// ----------------------------------------------------------------------------

    Matrix& normalize() {
        static_assert(W == 1 || H == 1, "Matrix is not a vector");
        *this /= length();
        return *this;
    }

    Matrix normalized() const {
        static_assert(W == 1 || H == 1, "Matrix is not a vector");
        return ::normalize(*this);
    }
    
// ---------------------------------------------------------------------------

    Matrix& toAbs() {
        for (size_t i = 0; i < W*H; ++i) {
            data[i] = ::abs(data[i]);
        }
        return *this;
    }

    Matrix abs() const {
        return ::abs(*this);
    }

};

// ----------------------------------------------------------------------------

template <typename Scalar, size_t W, size_t H> inline
Matrix<Scalar, W, H>::Matrix() {
    memset(data, 0, sizeof data);
    const size_t L = W > H ? H : W;
    for (size_t i = 0; i < L; ++i) {
        (*this)(i, i) = 1;
    }
}

template <typename Scalar, size_t W, size_t H> inline
Matrix<Scalar, W, H>::Matrix(const Scalar& s) {
    for (size_t i = 0; i < W*H; ++i) {
        data[i] = s;
    }
}

template <typename Scalar, size_t W, size_t H> inline
Matrix<Scalar, W, H>::Matrix(const Scalar* m) {
    memcpy(data, m, sizeof data);
}

// ----------------------------------------------------------------------------

/*template <typename Scalar> inline
Matrix<Scalar, 1, 1> createMatrix(const Scalar& s0) {
    const float m[] = {
        s0
    };
    return Matrix<Scalar, 1, 1>(m);
}*/

template <typename Scalar, size_t W, size_t H>
struct CMatrix;

template <typename Scalar>
struct CMatrix<Scalar, 1, 2> {
    static inline Matrix<Scalar, 1, 2>
    create(const Scalar& s0, const Scalar& s1) {
        const float m[] = {
            s0, s1
        };
        return Matrix<Scalar, 1, 2>(m);
    }
};

template <typename Scalar>
struct CMatrix<Scalar, 1, 3> {
    static inline Matrix<Scalar, 1, 3>
    create(const Scalar& s0, const Scalar& s1, const Scalar& s2) {
        const float m[] = {
            s0, s1, s2
        };
        return Matrix<Scalar, 1, 3>(m);
    }
};

template <typename Scalar>
struct CMatrix<Scalar, 1, 4> {
    static inline Matrix<Scalar, 1, 4>
    create(const Scalar& s0, const Scalar& s1, const Scalar& s2, const Scalar& s3) {
        const float m[] = {
            s0, s1, s2, s3
        };
        return Matrix<Scalar, 1, 4>(m);
    }
};

template <typename Scalar>
struct CMatrix<Scalar, 2, 1> {
    static inline Matrix<Scalar, 2, 1>
    create(const Scalar& s0,
           const Scalar& t0) {
        const float m[] = {
            s0,
            t0
        };
        return Matrix<Scalar, 2, 1>(m);
    }
};

template <typename Scalar>
struct CMatrix<Scalar, 2, 2> {
    static inline Matrix<Scalar, 2, 2>
    create(const Scalar& s0, const Scalar& s1,
           const Scalar& t0, const Scalar& t1) {
        const float m[] = {
            s0, s1,
            t0, t1
        };
        return Matrix<Scalar, 2, 2>(m);
    }
};

template <typename Scalar>
struct CMatrix<Scalar, 2, 3> {
    static inline Matrix<Scalar, 2, 3>
    create(const Scalar& s0, const Scalar& s1, const Scalar& s2,
           const Scalar& t0, const Scalar& t1, const Scalar& t2) {
        const float m[] = {
            s0, s1, s2,
            t0, t1, t2
        };
        return Matrix<Scalar, 2, 3>(m);
    }
};

template <typename Scalar>
struct CMatrix<Scalar, 2, 4> {
    static inline Matrix<Scalar, 2, 4>
    create(const Scalar& s0, const Scalar& s1, const Scalar& s2, const Scalar& s3,
           const Scalar& t0, const Scalar& t1, const Scalar& t2, const Scalar& t3) {
        const float m[] = {
            s0, s1, s2, s3,
            t0, t1, t2, t3
        };
        return Matrix<Scalar, 2, 4>(m);
    }
};

template <typename Scalar>
struct CMatrix<Scalar, 3, 1> {
    static inline Matrix<Scalar, 3, 1>
    create(const Scalar& s0,
           const Scalar& t0,
           const Scalar& u0) {
        const float m[] = {
            s0,
            t0,
            u0
        };
        return Matrix<Scalar, 3, 1>(m);
    }
};

template <typename Scalar>
struct CMatrix<Scalar, 3, 2> {
    static inline Matrix<Scalar, 3, 2>
    create(const Scalar& s0, const Scalar& s1,
           const Scalar& t0, const Scalar& t1,
           const Scalar& u0, const Scalar& u1) {
        const float m[] = {
            s0, s1,
            t0, t1,
            u0, u1
        };
        return Matrix<Scalar, 3, 2>(m);
    }
};

template <typename Scalar>
struct CMatrix<Scalar, 3, 3> {
    static inline Matrix<Scalar, 3, 3>
    create(const Scalar& s0, const Scalar& s1, const Scalar& s2,
           const Scalar& t0, const Scalar& t1, const Scalar& t2,
           const Scalar& u0, const Scalar& u1, const Scalar& u2) {
        const float m[] = {
            s0, s1, s2,
            t0, t1, t2,
            u0, u1, u2
        };
        return Matrix<Scalar, 3, 3>(m);
    }
};

template <typename Scalar>
struct CMatrix<Scalar, 3, 4> {
    static inline Matrix<Scalar, 3, 4>
    create(const Scalar& s0, const Scalar& s1, const Scalar& s2, const Scalar& s3,
           const Scalar& t0, const Scalar& t1, const Scalar& t2, const Scalar& t3,
           const Scalar& u0, const Scalar& u1, const Scalar& u2, const Scalar& u3) {
        const float m[] = {
            s0, s1, s2, s3,
            t0, t1, t2, t3,
            u0, u1, u2, u3
        };
        return Matrix<Scalar, 3, 4>(m);
    }
};

template <typename Scalar>
struct CMatrix<Scalar, 4, 1> {
    static inline Matrix<Scalar, 4, 1>
    create(const Scalar& s0,
           const Scalar& t0,
           const Scalar& u0,
           const Scalar& v0) {
        const float m[] = {
            s0,
            t0,
            u0,
            v0
        };
        return Matrix<Scalar, 4, 1>(m);
    }
};

template <typename Scalar>
struct CMatrix<Scalar, 4, 2> {
    static inline Matrix<Scalar, 4, 2>
    create(const Scalar& s0, const Scalar& s1,
           const Scalar& t0, const Scalar& t1,
           const Scalar& u0, const Scalar& u1,
           const Scalar& v0, const Scalar& v1) {
        const float m[] = {
            s0, s1,
            t0, t1,
            u0, u1,
            v0, v1
        };
        return Matrix<Scalar, 4, 2>(m);
    }
};

template <typename Scalar>
struct CMatrix<Scalar, 4, 3> {
    static inline Matrix<Scalar, 4, 3>
    create(const Scalar& s0, const Scalar& s1, const Scalar& s2,
           const Scalar& t0, const Scalar& t1, const Scalar& t2,
           const Scalar& u0, const Scalar& u1, const Scalar& u2,
           const Scalar& v0, const Scalar& v1, const Scalar& v2) {
        const float m[] = {
            s0, s1, s2,
            t0, t1, t2,
            u0, u1, u2,
            v0, v1, v2
        };
        return Matrix<Scalar, 4, 3>(m);
    }
};

template <typename Scalar>
struct CMatrix<Scalar, 4, 4> {
    static inline Matrix<Scalar, 4, 4>
    create(const Scalar& s0, const Scalar& s1, const Scalar& s2, const Scalar& s3,
           const Scalar& t0, const Scalar& t1, const Scalar& t2, const Scalar& t3,
           const Scalar& u0, const Scalar& u1, const Scalar& u2, const Scalar& u3,
           const Scalar& v0, const Scalar& v1, const Scalar& v2, const Scalar& v3) {
        const float m[] = {
            s0, s1, s2, s3,
            t0, t1, t2, t3,
            u0, u1, u2, u3,
            v0, v1, v2, v3
        };
        return Matrix<Scalar, 4, 4>(m);
    }
};

// ----------------------------------------------------------------------------

template <typename Scalar, size_t W, size_t H> inline
Scalar& Matrix<Scalar, W, H>::operator() (size_t x, size_t y) {
    assert(x < W && y < H);
    return data[x * W + y];
}

template <typename Scalar, size_t W, size_t H> inline
const Scalar& Matrix<Scalar, W, H>::operator() (size_t x, size_t y) const {
    assert(x < W && y < H);
    return data[x * W + y];
}

template <typename Scalar, size_t W, size_t H> inline
const Scalar& Transpose<Scalar, W, H>::operator() (size_t x, size_t y) const {
    assert(x < H && y < W);
    return link.data[y * H + x];
}

// ----------------------------------------------------------------------------

template <typename Scalar, size_t W, size_t H> inline
Scalar& Matrix<Scalar, W, H>::operator[] (size_t i) {
    assert((W == 1 && i < H) || (H == 1 && i < W));
    return data[i];
}

template <typename Scalar, size_t W, size_t H> inline
const Scalar& Matrix<Scalar, W, H>::operator[] (size_t i) const {
    assert((W == 1 && i < H) || (H == 1 && i < W));
    return data[i];
}

template <typename Scalar, size_t W, size_t H> inline
const Scalar& Transpose<Scalar, W, H>::operator[] (size_t i) const {
    assert((W == 1 && i < H) || (H == 1 && i < W));
    return link.data[i];
}

// ---------------------------------------------------------------------------

template <typename Scalar, size_t W, size_t H> inline
const Matrix<Scalar, W, H>& operator + (const Matrix<Scalar, W, H>& m) {
    return m;
}

template <typename Scalar, size_t W, size_t H> inline
const Transpose<Scalar, W, H>& operator + (const Transpose<Scalar, W, H>& m) {
    return m;
}

// ----------------------------------------------------------------------------

template <typename Scalar, size_t W, size_t H> inline
Matrix<Scalar, W, H> operator - (const Matrix<Scalar, W, H>& m) {
    Matrix<Scalar, W, H> ret;
    for (size_t i = 0; i < W*H; ++i) {
        ret.data[i] = -m.data[i];
    }
    return ret;
}

template <typename Scalar, size_t W, size_t H> inline
Matrix<Scalar, W, H> operator - (const Transpose<Scalar, H, W>& m) {
    Matrix<Scalar, W, H> ret;
    for (size_t i = 0; i < W*H; ++i) {
        ret.data[i] = -m.link.data[i];
    }
    return ret;
}

// ----------------------------------------------------------------------------

template <typename Scalar, size_t W, size_t H> inline
Matrix<Scalar, W, H> operator + (const Matrix<Scalar, W, H>& m1,
                                 const Matrix<Scalar, W, H>& m2) {
    Matrix<Scalar, W, H> ret;
    for (size_t i = 0; i < W*H; ++i) {
        ret.data[i] = m1.data[i] + m2.data[i];
    }
    return ret;
}

template <typename Scalar, size_t W, size_t H> inline
Matrix<Scalar, W, H> operator + (const Transpose<Scalar, H, W>& m1,
                                 const Matrix<Scalar, W, H>& m2) {
    Matrix<Scalar, W, H> ret;
    for (size_t i = 0; i < W*H; ++i) {
        ret.data[i] = m1.link.data[i] + m2.data[i];
    }
    return ret;
}

template <typename Scalar, size_t W, size_t H> inline
Matrix<Scalar, W, H> operator + (const Matrix<Scalar, W, H>& m1,
                                 const Transpose<Scalar, H, W>& m2) {
    Matrix<Scalar, W, H> ret;
    for (size_t i = 0; i < W*H; ++i) {
        ret.data[i] = m1.data[i] + m2.link.data[i];
    }
    return ret;
}

template <typename Scalar, size_t W, size_t H> inline
Matrix<Scalar, W, H> operator + (const Transpose<Scalar, H, W>& m1,
                                 const Transpose<Scalar, H, W>& m2) {
    Matrix<Scalar, W, H> ret;
    for (size_t i = 0; i < W*H; ++i) {
        ret.data[i] = m1.link.data[i] + m2.link.data[i];
    }
    return ret;
}

// ----------------------------------------------------------------------------

template <typename Scalar, size_t W, size_t H> inline
Matrix<Scalar, W, H> operator - (const Matrix<Scalar, W, H>& m1,
                                 const Matrix<Scalar, W, H>& m2) {
    Matrix<Scalar, W, H> ret;
    for (size_t i = 0; i < W*H; ++i) {
        ret.data[i] = m1.data[i] - m2.data[i];
    }
    return ret;
}

template <typename Scalar, size_t W, size_t H> inline
Matrix<Scalar, W, H> operator - (const Transpose<Scalar, H, W>& m1,
                                 const Matrix<Scalar, W, H>& m2) {
    Matrix<Scalar, W, H> ret;
    for (size_t i = 0; i < W*H; ++i) {
        ret.data[i] = m1.link.data[i] - m2.data[i];
    }
    return ret;
}

template <typename Scalar, size_t W, size_t H> inline
Matrix<Scalar, W, H> operator - (const Matrix<Scalar, W, H>& m1,
                                 const Transpose<Scalar, H, W>& m2) {
    Matrix<Scalar, W, H> ret;
    for (size_t i = 0; i < W*H; ++i) {
        ret.data[i] = m1.data[i] - m2.link.data[i];
    }
    return ret;
}

template <typename Scalar, size_t W, size_t H> inline
Matrix<Scalar, W, H> operator - (const Transpose<Scalar, H, W>& m1,
                                 const Transpose<Scalar, H, W>& m2) {
    Matrix<Scalar, W, H> ret;
    for (size_t i = 0; i < W*H; ++i) {
        ret.data[i] = m1.link.data[i] - m2.link.data[i];
    }
    return ret;
}

// ----------------------------------------------------------------------------

template <typename Scalar, size_t W, size_t H> inline
Matrix<Scalar, W, H> operator * (const Matrix<Scalar, W, H>& m,
                                 const Scalar& s) {
    Matrix<Scalar, W, H> ret;
    for (size_t i = 0; i < W*H; ++i) {
        ret.data[i] = m.data[i] * s;
    }
    return ret;
}

template <typename Scalar, size_t W, size_t H> inline
Matrix<Scalar, W, H> operator * (const Transpose<Scalar, H, W>& m,
                                 const Scalar& s) {
    Matrix<Scalar, W, H> ret;
    for (size_t i = 0; i < W*H; ++i) {
        ret.data[i] = m.link.data[i] * s;
    }
    return ret;
}

// ----------------------------------------------------------------------------

template <typename Scalar, size_t W, size_t H> inline
Matrix<Scalar, W, H> operator * (const Scalar& s,
                                 const Matrix<Scalar, W, H>& m) {
    return m * s;
}

template <typename Scalar, size_t W, size_t H> inline
Matrix<Scalar, W, H> operator * (const Scalar& s,
                                 const Transpose<Scalar, H, W>& m) {
    return m * s;
}

// ----------------------------------------------------------------------------

template <typename Scalar, size_t W, size_t H> inline
Matrix<Scalar, W, H> operator / (const Matrix<Scalar, W, H>& m,
                                 const Scalar& s) {
    Scalar mod = 1 / s;
    Matrix<Scalar, W, H> ret;
    for (size_t i = 0; i < W*H; ++i) {
        ret.data[i] = m.data[i] * mod;
    }
    return ret;
}

template <typename Scalar, size_t W, size_t H> inline
Matrix<Scalar, W, H> operator / (const Transpose<Scalar, H, W>& m,
                                 const Scalar& s) {
    Scalar mod = 1 / s;
    Matrix<Scalar, W, H> ret;
    for (size_t i = 0; i < W*H; ++i) {
        ret.data[i] = m.link.data[i] * mod;
    }
    return ret;
}

// ----------------------------------------------------------------------------

template <typename Scalar, size_t W, size_t H> inline
Matrix<Scalar, W, H> operator / (const Scalar& s,
                                 const Matrix<Scalar, W, H>& m) {
    Matrix<Scalar, W, H> ret;
    for (size_t i = 0; i < W*H; ++i) {
        ret.data[i] = s / m.data[i];
    }
    return ret;
}

template <typename Scalar, size_t W, size_t H> inline
Matrix<Scalar, W, H> operator / (const Scalar& s,
                                 const Transpose<Scalar, H, W>& m) {
    Matrix<Scalar, W, H> ret;
    for (size_t i = 0; i < W*H; ++i) {
        ret.data[i] = s / m.link.data[i];
    }
    return ret;
}

// ----------------------------------------------------------------------------

template <typename Scalar, size_t W, size_t H, size_t W2> inline
Matrix<Scalar, W2, H> operator * (const Matrix<Scalar, W, H>& m1,
                                  const Matrix<Scalar, W2, W>& m2) {
    Matrix<Scalar, W2, H> ret;
    for (size_t x = 0; x < W2; ++x) {
        for (size_t y = 0; y < H; ++y) {
            Scalar s(0);
            for (size_t i = 0; i < W; ++i) {
                s += m1(i, y) * m2(x, i);
            }
            ret(x, y) = s;
        }
    }
    return ret;
}

template <typename Scalar, size_t W, size_t H, size_t W2> inline
Matrix<Scalar, W2, H> operator * (const Transpose<Scalar, H, W>& m1,
                                  const Matrix<Scalar, W2, W>& m2) {
    Matrix<Scalar, W2, H> ret;
    for (size_t x = 0; x < W2; ++x) {
        for (size_t y = 0; y < H; ++y) {
            Scalar s(0);
            for (size_t i = 0; i < W; ++i) {
                s += m1(i, y) * m2(x, i);
            }
            ret(x, y) = s;
        }
    }
    return ret;
}

template <typename Scalar, size_t W, size_t H, size_t W2> inline
Matrix<Scalar, W2, H> operator * (const Matrix<Scalar, W, H>& m1,
                                  const Transpose<Scalar, W, W2>& m2) {
    Matrix<Scalar, W2, H> ret;
    for (size_t x = 0; x < W2; ++x) {
        for (size_t y = 0; y < H; ++y) {
            Scalar s(0);
            for (size_t i = 0; i < W; ++i) {
                s += m1(i, y) * m2(x, i);
            }
            ret(x, y) = s;
        }
    }
    return ret;
}

template <typename Scalar, size_t W, size_t H, size_t W2> inline
Matrix<Scalar, W2, H> operator * (const Transpose<Scalar, H, W>& m1,
                                  const Transpose<Scalar, W, W2>& m2) {
    Matrix<Scalar, W2, H> ret;
    for (size_t x = 0; x < W2; ++x) {
        for (size_t y = 0; y < H; ++y) {
            Scalar s(0);
            for (size_t i = 0; i < W; ++i) {
                s += m1(i, y) * m2(x, i);
            }
            ret(x, y) = s;
        }
    }
    return ret;
}

// ----------------------------------------------------------------------------

template <typename Scalar, size_t N> inline
Scalar operator * (const Matrix<Scalar, N, 1>& m1,
                   const Matrix<Scalar, 1, N>& m2) {
    Scalar s(0);
    for (size_t i = 0; i < N; ++i) {
        s += m1[i] * m2[i];
    }
    return s;
}

template <typename Scalar, size_t N> inline
Scalar operator * (const Matrix<Scalar, N, 1>& m1,
                   const Transpose<Scalar, N, 1>& m2) {
    Scalar s(0);
    for (size_t i = 0; i < N; ++i) {
        s += m1[i] * m2[i];
    }
    return s;
}

template <typename Scalar, size_t N> inline
Scalar operator * (const Transpose<Scalar, 1, N>& m1,
                   const Matrix<Scalar, 1, N>& m2) {
    Scalar s(0);
    for (size_t i = 0; i < N; ++i) {
        s += m1[i] * m2[i];
    }
    return s;
}

template <typename Scalar, size_t N> inline
Scalar operator * (const Transpose<Scalar, 1, N>& m1,
                   const Transpose<Scalar, N, 1>& m2) {
    Scalar s(0);
    for (size_t i = 0; i < N; ++i) {
        s += m1[i] * m2[i];
    }
    return s;
}

// ----------------------------------------------------------------------------

template <typename Scalar> inline
Matrix<Scalar, 1, 3> cross(const Matrix<Scalar, 1, 3>& m1,
                           const Matrix<Scalar, 1, 3>& m2) {
    Matrix<Scalar, 1, 3> ret;
    ret[0] = m1[1]*m2[2] - m1[2]*m2[1];
    ret[1] = m1[2]*m2[0] - m1[0]*m2[2];
    ret[2] = m1[0]*m2[1] - m1[1]*m2[0];
    return ret;
}

template <typename Scalar> inline
Matrix<Scalar, 1, 3> cross(const Matrix<Scalar, 1, 3>& m1,
                           const Transpose<Scalar, 3, 1>& m2) {
    Matrix<Scalar, 1, 3> ret;
    ret[0] = m1[1]*m2[2] - m1[2]*m2[1];
    ret[1] = m1[2]*m2[0] - m1[0]*m2[2];
    ret[2] = m1[0]*m2[1] - m1[1]*m2[0];
    return ret;
}

template <typename Scalar> inline
Matrix<Scalar, 1, 3> cross(const Transpose<Scalar, 3, 1>& m1,
                           const Matrix<Scalar, 1, 3>& m2) {
    Matrix<Scalar, 1, 3> ret;
    ret[0] = m1[1]*m2[2] - m1[2]*m2[1];
    ret[1] = m1[2]*m2[0] - m1[0]*m2[2];
    ret[2] = m1[0]*m2[1] - m1[1]*m2[0];
    return ret;
}

template <typename Scalar> inline
Matrix<Scalar, 1, 3> cross(const Transpose<Scalar, 3, 1>& m1,
                           const Transpose<Scalar, 3, 1>& m2) {
    Matrix<Scalar, 1, 3> ret;
    ret[0] = m1[1]*m2[2] - m1[2]*m2[1];
    ret[1] = m1[2]*m2[0] - m1[0]*m2[2];
    ret[2] = m1[0]*m2[1] - m1[1]*m2[0];
    return ret;
}

// ----------------------------------------------------------------------------

template <typename Scalar> inline
Scalar det(const Matrix<Scalar, 1, 1>& m) {
    return m.data[0];
}

template <typename Scalar> inline
Scalar det(const Matrix<Scalar, 2, 2>& m) {
    return m(0, 0)*m(1, 1) -
           m(0, 1)*m(1, 0);
}

template <typename Scalar> inline
Scalar det(const Matrix<Scalar, 3, 3>& m) {
    return m(0, 0)*m(1, 1)*m(2, 2) +
           m(1, 0)*m(2, 1)*m(0, 2) +
           m(2, 0)*m(0, 1)*m(1, 2) -
           m(2, 0)*m(1, 1)*m(0, 2) -
           m(2, 1)*m(1, 2)*m(0, 0) -
           m(2, 2)*m(1, 0)*m(0, 1);
}

template <typename Scalar> inline
Scalar det(const Matrix<Scalar, 4, 4>& m) {
    return m(0, 0)*m(1, 1)*m(2, 2)*m(3, 3) +
           m(1, 0)*m(2, 1)*m(0, 2)*m(3, 3) +
           m(2, 0)*m(0, 1)*m(1, 2)*m(3, 3) -
           m(2, 0)*m(1, 1)*m(0, 2)*m(3, 3) -
           m(2, 1)*m(1, 2)*m(0, 0)*m(3, 3) -
           m(2, 2)*m(1, 0)*m(0, 1)*m(3, 3) -
           
           m(0, 0)*m(1, 1)*m(2, 3)*m(3, 2) -
           m(1, 0)*m(2, 1)*m(0, 3)*m(3, 2) -
           m(2, 0)*m(0, 1)*m(1, 3)*m(3, 2) +
           m(2, 0)*m(1, 1)*m(0, 3)*m(3, 2) +
           m(2, 1)*m(1, 3)*m(0, 0)*m(3, 2) +
           m(2, 3)*m(1, 0)*m(0, 1)*m(3, 2) +
           
           m(0, 0)*m(1, 2)*m(2, 3)*m(3, 1) +
           m(1, 0)*m(2, 2)*m(0, 3)*m(3, 1) +
           m(2, 0)*m(0, 2)*m(1, 3)*m(3, 1) -
           m(2, 0)*m(1, 2)*m(0, 3)*m(3, 1) -
           m(2, 2)*m(1, 3)*m(0, 0)*m(3, 1) -
           m(2, 3)*m(1, 0)*m(0, 2)*m(3, 1) -
           
           
           m(0, 1)*m(1, 2)*m(2, 3)*m(3, 0) -
           m(1, 1)*m(2, 2)*m(0, 3)*m(3, 0) -
           m(2, 1)*m(0, 2)*m(1, 3)*m(3, 0) +
           m(2, 1)*m(1, 2)*m(0, 3)*m(3, 0) +
           m(2, 2)*m(1, 3)*m(0, 1)*m(3, 0) +
           m(2, 3)*m(1, 1)*m(0, 2)*m(3, 0);
}

template <typename Scalar, size_t N> inline
Scalar det(const Transpose<Scalar, N, N>& m) {
    return det(*m);
}

// ----------------------------------------------------------------------------

template <typename Scalar, size_t N> inline
Scalar length(const Matrix<Scalar, 1, N>& v) {
    Scalar l = 0;
    for (size_t i = 0; i < N; ++i) {
        l += v[i]*v[i];
    }
    return sqrt(l);
}

template <typename Scalar, size_t N> inline
Scalar length(const Matrix<Scalar, N, 1>& v) {
    Scalar l = 0;
    for (size_t i = 0; i < N; ++i) {
        l += v[i]*v[i];
    }
    return sqrt(l);
}

template <typename Scalar, size_t N> inline
Scalar length(const Transpose<Scalar, 1, N>& v) {
    Scalar l = 0;
    for (size_t i = 0; i < N; ++i) {
        l += v[i]*v[i];
    }
    return sqrt(l);
}

template <typename Scalar, size_t N> inline
Scalar length(const Transpose<Scalar, N, 1>& v) {
    Scalar l = 0;
    for (size_t i = 0; i < N; ++i) {
        l += v[i]*v[i];
    }
    return sqrt(l);
}

// ----------------------------------------------------------------------------

template <typename Scalar, size_t N> inline
Matrix<Scalar, 1, N> normalize(const Matrix<Scalar, 1, N>& v) {
    return v / length(v);
}

template <typename Scalar, size_t N> inline
Matrix<Scalar, N, 1> normalize(const Matrix<Scalar, N, 1>& v) {
    return v / length(v);
}

template <typename Scalar, size_t N> inline
Matrix<Scalar, 1, N> normalize(const Transpose<Scalar, N, 1>& v) {
    return v / length(v);
}

template <typename Scalar, size_t N> inline
Matrix<Scalar, N, 1> normalize(const Transpose<Scalar, 1, N>& v) {
    return v / length(v);
}

// ----------------------------------------------------------------------------

template <typename Scalar, size_t W, size_t H> inline
Matrix<Scalar, W, H> abs(const Matrix<Scalar, W, H>& v) {
    Matrix<Scalar, W, H> ret = v;
    return ret.toAbs();
}

template <typename Scalar, size_t W, size_t H> inline
Matrix<Scalar, W, H> abs(const Transpose<Scalar, H, W>& v) {
    Matrix<Scalar, W, H> ret = v;
    return ret.toAbs();
}

// ----------------------------------------------------------------------------

Matrix<float, 4, 4> translationMatrix(float x, float y, float z);

inline Matrix<float, 4, 4> translationMatrix(const Matrix<float, 1, 3>& v) {
    return translationMatrix(v[0], v[1], v[2]);
}

inline Matrix<float, 4, 4> translationMatrix(const Matrix<float, 3, 1>& v) {
    return translationMatrix(v[0], v[1], v[2]);
}

inline Matrix<float, 4, 4> translationMatrix(const Transpose<float, 1, 3>& v) {
    return translationMatrix(v[0], v[1], v[2]);
}

inline Matrix<float, 4, 4> translationMatrix(const Transpose<float, 3, 1>& v) {
    return translationMatrix(v[0], v[1], v[2]);
}

// ----------------------------------------------------------------------------

// NOTE |(x, y, z)| must be 1!
Matrix<float, 4, 4> rotationMatrix(float angle, float x, float y, float z);

inline Matrix<float, 4, 4> rotationMatrix(float angle, const Matrix<float, 1, 3>& v) {
    return rotationMatrix(angle, v[0], v[1], v[2]);
}

inline Matrix<float, 4, 4> rotationMatrix(float angle, const Matrix<float, 3, 1>& v) {
    return rotationMatrix(angle, v[0], v[1], v[2]);
}

inline Matrix<float, 4, 4> rotationMatrix(float angle, const Transpose<float, 1, 3>& v) {
    return rotationMatrix(angle, v[0], v[1], v[2]);
}

inline Matrix<float, 4, 4> rotationMatrix(float angle, const Transpose<float, 3, 1>& v) {
    return rotationMatrix(angle, v[0], v[1], v[2]);
}

// ----------------------------------------------------------------------------

// Simple 2D rotation (by Z vector).
Matrix<float, 2, 2> rotationMatrix(float angle);

// ----------------------------------------------------------------------------

Matrix<float, 4, 4> scalingMatrix(float x, float y, float z);

inline Matrix<float, 4, 4> scalingMatrix(const Matrix<float, 1, 3>& v) {
    return scalingMatrix(v[0], v[1], v[2]);
}

inline Matrix<float, 4, 4> scalingMatrix(const Matrix<float, 3, 1>& v) {
    return scalingMatrix(v[0], v[1], v[2]);
}

inline Matrix<float, 4, 4> scalingMatrix(const Transpose<float, 1, 3>& v) {
    return scalingMatrix(v[0], v[1], v[2]);
}

inline Matrix<float, 4, 4> scalingMatrix(const Transpose<float, 3, 1>& v) {
    return scalingMatrix(v[0], v[1], v[2]);
}

// ----------------------------------------------------------------------------

Matrix<float, 4, 4> orthogonalMatrix(float xMin, float xMax,
                                     float yMin, float yMax,
                                     float zMin, float zMax);

// ----------------------------------------------------------------------------

Matrix<float, 4, 4> perspectiveMatrix(float fieldOfView, float aspectRatio,
                                      float zNear, float zFar);

Matrix<float, 4, 4> frustumMatrix(float xMin, float xMax,
                                  float yMin, float yMax,
                                  float zMin, float zMax);

// ----------------------------------------------------------------------------

//TODO permute it...
Matrix<float, 4, 4> lookAtMatrix(const Matrix<float, 1, 3>& eye,
                                 const Matrix<float, 1, 3>& center,
                                 const Matrix<float, 1, 3>& up);

// ----------------------------------------------------------------------------

//TODO permute it...
Matrix<float, 4, 4> pickMatrix(const Matrix<float, 1, 2>& position,
                               const Matrix<float, 1, 2>& radius);

// ----------------------------------------------------------------------------

template <typename Scalar> inline
Scalar sign(const Scalar& s) {
    if (s < 0) {
        return -1;
    }
    return 1;
}

// ----------------------------------------------------------------------------

// ### Optimizations

union FloatInt {
    static_assert(sizeof(float) == 4 && sizeof(unsigned) == 4,
        "Unsupported float and unsigned sizes");
    float f;
    unsigned i;
};

union DoubleInt {
    static_assert(sizeof(double) == 8 && sizeof(unsigned long long) == 8,
        "Unsupported double and unsigned long long sizes");
    double d;
    unsigned long long i;
};

// ----------------------------------------------------------------------------

template <size_t W, size_t H> inline
Matrix<float, W, H> operator - (const Matrix<float, W, H>& m) {
    Matrix<float, W, H> ret;
    FloatInt fi;
    for (size_t i = 0; i < W*H; ++i) {
        fi.f = m.data[i];
        fi.i ^= 0x80000000;
        ret.data[i] = fi.f;
    }
    return ret;
}

template <size_t W, size_t H> inline
Matrix<float, W, H> operator - (const Transpose<float, H, W>& m) {
    Matrix<float, W, H> ret;
    FloatInt fi;
    for (size_t i = 0; i < W*H; ++i) {
        fi.f = m.data[i];
        fi.i ^= 0x80000000;
        ret.data[i] = fi.f;
    }
    return ret;
}

// ----------------------------------------------------------------------------

template <size_t W, size_t H> inline
Matrix<double, W, H> operator - (const Matrix<double, W, H>& m) {
    Matrix<double, W, H> ret;
    doubleInt di;
    for (size_t i = 0; i < W*H; ++i) {
        di.d = m.data[i];
        di.i ^= 0x8000000000000000ULL;
        ret.data[i] = di.d;
    }
    return ret;
}

template <size_t W, size_t H> inline
Matrix<double, W, H> operator - (const Transpose<double, H, W>& m) {
    Matrix<double, W, H> ret;
    FloatInt fi;
    for (size_t i = 0; i < W*H; ++i) {
        di.d = m.data[i];
        di.i ^= 0x8000000000000000ULL;
        ret.data[i] = di.d;
    }
    return ret;
}

// ----------------------------------------------------------------------------

template <> inline
float sign(const float& s) {
    FloatInt fi;
    fi.f = s;
    return fi.i & 0x80000000U ? -1.0f : 1.0f;
}

#endif
