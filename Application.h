/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#ifndef Application_h
#define Application_h

#include "CommandLine.h"
#include "GL.h"
#include "Renderer.h"
#include "Scene.h"
#include "System.h"

class Application {
    static Application* m_instance;

    class ExtensionWrangler {
    public:
        const char* wglExtensions;

        PFNWGLGETEXTENSIONSSTRINGARBPROC _wglGetExtensionsStringARB;
        PFNWGLGETEXTENSIONSSTRINGEXTPROC _wglGetExtensionsStringEXT;

        PFNWGLGETPIXELFORMATATTRIBIVARBPROC _wglGetPixelFormatAttribivARB;
        PFNWGLGETPIXELFORMATATTRIBFVARBPROC _wglGetPixelFormatAttribfvARB;
        PFNWGLCHOOSEPIXELFORMATARBPROC _wglChoosePixelFormatARB;

        PFNWGLCREATECONTEXTATTRIBSARBPROC _wglCreateContextAttribsARB;

        ExtensionWrangler();

        void init(Application& app);
        void finit();
        bool isSupported(const char* name);
        void use();
        void unuse();

        operator bool() const { return wglExtensions != nullptr; }

    private:
        void initExtensions();
        
        HWND fakeWindow;
        HDC fakeDC;
        HGLRC fakeContext;
    };

public:
    static Application& instance() { return *m_instance; }

    enum MouseButton {
        MB_Left,
        MB_Right,
        MB_Middle
    };

    class State {
        friend class Application;

        Application& app;
        DWORD style, exStyle;

    public:
        State(Application& app) : app(app) {}
        HINSTANCE instance;
        HWND wnd;
        HDC dc;
        HGLRC glrc;
        bool active;
        int width, height;
        float aspectRatio;
        bool keys[256];
        
        Scene::Configuration configuration;
        RendererManager rendererManager;

        void clear();
        void reset(const Scene::Configuration* conf = nullptr);
        operator bool () const { return wnd != 0; }
    };

    struct Options {
        CommandLine::Option<bool> disableChecks;
        CommandLine::Option<std::string> root;
        CommandLine::Option<std::string> renderer;
        CommandLine::Option<bool> debug;
        CommandLine::Option<std::string> scene;
    };

    Application(HINSTANCE hInstance, const char* commandLine);
    ~Application();
    
    void init();
    void setScene(Scene& scene);
    bool execute();

    static void quit(int code = 0);
    void changeWindowName(const std::string& name);

    const Options& options() const { return m_options; }
    const Scene::Factory& sceneFactory() const { return m_sceneFactory; }
    const State& state() const { return m_state; }
    Scene& scene() const { return *m_scene; }

    LRESULT windowProcedure(UINT uMsg, WPARAM wParam, LPARAM lParam);

    static const wchar_t className[];
    static const char windowName[];

protected:
    // Init
    void registerClass();
    HWND createWindow(int width, int height,
        DWORD style = WS_OVERLAPPED, DWORD exStyle = WS_EX_OVERLAPPEDWINDOW);
    HDC configureDC(HWND window);
    HGLRC createContextAttribs(HDC dc, bool compatibilityProfile,
                               int major, int minor, bool debug);
    HGLRC createContext(HDC dc, RendererManager::Version* version = nullptr,
                        bool compatibilityProfile = true);

    void unregisterClass();
    static void destroyWindow(HWND wnd);
    static void destroyDC(HWND wnd, HDC dc);
    static void destroyContext(HGLRC glrc);

    void resizeWindow(int w, int h);

    RendererManager::Version chooseRenderer();
    void initRenderer(RendererManager::Version version);

    // Events
    void activated();
    void resized();
    void keyPressed(int which);
    void keyReleased(int which);
    void mouseButtonPressed(MouseButton which, int x, int y);
    void mouseButtonReleased(MouseButton which, int x, int y);
    void mouseMoved(int x, int y);
    void nextFrame();
    
    void makeCurrent() { makeCurrent(m_state.dc, m_state.glrc); }
    static void makeCurrent(HDC dc, HGLRC glrc);
    static void unmakeCurrent();

    CommandLine::Parser cmdLineParser;
    Options m_options;
    Scene::Factory m_sceneFactory;
    State m_state;
    bool classRegistered;
    ExtensionWrangler extensionWrangler;
    RendererManager::Version rendererVersion;
    Scene* m_scene;
    ULONGLONG lastTick;
};

#endif
