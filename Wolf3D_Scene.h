/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#ifndef Wolf3D_Scene_h
#define Wolf3D_Scene_h

#include "Application.h"
#include "Camera.h"
#include "GL.h"
#include "Matrix.h"
#include "Scene.h"
#include "Wolf3D.h"
#include "Wolf3D_Resources.h"
#include "Texture.h"
#include "Wolf3D_Level.h"

namespace Wolf3D {

class Level;

class Scene : public ::Scene {
public:
    class Camera : public ::Camera {
    public:
        void init(Level* level);
        
        void recomputeMatrices();

    private:
		Level* level;
    };

    Scene() : ::Scene("Wolfenstein 3D") {}

    virtual void load(Application&);
    virtual void unload(Application&);
    virtual void activate(Application&);
    virtual void suspend(Application&);
    virtual void resized(Application&);
    virtual void keyPressed(Application&, int which);
    virtual void keyReleased(Application&, int which);
    virtual void update(Application&, long diff);
    virtual void repaint(Application&);

private:
	Palette palette;
    GameMaps gameMaps;
    Graphics graphics;
	WallCatalog wallCatalog;
	SpriteCatalog spriteCatalog;
	DoorCatalog doorCatalog;
	
    Level level;
    Camera camera;
    Renderer<Level>* renderer;
};

} // end of Wolf3D

#endif
