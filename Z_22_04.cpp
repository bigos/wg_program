/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#include "Z_22_04.h"

#include "Log.h"

template <>
class Renderer_1_1<Z_22_04::Floor> : public Renderer<Z_22_04::Floor> {
public:
    virtual ~Renderer_1_1() {}

    virtual void init(const Z_22_04::Floor* object, size_t num = 1) {
        floor = object;
        this->num = num;
    }

    virtual void draw(DefaultContext& context) {
        for (size_t i = 0; i < num; ++i) {
            const Z_22_04::Floor& floor = this->floor[i];
            glColor3f(floor.color[0], floor.color[1], floor.color[2]);
            
            Matrix<float, 1, 3> dimen = CMatrix<float, 1, 3>::create(0, 1, 0);

            float slices = 100;

            Matrix<float, 1, 3> base = CMatrix<float, 1, 3>::create(0, 0, 0);
            Matrix<float, 1, 3> rest1 = 1.0f, rest2;
            rest1 -= abs(dimen);
            rest1 *= floor.width/2;
            rest2 = rest1;
            if (dimen[0] != 0) {
                rest1[1] = 0;
                rest2[2] = 0;
            } else {
                rest1[1] = rest1[2] = 0;
                rest2[0] = 0;
            }

            glNormal3f(dimen[0], dimen[1], dimen[2]);

            glBegin(GL_QUADS);
                Matrix<float, 1, 3> lastX = -rest1;
                Matrix<float, 1, 3> X = lastX + rest1/slices*2.0f;
                for (size_t x = 0; x < slices; ++x) {
                    Matrix<float, 1, 3> lastY = -rest2;
                    Matrix<float, 1, 3> Y = lastY + rest2/slices*2.0f;

                    for (size_t y = 0; y < slices; ++y) {
                        Matrix<float, 1, 3> xy, xY, Xy, XY;
                        xy = base + lastX + lastY;
                        xY = base + lastX + Y;
                        Xy = base + X + lastY;
                        XY = base + X + Y;

                        glVertex3f(xy[0], xy[1], xy[2]);
                        glVertex3f(xY[0], xY[1], xY[2]);
                        glVertex3f(XY[0], XY[1], XY[2]);
                        glVertex3f(Xy[0], Xy[1], Xy[2]);

                        lastY = Y;
                        Y += rest2/slices*2.0f;
                    }
                    lastX = X;
                    X += rest1/slices*2.0f;
                }
            glEnd();
        }
    }

private:
    const Z_22_04::Floor* floor;
    size_t num;
};

template <>
class Renderer_1_1<Z_22_04::Sphere> : public Renderer<Z_22_04::Sphere> {
public:
    virtual ~Renderer_1_1() {}

    virtual void init(const Z_22_04::Sphere* object, size_t num = 1) {
        sphere = object;
        this->num = num;
    }

    virtual void draw(DefaultContext& context) {
        for (size_t i = 0; i < num; ++i) {
            const Z_22_04::Sphere& sphere = this->sphere[i];
            glColor3f(sphere.color[0], sphere.color[1], sphere.color[2]);

            if (sphere.selected) {
                glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            }
            
            glBegin(GL_QUADS);
                float lastX = -M_PI/sphere.slices;
                float X = 0.0f;
                for (size_t x = 0; x < 2*sphere.slices; ++x) {
                    float lastY = -M_PI/2;
                    float Y = M_PI/sphere.stacks - M_PI/2;
                    for (size_t y = 0; y < sphere.stacks; ++y) {
                        Matrix<float, 1, 3> xy, xY, Xy, XY;
                        xy = sphere.spheric(lastX, lastY);
                        xY = sphere.spheric(lastX, Y);
                        Xy = sphere.spheric(X, lastY);
                        XY = sphere.spheric(X, Y);
                        Matrix<float, 1, 3> Nxy, NxY, NXy, NXY;
                        Nxy = sphere.spheric(lastX, lastY, 1);
                        NxY = sphere.spheric(lastX, Y, 1);
                        NXy = sphere.spheric(X, lastY, 1);
                        NXY = sphere.spheric(X, Y, 1);

                        glNormal3f(Nxy[0], Nxy[1], Nxy[2]);
                        glVertex3f(xy[0], xy[1], xy[2]);

                        glNormal3f(NxY[0], NxY[1], NxY[2]);
                        glVertex3f(xY[0], xY[1], xY[2]);

                        glNormal3f(NXY[0], NXY[1], NXY[2]);
                        glVertex3f(XY[0], XY[1], XY[2]);

                        glNormal3f(NXy[0], NXy[1], NXy[2]);
                        glVertex3f(Xy[0], Xy[1], Xy[2]);

                        lastY = Y;
                        Y += M_PI/sphere.stacks;
                    }
                    lastX = X;
                    X += M_PI/sphere.slices;
                }
            glEnd();

            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
    }

private:
    const Z_22_04::Sphere* sphere;
    size_t num;
};

template <>
class Renderer_1_1<Z_22_04::Cylinder> : public Renderer<Z_22_04::Cylinder> {
public:
    virtual ~Renderer_1_1() {}

    virtual void init(const Z_22_04::Cylinder* object, size_t num = 1) {
        cylinder = object;
        this->num = num;
    }

    virtual void draw(DefaultContext& context) {
        for (size_t i = 0; i < num; ++i) {
            const Z_22_04::Cylinder& cylinder = this->cylinder[i];
            glColor3f(cylinder.color[0], cylinder.color[1], cylinder.color[2]);

            if (cylinder.selected) {
                glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            }
            
            glBegin(GL_QUADS);
                float lastX = -M_PI/cylinder.slices;
                float X = 0.0f;
                for (size_t x = 0; x < 2*cylinder.slices; ++x) {
                    float lastY = -cylinder.height/2;
                    float Y = cylinder.height/cylinder.stacks + lastY;

                    Matrix<float, 1, 3> Nxy, NxY, NXy, NXY;
                    Nxy = cylinder.cylindric(lastX, 1, 0);
                    NxY = cylinder.cylindric(lastX, 1, 0);
                    NXy = cylinder.cylindric(X, 1, 0);
                    NXY = cylinder.cylindric(X, 1, 0);

                    for (size_t y = 0; y < cylinder.stacks; ++y) {
                        Matrix<float, 1, 3> xy, xY, Xy, XY;
                        xy = cylinder.cylindric(lastX, cylinder.radius, lastY);
                        xY = cylinder.cylindric(lastX, cylinder.radius, Y);
                        Xy = cylinder.cylindric(X, cylinder.radius, lastY);
                        XY = cylinder.cylindric(X, cylinder.radius, Y);

                        glNormal3f(Nxy[0], Nxy[1], Nxy[2]);
                        glVertex3f(xy[0], xy[1], xy[2]);

                        glNormal3f(NxY[0], NxY[1], NxY[2]);
                        glVertex3f(xY[0], xY[1], xY[2]);

                        glNormal3f(NXY[0], NXY[1], NXY[2]);
                        glVertex3f(XY[0], XY[1], XY[2]);

                        glNormal3f(NXy[0], NXy[1], NXy[2]);
                        glVertex3f(Xy[0], Xy[1], Xy[2]);

                        lastY = Y;
                        Y += cylinder.height/cylinder.stacks;
                    }
                    lastX = X;
                    X += M_PI/cylinder.slices;
                }
            glEnd();

            Matrix<float, 1, 3> Normal = CMatrix<float, 1, 3>::create(0.0f, 1.0f, 0.0f);
            float offset = cylinder.height/2;
            for (size_t side = 0; side < 2; ++side) {
                glBegin(GL_TRIANGLE_FAN);
                    glNormal3f(Normal[0], Normal[1], Normal[2]);

                    glVertex3f(0, offset, 0);

                    float X = 0.0f;
                    for (size_t x = 0; x < 2*cylinder.slices+1; ++x) {
                        Matrix<float, 1, 3> x1 = cylinder.cylindric(X, cylinder.radius, offset);
                        glVertex3f(x1[0], x1[1], x1[2]);

                        X += M_PI/cylinder.slices;
                    }
                glEnd();

                Normal = -Normal;
                offset = -offset;
            }

            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
    }

private:
    const Z_22_04::Cylinder* cylinder;
    size_t num;
};

template <>
class Renderer_1_1<Z_22_04::Cone> : public Renderer<Z_22_04::Cone> {
public:
    virtual ~Renderer_1_1() {}

    virtual void init(const Z_22_04::Cone* object, size_t num = 1) {
        cone = object;
        this->num = num;
    }

    virtual void draw(DefaultContext& context) {
        for (size_t i = 0; i < num; ++i) {
            const Z_22_04::Cone& cone = this->cone[i];
            glColor3f(cone.color[0], cone.color[1], cone.color[2]);

            if (cone.selected) {
                glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            }
            
            glBegin(GL_QUADS);
                float lastX = -M_PI/cone.slices;
                float X = 0.0f;
                for (size_t x = 0; x < 2*cone.slices; ++x) {
                    float lastY = -cone.height/2;
                    float Y = cone.height/cone.stacks + lastY;

                    float lastR = cone.radius;
                    float R = lastR - cone.radius/cone.stacks;

                    float p = atan(cone.height/cone.radius);
                    float a = sin(p), b = cos(p);

                    Matrix<float, 1, 3> Nxy, NxY, NXy, NXY;
                    Nxy = cone.cylindric(lastX, a, b);
                    NxY = cone.cylindric(lastX, a, b);
                    NXy = cone.cylindric(X, a, b);
                    NXY = cone.cylindric(X, a, b);

                    for (size_t y = 0; y < cone.stacks; ++y) {
                        Matrix<float, 1, 3> xy, xY, Xy, XY;
                        xy = cone.cylindric(lastX, lastR, lastY);
                        xY = cone.cylindric(lastX, R, Y);
                        Xy = cone.cylindric(X, lastR, lastY);
                        XY = cone.cylindric(X, R, Y);

                        glNormal3f(Nxy[0], Nxy[1], Nxy[2]);
                        glVertex3f(xy[0], xy[1], xy[2]);

                        glNormal3f(NxY[0], NxY[1], NxY[2]);
                        glVertex3f(xY[0], xY[1], xY[2]);

                        glNormal3f(NXY[0], NXY[1], NXY[2]);
                        glVertex3f(XY[0], XY[1], XY[2]);

                        glNormal3f(NXy[0], NXy[1], NXy[2]);
                        glVertex3f(Xy[0], Xy[1], Xy[2]);

                        lastY = Y;
                        Y += cone.height/cone.stacks;

                        lastR = R;
                        R -= cone.radius/cone.stacks;
                    }
                    lastX = X;
                    X += M_PI/cone.slices;
                }
            glEnd();

            Matrix<float, 1, 3> Normal = CMatrix<float, 1, 3>::create(0.0f, -1.0f, 0.0f);
            float offset = -cone.height/2;
            glBegin(GL_TRIANGLE_FAN);
                glNormal3f(Normal[0], Normal[1], Normal[2]);

                glVertex3f(0, offset, 0);

                X = 0.0f;
                for (size_t x = 0; x < 2*cone.slices+1; ++x) {
                    Matrix<float, 1, 3> x1 = cone.cylindric(X, cone.radius, offset);
                    glVertex3f(x1[0], x1[1], x1[2]);

                    X += M_PI/cone.slices;
                }
            glEnd();

            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
    }

private:
    const Z_22_04::Cone* cone;
    size_t num;
};

template <>
class Renderer_1_1<Z_22_04::Cube> : public Renderer<Z_22_04::Cube> {
public:
    virtual ~Renderer_1_1() {}

    virtual void init(const Z_22_04::Cube* object, size_t num = 1) {
        cube = object;
        this->num = num;
    }

    virtual void draw(DefaultContext& context) {
        for (size_t i = 0; i < num; ++i) {
            const Z_22_04::Cube& cube = this->cube[i];
            glColor3f(cube.color[0], cube.color[1], cube.color[2]);

            if (cube.selected) {
                glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            }
            
            drawSide(CMatrix<float, 1, 3>::create( 1,  0,  0), cube.size, cube.slices);
            drawSide(CMatrix<float, 1, 3>::create(-1,  0,  0), cube.size, cube.slices);
            drawSide(CMatrix<float, 1, 3>::create( 0,  1,  0), cube.size, cube.slices);
            drawSide(CMatrix<float, 1, 3>::create( 0, -1,  0), cube.size, cube.slices);
            drawSide(CMatrix<float, 1, 3>::create( 0,  0,  1), cube.size, cube.slices);
            drawSide(CMatrix<float, 1, 3>::create( 0,  0, -1), cube.size, cube.slices);

            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
    }

private:
    const Z_22_04::Cube* cube;
    size_t num;

    void drawSide(const Matrix<float, 1, 3>& dimen, float size, float slices) {
        Matrix<float, 1, 3> base = dimen * (size/2);
        Matrix<float, 1, 3> rest1 = 1.0f, rest2;
        rest1 -= abs(dimen);
        rest1 *= size/2;
        rest2 = rest1;
        if (dimen[0] != 0) {
            rest1[1] = 0;
            rest2[2] = 0;
        } else {
            rest1[1] = rest1[2] = 0;
            rest2[0] = 0;
        }
        
        glNormal3f(dimen[0], dimen[1], dimen[2]);

        glBegin(GL_QUADS);
            Matrix<float, 1, 3> lastX = -rest1;
            Matrix<float, 1, 3> X = lastX + rest1/slices*2.0f;
            for (size_t x = 0; x < slices; ++x) {
                Matrix<float, 1, 3> lastY = -rest2;
                Matrix<float, 1, 3> Y = lastY + rest2/slices*2.0f;

                for (size_t y = 0; y < slices; ++y) {
                    Matrix<float, 1, 3> xy, xY, Xy, XY;
                    xy = base + lastX + lastY;
                    xY = base + lastX + Y;
                    Xy = base + X + lastY;
                    XY = base + X + Y;

                    glVertex3f(xy[0], xy[1], xy[2]);
                    glVertex3f(xY[0], xY[1], xY[2]);
                    glVertex3f(XY[0], XY[1], XY[2]);
                    glVertex3f(Xy[0], Xy[1], Xy[2]);
                    
                    lastY = Y;
                    Y += rest2/slices*2.0f;
                }
                lastX = X;
                X += rest1/slices*2.0f;
            }
        glEnd();
    }
};

template <>
class Renderer_1_1<Z_22_04::HUD> : public Renderer<Z_22_04::HUD> {
public:
    virtual ~Renderer_1_1() {
        glDeleteLists(asciiList, 127-32);
    }

    virtual void init(const Z_22_04::HUD* object, size_t num = 1) {
        hud = object;
        this->num = num;
        
        const Application::State& state = Application::instance().state();

        HFONT font, lastFont;
        font = CreateFont(-16, 0, 0, 0, FW_NORMAL, false, false, false, ANSI_CHARSET,
                          OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, ANTIALIASED_QUALITY,
                          DEFAULT_PITCH | FF_DONTCARE, L"Arial");
        if (!font) {
            throw WinApiException("CreateFont");
        }
        lastFont = (HFONT)SelectObject(state.dc, font);

        asciiList = glGenLists(127-32);

        if (!wglUseFontBitmaps(state.dc, 32, 127-32, asciiList)) {
            throw WinApiException("wglUseFontBitmaps");
        }

        SelectObject(state.dc, lastFont);
        DeleteObject(font);
    }

    virtual void draw(DefaultContext& context) {
        char buf[128];
        glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        glDisable(GL_LIGHTING);
        glListBase(asciiList-32);
        for (size_t i = 0; i < num; ++i) {
            const Z_22_04::HUD& hud = this->hud[i];
            const Z_22_04::Camera& camera = *hud.camera;

            sprintf(buf, "Alpha: %f, Beta: %f, Zoom: %f",
                double(camera.getRotY()),
                double(camera.getRotX()),
                double(camera.getZoomZ()));

            glPushAttrib(GL_LIST_BIT);
            glRasterPos3f(-1, -1, 0);
            glCallLists(strlen(buf), GL_UNSIGNED_BYTE, buf);
            glPopAttrib();
        }
        glEnable(GL_LIGHTING);
    }

private:
    const Z_22_04::HUD* hud;
    size_t num;
    GLuint asciiList;
};

template <>
class Renderer_1_1<Z_22_04::Objects> : public Renderer<Z_22_04::Objects> {
public:
    using Renderer<Z_22_04::Objects>::Context;
    
    virtual ~Renderer_1_1() {}

    virtual void init(const Z_22_04::Objects* object, size_t num) {
        this->object = object;
        assert(num == 1);
        floor.init(&object->floor);
        sphere.init(&object->sphere);
        cylinder.init(&object->cylinder);
        cone.init(&object->cone);
        cube.init(&object->cube);
        hud.init(&object->hud);
    }

    virtual void draw(Context& _context) {
        DefaultContext context = {_context.camera};

        glMatrixMode(GL_MODELVIEW);
        glLoadMatrixf(((Z_22_04::Camera*)context.camera)->getViewMatrix().data);

        if (!_context.select) {
            glMatrixMode(GL_PROJECTION);
            glLoadMatrixf(((Z_22_04::Camera*)context.camera)->getProjectionMatrix().data);
            glMatrixMode(GL_MODELVIEW);

            object->light->updateLight();

            glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, object->shininess);
            glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, object->ambient.data);
            glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, object->diffuse.data);
            glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, object->specular.data);
        
            {
                Matrix<float, 1, 4> pos =
                    rotationMatrix(object->light->rotY * (M_PI/180), 0, 1, 0) *
                    rotationMatrix(object->light->rotX * (M_PI/180), 1, 0, 0) *
                    CMatrix<float, 1, 4>::create(0, 0, object->light->trZ, 1);
                glDisable(GL_LIGHTING);
                glPointSize(6.0f);
                glBegin(GL_POINTS);
                    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
                    glVertex3f(pos[0], pos[1], pos[2]);
                glEnd();
                glEnable(GL_LIGHTING);
            }

            drawObjects(context);
        } else {    
            glMatrixMode(GL_PROJECTION);
            glLoadMatrixf((_context.selectMatrix*((Z_22_04::Camera*)context.camera)->getProjectionMatrix()).data);
            glMatrixMode(GL_MODELVIEW);
            
            glSelectBuffer(Context::BufferSize, _context.buffer);
            glRenderMode(GL_SELECT);

            glInitNames();
            glPushName(0);

            drawSelect(context);
            glFlush();

            glPopName();

            _context.hits = glRenderMode(GL_RENDER);
        }
    }

private:
    const Z_22_04::Objects* object;
    Renderer_1_1<Z_22_04::Floor> floor;
    Renderer_1_1<Z_22_04::Sphere> sphere;
    Renderer_1_1<Z_22_04::Cylinder> cylinder;
    Renderer_1_1<Z_22_04::Cone> cone;
    Renderer_1_1<Z_22_04::Cube> cube;
    Renderer_1_1<Z_22_04::HUD> hud;

    void drawObjects(DefaultContext& context) {
        floor.draw(context);

        glTranslatef(1.0f, 0.7f, 1.0f);
        sphere.draw(context);

        glTranslatef(-2.0f, 0.0f, 0.0f);
        cylinder.draw(context);
        
        glTranslatef(0.0f, 0.0f, -2.0f);
        cone.draw(context);

        glTranslatef(1.0f, -0.3f, 1.0f);
        cube.draw(context);

        glMatrixMode(GL_PROJECTION);
        glLoadMatrixf(orthogonalMatrix(-1, 1, -1, 1, -1, 1).data);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        hud.draw(context);
    }

    void drawSelect(DefaultContext& context) {
        glLoadName(Z_22_04::Objects::SphereName);
        glTranslatef(1.0f, 0.7f, 1.0f);
        sphere.draw(context);
        
        glLoadName(Z_22_04::Objects::CylinderName);
        glTranslatef(-2.0f, 0.0f, 0.0f);
        cylinder.draw(context);
        
        glLoadName(Z_22_04::Objects::ConeName);
        glTranslatef(0.0f, 0.0f, -2.0f);
        cone.draw(context);
        
        glLoadName(Z_22_04::Objects::CubeName);
        glTranslatef(1.0f, -0.3f, 1.0f);
        cube.draw(context);
    }
};

void Z_22_04::Objects::init(Light* light, Camera* camera) {
    floor.init(CMatrix<float, 1, 3>::create(1, 1, 1), 5.0f);
    sphere.init(CMatrix<float, 1, 3>::create(1, 1, 1), 0.7f);
    cylinder.init(CMatrix<float, 1, 3>::create(1, 1, 1), 0.7f, 1.4f);
    cone.init(CMatrix<float, 1, 3>::create(1, 1, 1), 0.7f, 1.4f);
    cube.init(CMatrix<float, 1, 3>::create(1, 1, 1), 0.8f);
    hud.init(camera);

    shininess = 5;
    ambient = CMatrix<float, 1, 4>::create(0.5f, 0.5f, 0.5f, 1.0f);
    diffuse = CMatrix<float, 1, 4>::create(0.5f, 0.5f, 0.5f, 1.0f);
    specular = CMatrix<float, 1, 4>::create(0.4f, 0.4f, 0.4f, 1.0f);

    this->light = light;
}

void Z_22_04::Objects::select(Names name) {
    switch (name) {
    case SphereName:
        sphere.selected = true;
        break;
    case CylinderName:
        cylinder.selected = true;
        break;
    case ConeName:
        cone.selected = true;
        break;
    case CubeName:
        cube.selected = true;
        break;
    default:
        assert(!"Invalid name");
    }
}

void Z_22_04::Objects::invertSelection(Names name) {
    switch (name) {
    case SphereName:
        sphere.selected ^= 1;
        break;
    case CylinderName:
        cylinder.selected ^= 1;
        break;
    case ConeName:
        cone.selected ^= 1;
        break;
    case CubeName:
        cube.selected ^= 1;
        break;
    default:
        assert(!"Invalid name");
    }
}

void Z_22_04::Objects::deselectAll() {
    sphere.selected = false;
    cylinder.selected = false;
    cone.selected = false;
    cube.selected = false;
}

void Z_22_04::Light::init(int number) {
    this->number = number;
    rotY = 0;
    rotX = -90;
    trZ = 5.0f;
    attenuation = CMatrix<float, 1, 3>::create(1, 0, 0);
    ambient = CMatrix<float, 1, 4>::create(0.2f, 0.2f, 0.2f, 1.0f);
    diffuse = CMatrix<float, 1, 4>::create(0.8f, 0.8f, 0.8f, 1.0f);
    specular = CMatrix<float, 1, 4>::create(1, 1, 1, 1);
    spotCutoff = 75;
    spotExponent = 10;
    
    glEnable(GL_LIGHT0+number);
}

void Z_22_04::Light::updateLight() {
    
    
    Matrix<float, 1, 4> pos =
            rotationMatrix(rotY * (M_PI/180), 0, 1, 0) *
            rotationMatrix(rotX * (M_PI/180), 1, 0, 0)
        * CMatrix<float, 1, 4>::create(0, 0, trZ, 1);
    glLightfv(GL_LIGHT0+number, GL_POSITION, pos.data);
    pos = -pos;
    glLightfv(GL_LIGHT0+number, GL_SPOT_DIRECTION, pos.data);

    glLightfv(GL_LIGHT0+number, GL_AMBIENT, ambient.data);
    glLightfv(GL_LIGHT0+number, GL_DIFFUSE, diffuse.data);
    glLightfv(GL_LIGHT0+number, GL_SPECULAR, specular.data);

    glLightfv(GL_LIGHT0+number, GL_SPOT_CUTOFF, &spotCutoff);
    glLightfv(GL_LIGHT0+number, GL_SPOT_EXPONENT, &spotExponent);
    
    glLightfv(GL_LIGHT0+number, GL_CONSTANT_ATTENUATION, &attenuation[0]);
    glLightfv(GL_LIGHT0+number, GL_LINEAR_ATTENUATION, &attenuation[1]);
    glLightfv(GL_LIGHT0+number, GL_QUADRATIC_ATTENUATION, &attenuation[2]);
}

void Z_22_04::Light::rotateX(float degrees) {
    if (rotX + degrees > 90) {
        rotX = 90;
        return;
    }
    if (rotX + degrees < -90) {
        rotX = -90;
        return;
    }
    rotX += degrees;
}

void Z_22_04::Light::rotateY(float degrees) {
    rotY += degrees;
    if (rotY > 180) {
        rotY -= 360;
        return;
    }
    if (rotY < -180) {
        rotY += 360;
        return;
    }
}

void Z_22_04::Light::zoomZ(float meters) {
    if (trZ + meters > 16) {
        trZ = 16;
        return;
    }
    if (trZ + meters < 0) {
        trZ = 0;
        return;
    }
    trZ += meters;
}

void Z_22_04::Camera::init() {
    scale = 100000.0f;
    rotX = rotY = 0;
    trZ = 5.0f;
}

void Z_22_04::Camera::recomputeMatrices() {
    Application& app = Application::instance();
    float w = app.state().width/scale;
    float h = app.state().height/scale;

    //projectionMatrix = frustumMatrix(-w, w, -h, h, 0.01f, 25.0f);
    projectionMatrix = perspectiveMatrix(2*atan(h*100.0f), w/h, 0.01f, 25.0f);
    viewMatrix = translationMatrix(0, 0, -trZ) *
                 rotationMatrix(rotX * (M_PI/180), 1, 0, 0) *
                 rotationMatrix(rotY * (M_PI/180), 0, 1, 0);
    Matrix<float, 1, 4> eye4 = rotationMatrix(rotY * (M_PI/180), 0, -1, 0) *
                               rotationMatrix(rotX * (M_PI/180), -1, 0, 0) *
                               CMatrix<float, 1, 4>::create(0, 0, trZ, 1);
    Matrix<float, 1, 4> up4 = rotationMatrix(rotY * (M_PI/180), 0, -1, 0) *
                              rotationMatrix(rotX * (M_PI/180), -1, 0, 0) *
                              CMatrix<float, 1, 4>::create(0, 1, 0, 1);
    viewMatrix = lookAtMatrix(Matrix<float, 1, 3> (eye4.data),
                              CMatrix<float, 1, 3>::create(0, 0, 0),
                              Matrix<float, 1, 3> (up4.data));

    ::Camera::recomputeMatrices();
}

void Z_22_04::Camera::rotateX(float degrees) {
    if (rotX + degrees > 90) {
        rotX = 90;
        return;
    }
    if (rotX + degrees < -90) {
        rotX = -90;
        return;
    }
    rotX += degrees;
}

void Z_22_04::Camera::rotateY(float degrees) {
    rotY += degrees;
    if (rotY > 180) {
        rotY -= 360;
        return;
    }
    if (rotY < -180) {
        rotY += 360;
        return;
    }
}

void Z_22_04::Camera::zoomZ(float meters) {
    if (trZ + meters > 16) {
        trZ = 16;
        return;
    }
    if (trZ + meters < 0) {
        trZ = 0;
        return;
    }
    trZ += meters;
}

void Z_22_04::load(Application& app) {
    dragType = DragNone;
    camera.init();
    light.init(0);

    objects.init(&light, &camera);
    renderer = app.state().rendererManager.create<Objects>();
    renderer->init(&objects);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClearDepth(1.0f);
    glDepthFunc(GL_LESS);
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
    glEnable(GL_LIGHTING);
}

void Z_22_04::unload(Application& app) {
}

void Z_22_04::activate(Application&) {
}

void Z_22_04::suspend(Application&) {
}

void Z_22_04::resized(Application&) {
}

void Z_22_04::keyPressed(Application& app, int which) {
    const Application::State& state = app.state();

    switch (which) {
    case VK_ESCAPE:
        app.quit();
        break;
    case VK_LEFT:
        if (state.keys[VK_SHIFT]) {
            light.rotateY(1);
        } else {
            camera.rotateY(1);
        }
        break;
    case VK_RIGHT:
        if (state.keys[VK_SHIFT]) {
            light.rotateY(-1);
        } else {
            camera.rotateY(-1);
        }
        break;
    case VK_DOWN:
        if (state.keys[VK_SHIFT]) {
            light.rotateX(1);
        } else {
            camera.rotateX(1);
        }
        break;
    case VK_UP:
        if (state.keys[VK_SHIFT]) {
            light.rotateX(-1);
        } else {
            camera.rotateX(-1);
        }
        break;
    case VK_PRIOR:
        if (state.keys[VK_SHIFT]) {
            light.zoomZ(0.1f);
        } else {
            camera.zoomZ(0.1f);
        }
        break;
    case VK_NEXT:
        if (state.keys[VK_SHIFT]) {
            light.zoomZ(-0.1f);
        } else {
            camera.zoomZ(-0.1f);
        }
        break;
    case '1':
        if (state.keys[VK_SHIFT]) {
            if (light.ambient[0] > 0) {
                light.ambient -= 0.05f;
            }
        } else {
            if (light.ambient[0] < 1) {
                light.ambient += 0.05f;
            }
        }
        break;
    case '2':
        if (state.keys[VK_SHIFT]) {
            if (light.diffuse[0] > 0) {
                light.diffuse -= 0.05f;
            }
        } else {
            if (light.diffuse[0] < 1) {
                light.diffuse += 0.05f;
            }
        }
        break;
    case '3':
        if (state.keys[VK_SHIFT]) {
            if (light.specular[0] > 0) {
                light.specular -= 0.05f;
            }
        } else {
            if (light.specular[0] < 1) {
                light.specular += 0.05f;
            }
        }
        break;
    case '4':
        if (state.keys[VK_SHIFT]) {
            if (objects.shininess > 0) {
                objects.shininess -= 1;
            }
        } else {
            if (objects.shininess < 128) {
                objects.shininess += 1;
            }
        }
        break;
    case '5':
        if (state.keys[VK_SHIFT]) {
            if (objects.ambient[0] > 0) {
                objects.ambient -= 0.05f;
            }
        } else {
            if (objects.ambient[0] < 1) {
                objects.ambient += 0.05f;
            }
        }
        break;
    case '6':
        if (state.keys[VK_SHIFT]) {
            if (objects.diffuse[0] > 0) {
                objects.diffuse -= 0.05f;
            }
        } else {
            if (objects.diffuse[0] < 1) {
                objects.diffuse += 0.05f;
            }
        }
        break;
    case '7':
        if (state.keys[VK_SHIFT]) {
            if (objects.specular[0] > 0) {
                objects.specular -= 0.05f;
            }
        } else {
            if (objects.specular[0] < 1) {
                objects.specular += 0.05f;
            }
        }
        break;
    case 'Q':
        if (!state.keys[VK_SHIFT]) {
            ++objects.sphere.slices;
        } else {
            if (objects.sphere.slices > 4) {
                --objects.sphere.slices;
            }
        }
        break;
    case 'A':
        if (!state.keys[VK_SHIFT]) {
            ++objects.sphere.stacks;
        } else {
            if (objects.sphere.stacks > 4) {
                --objects.sphere.stacks;
            }
        }
        break;
    case 'W':
        if (!state.keys[VK_SHIFT]) {
            ++objects.cylinder.slices;
        } else {
            if (objects.cylinder.slices > 4) {
                --objects.cylinder.slices;
            }
        }
        break;
    case 'S':
        if (!state.keys[VK_SHIFT]) {
            ++objects.cylinder.stacks;
        } else {
            if (objects.cylinder.stacks > 4) {
                --objects.cylinder.stacks;
            }
        }
        break;
    case 'E':
        if (!state.keys[VK_SHIFT]) {
            ++objects.cone.slices;
        } else {
            if (objects.cone.slices > 4) {
                --objects.cone.slices;
            }
        }
        break;
    case 'D':
        if (!state.keys[VK_SHIFT]) {
            ++objects.cone.stacks;
        } else {
            if (objects.cone.stacks > 4) {
                --objects.cone.stacks;
            }
        }
        break;
    case 'R':
        if (!state.keys[VK_SHIFT]) {
            ++objects.cube.slices;
        } else {
            if (objects.cube.slices > 4) {
                --objects.cube.slices;
            }
        }
        break;
    }
}

void Z_22_04::keyReleased(Application&, int) {
}

void Z_22_04::mouseButtonPressed(Application&, int which, int x, int y) {
    if (dragType != DragNone) {
        return;
    }

    switch (which) {
    case Application::MB_Left:
        dragType = DragRotation;
        break;
    case Application::MB_Right:
        dragType = DragPosition;
        break;
    default:
        // Bypass the code below.
        return;
    }

    dragPosition[0] = x;
    dragPosition[1] = y;
    dragSnapped = false;
}

void Z_22_04::mouseButtonReleased(Application& app, int which, int x, int y) {
    switch (which) {
    case Application::MB_Left:
        if (dragType == DragRotation) {
            break;
        }
        return;
    case Application::MB_Right:
        if (dragType == DragPosition) {
            break;
        }
        return;
    default:
        // Bypass the code below.
        return;
    }

    mouseMoved(app, x, y);

    if (!dragSnapped && dragType == DragRotation) {
        // Left mouse button clicked.
        mouseClick(dragPosition[0], dragPosition[1]);
    }

    dragType = DragNone;
}

void Z_22_04::mouseMoved(Application&, int x, int y) {
    if (!dragSnapped) {
        const int xDiff = x - dragPosition[0];
        const int yDiff = y - dragPosition[1];
        const int lenSquare = xDiff*xDiff + yDiff*yDiff;
        if (lenSquare < 16) {
            return;
        }
        dragSnapped = true;
    }
    switch (dragType) {
    case DragRotation:
        mouseRotate(x - dragPosition[0], y - dragPosition[1]);
        break;
    case DragPosition:
        mouseZoom(x - dragPosition[0], y - dragPosition[1]);
        break;
    default:
        // Bypass the code below.
        return;
    }
    
    dragPosition[0] = x;
    dragPosition[1] = y;
}

void Z_22_04::mouseRotate(int x, int y) {
    const Application::State& state = Application::instance().state();
    const Matrix<float, 1, 2> diff = CMatrix<float, 1, 2>::create(
        float(x)/float(state.width),
        float(y)/float(state.height)
    );

    if (state.keys[VK_SHIFT]) {
        light.rotateY(diff[0]*180.0f);
        light.rotateX(diff[1]*120.0f);
    } else {
        camera.rotateY(diff[0]*180.0f);
        camera.rotateX(diff[1]*120.0f);
    }
}

void Z_22_04::mouseZoom(int x, int y) {
    const Application::State& state = Application::instance().state();
    const Matrix<float, 1, 2> diff = CMatrix<float, 1, 2>::create(
        float(x)/float(state.width),
        float(y)/float(state.height)
    );

    if (state.keys[VK_SHIFT]) {
        light.zoomZ(diff[1]*8.0f);
    } else {
        camera.zoomZ(diff[1]*8.0f);
    }
}

void Z_22_04::mouseClick(int x, int y) {
    const Application::State& state = Application::instance().state();

    if (!state.keys[VK_CONTROL]) {
        objects.deselectAll();
    }

    Renderer<Z_22_04::Objects>::Context ctx = {&camera, true};
    ctx.selectMatrix = pickMatrix(
        CMatrix<float, 1, 2>::create(
            float(x)/float(state.width)*2.0f - 1.0f,
            -(float(y)/float(state.height)*2.0f - 1.0f)),
        CMatrix<float, 1, 2>::create(
            1.0f/float(state.width)*2.0f,
            1.0f/float(state.height)*2.0f));

    glDisable(GL_DEPTH_TEST);
    renderer->draw(ctx);
    glEnable(GL_DEPTH_TEST);

    global() << "hits: " << ctx.hits;
    if (ctx.hits == 0) {
        return;
    }
    GLuint* ptr = ctx.buffer;

    int minName;
    float minZ = std::numeric_limits<float>::infinity();

    //FIXME magic number 5 = number of names + 1
    bool tab[5] = {false, false, false, false, false};

    for (int i = 0; i < ctx.hits; ++i) {
        int names = *ptr++;
        float zFrom = float(*ptr++)/0x7FFFFFFF;
        float zTo = float(*ptr++)/0x7FFFFFFF;
        global() << (i+1) << ". Number of names: " << names;
        global() << (i+1) << ". From: " << zFrom;
        global() << (i+1) << ". To: " << zTo;

        if (names == 0) {
            continue;
        }

        if (zFrom < minZ) {
            minZ = zFrom;
            minName = *ptr;
        }

        for (int j = 0; j < names; ++j) {
            int name = *ptr++;
            global() << (i+1) << "." << (j+1) << ". Name: " << name;

            tab[name] = true;
        }
    }

    if (state.keys[VK_SHIFT]) {
        if (!state.keys[VK_CONTROL]) {
            objects.select(Objects::Names(minName));
        } else {
            objects.invertSelection(Objects::Names(minName));
        }
        return;
    }

    for (int i = 1; i < 5; ++i) {
        if (!tab[i]) {
            continue;
        }
        if (!state.keys[VK_CONTROL]) {
            objects.select(Objects::Names(i));
        } else {
            objects.invertSelection(Objects::Names(i));
        }
    }
}

void Z_22_04::update(Application&, long) {
}

void Z_22_04::repaint(Application&) {
    camera.recomputeMatrices();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    Renderer<Z_22_04::Objects>::Context ctx = {&camera, false};
    renderer->draw(ctx);
}
