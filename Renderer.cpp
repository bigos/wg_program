/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#include "Renderer.h"

#include "GL.h"
#include "Log.h"

#include <assert.h>

const char* RendererManager::getVersionName(Version version) {
    switch (version) {
    case Version_1_1:
        return "1.1";
    case Version_4_3:
        return "4.2+extensions";
    default:
        assert(!"Wrong version");
        return 0;
    }
}

void RendererManager::init(Version required, bool force) {
    if (force && required != Version_Auto) {
        m_version = required;
        global() << "Forced OpenGL version to " << getVersionName(required) << ".";
        return;
    }

    switch (required) {
    case Version_1_1:
        check1_1();
        m_version = Version_1_1;
        break;
    case Version_4_3:
        check4_3();
        m_version = Version_4_3;
        break;
    case Version_Auto:
        try {
            check4_3();
            m_version = Version_4_3;
            break;
        } catch (RequirementError) {
        }
        try {
            check1_1();
            m_version = Version_1_1;
            break;
        } catch (RequirementError) {
        }
        assert(!"Shouldn't happen");
        break;
    default:
        assert(!"Wrong version");
    }
}

#define CHECK_VERSION(MAJOR, MINOR) \
    if (!GLEW_VERSION_##MAJOR##_##MINOR) { \
        std::ostringstream oss; \
        oss << "Insufficient OpenGL version ("; \
        GLint major, minor; \
        glGetIntegerv(GL_MAJOR_VERSION, &major); \
        glGetIntegerv(GL_MINOR_VERSION, &minor); \
        oss << major << "." << minor; \
        oss << " < " #MAJOR "." #MINOR ")"; \
        throw RequirementError(oss.str()); \
    }

#define CHECK_EXTENSION(EXT) \
    if (!GLEW_##EXT) { \
        throw RequirementError("OpenGL extension " #EXT " not found"); \
    }

bool RendererManager::check1_1() {
    // Always supported.
    return true;
}

bool RendererManager::check4_3() {
    if (GLEW_VERSION_4_3) {
        return true;
    }
    CHECK_VERSION(4, 2)

    CHECK_EXTENSION(ARB_fragment_layer_viewport)
    CHECK_EXTENSION(ARB_multi_draw_indirect)
    CHECK_EXTENSION(ARB_texture_buffer_range)
    CHECK_EXTENSION(ARB_texture_storage_multisample)

    return true;
}

#undef CHECK_EXTENSION
#undef CHECK_VERSION
