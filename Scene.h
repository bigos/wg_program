/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#ifndef Scene_h
#define Scene_h

#include <string>

class Application;

class Scene {
public:
    struct Configuration {
        int width, height;
        bool fullscreen, resizable;
        bool compatibilityProfile;
    };

    static const Configuration DefaultConfiguration;

    class Factory {
    public:
        void init();

        Scene* create(const std::string& name) const;
    };

    Scene(const std::string& name) : name(name) {}

    const std::string name;

    virtual const Configuration& getConfiguration() const;
    virtual void load(Application&) = 0;
    virtual void unload(Application&) = 0;
    virtual void activate(Application&) = 0;
    virtual void suspend(Application&) = 0;
    virtual void resized(Application&) = 0;
    virtual void keyPressed(Application&, int which) = 0;
    virtual void keyReleased(Application&, int which) = 0;
    virtual void mouseButtonPressed(Application&, int which, int x, int y);
    virtual void mouseButtonReleased(Application&, int which, int x, int y);
    virtual void mouseMoved(Application&, int x, int y);
    virtual void update(Application&, long diff) = 0;
    virtual void repaint(Application&) = 0;
};

#endif
