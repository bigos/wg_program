/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#ifndef Wolf3D_h
#define Wolf3D_h

#include "Texture.h"

#include <assert.h>
#include <stdint.h>
#include <vector>

namespace Wolf3D {

class Color {
public:
	Color() : value(0) {}
    explicit Color(uint8_t v) : value(v) {}

	operator uint8_t() const { return value; }

private:
	uint8_t value;
};

enum Orientation {
	FaceNorth,
	FaceSouth,
	FaceWest,
	FaceEast,
    FaceNone
};

class Position {
public:
	static const uint8_t MaxSize = 63;

	Position() : m_x(0), m_y(0) {}
	Position(uint8_t x, uint8_t y) : m_x(x), m_y(y) {
		assert(x <= MaxSize);
		assert(y <= MaxSize);
	}

	uint8_t x() const { return m_x; }
	uint8_t y() const { return m_y; }

    Position moved(Orientation o) const {
        switch (o) {
        case FaceNorth:
            return Position(x(), y()-1);
        case FaceSouth:
            return Position(x(), y()+1);
        case FaceWest:
            return Position(x()-1, y());
        case FaceEast:
            return Position(x()+1, y());
        case FaceNone:
            return *this;
        default:
            assert(!"Impossible");
            return Position();
        }
    }

    class Hash {
    public:
        size_t operator() (const Position& pos) const {
            return pos.x() * 0x010000100 | pos.y() * 0x00010001;
        }
    };

private:
	uint8_t m_x, m_y;
};

inline bool operator == (const Position& a, const Position& b) {
    return a.x() == b.x() && a.y() == b.y();
}

template <class Catalog>
class TexturePosition {
public:
	static const uint8_t MaxX = Catalog::MaxX;
	static const uint8_t MaxY = Catalog::MaxY;

	TexturePosition() : m_x(0), m_y(0) {}
	TexturePosition(unsigned num) : m_x(num % (MaxX+1)), m_y(num / (MaxX+1)) {
		assert(num < (MaxX+1)*(MaxY+1));
	}
	TexturePosition(uint8_t x, uint8_t y) : m_x(x), m_y(y) {
		assert(x <= MaxX);
		assert(y <= MaxY);
	}

	uint8_t x() const { return m_x; }
	uint8_t y() const { return m_y; }

    friend bool operator == (const TexturePosition& a, const TexturePosition& b) {
        return a.x() == b.x() && a.y() == b.y();
    }

private:
	uint8_t m_x, m_y;
};

} // end of Wolf3D

#endif
