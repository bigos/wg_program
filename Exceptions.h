/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#ifndef Exceptions_h
#define Exceptions_h

#include <exception>
#include <iosfwd>
#include <string>

class Exception : public std::exception {
public:
    Exception(const char* title) : title(title), whatStorage(0) {}
    virtual ~Exception() throw();

    const char* const title;
    
    virtual void describe(std::ostream& os) const throw() = 0;

    virtual const char* what() const throw();

private:
    mutable char* whatStorage;
};

inline std::ostream& operator<< (std::ostream& os, const Exception& e) {
    e.describe(os);
    return os;
}

class WinApiException : public Exception {
public:
    WinApiException(const char* function = 0);

    virtual void describe(std::ostream& os) const throw();

    const char* const function;
    const int errorNumber;
};

class RequirementError : public Exception {
public:
    RequirementError(const std::string& message) :
        Exception("Requirement Error"), message(message) {}

    virtual void describe(std::ostream& os) const throw();

    const std::string message;
};

#endif
