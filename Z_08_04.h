/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#ifndef Z_08_04_h
#define Z_08_04_h

#include "Application.h"
#include "Camera.h"
#include "GL.h"
#include "Matrix.h"
#include "Scene.h"

class Z_08_04 : public Scene {
    static const Configuration TheConfiguration;

public:
    class Camera : public ::Camera {
    public:
        enum Mode {
            FPP,
            TPP,
            TPP_Behind,
            NumModes
        } mode;

        void init();
        
        void recomputeMatrices();
        
        void move(float t);
        void rotateX(float degrees);
        void rotateY(float degrees);
        void nextMode();

        float rotX, rotY;
        Matrix<float, 1, 3> position;
    };

    class Side {
    public:
        void init(const Matrix<float, 1, 3>& dimen, const Matrix<float, 1, 3>& color) {
            this->dimen = dimen;
            this->color = color;
        }

        Matrix<float, 1, 3> dimen, color;
    };

    class HUD {
    public:
        void init(Camera& camera) { this->camera = &camera; }
        
        Camera* camera;
    };

    Z_08_04() : Scene("Z 08.04.2013") {}
    
    virtual const Configuration& getConfiguration() const;
    virtual void load(Application&);
    virtual void unload(Application&);
    virtual void activate(Application&);
    virtual void suspend(Application&);
    virtual void resized(Application&);
    virtual void keyPressed(Application&, int which);
    virtual void keyReleased(Application&, int which);
    virtual void update(Application&, long diff);
    virtual void repaint(Application&);

private:
    Camera camera;
    HUD hud;
    Side sides[6];
    Renderer<HUD>* hudRenderer;
    Renderer<Side>* sideRenderer;
    unsigned* image;
};

#endif
