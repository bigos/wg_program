/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#include "Exceptions.h"

#include "GL.h"
#include "System.h"

#include <ostream>
#include <sstream>

Exception::~Exception() {
    if (whatStorage) {
        delete[] whatStorage;
    }
}

const char* Exception::what() const {
    if (!whatStorage) {
        std::ostringstream os;
        describe(os);
        size_t size = os.str().size();
        whatStorage = new char[size+1];
        memcpy(whatStorage, os.str().data(), size);
        whatStorage[size] = 0;
    }
    return whatStorage;
}

WinApiException::WinApiException(const char* function) :
  Exception("WinApi Error"),
  function(function),
  errorNumber(GetLastError()) {
}

void WinApiException::describe(std::ostream& os) const {
    os << "WinApi error";
    if (function) {
        os << " from function " << function;
    }
    LPWSTR msg = 0;
    FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM
        | FORMAT_MESSAGE_IGNORE_INSERTS, NULL, errorNumber,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPWSTR)&msg, 0, NULL);
    
    os << " (" << errorNumber << "):\n" << (msg ? narrow(msg) : "Unknown error.");
    if (msg) {
        LocalFree(msg);
    }
}

void RequirementError::describe(std::ostream& os) const {
    os << "Requirement error: " << message << ".";
}

void GlewError::describe(std::ostream& os) const {
    os << "GLEW error: " << glewGetErrorString(error);
}
