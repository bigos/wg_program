/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#include "Matrix.h"

Matrix<float, 4, 4> translationMatrix(float x, float y, float z) {
    const float values[16] = {
        1, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, 1, 0,
        x, y, z, 1
    };
    Matrix<float, 4, 4> ret(values);
    return ret;
}

Matrix<float, 4, 4> rotationMatrix(float angle, float x, float y, float z) {
    float c = cos(angle), s = sin(angle), c_ = 1 - c;

    const float values[16] = {
        x*x*c_+c, y*x*c_+z*s, x*z*c_-y*s, 0,
        x*y*c_-z*s, y*y*c_+c, y*z*c_+x*s, 0,
        x*z*c_+y*s, y*z*c_-x*s, z*z*c_+c, 0,
        0, 0, 0, 1
    };
    Matrix<float, 4, 4> ret(values);
    return ret;
}

Matrix<float, 2, 2> rotationMatrix(float angle) {
    float s = sin(angle), c = cos(angle);
    const float values[4] = {
         c,  s,
        -s,  c
    };
    return Matrix<float, 2, 2>(values);
}

Matrix<float, 4, 4> scalingMatrix(float x, float y, float z) {
    const float values[16] = {
        x, 0, 0, 0,
        0, y, 0, 0,
        0, 0, z, 0,
        0, 0, 0, 1
    };
    Matrix<float, 4, 4> ret(values);
    return ret;
}

Matrix<float, 4, 4> orthogonalMatrix(float xMin, float xMax,
                                     float yMin, float yMax,
                                     float zMin, float zMax) {

    float xSpan = xMax - xMin,
          ySpan = yMax - yMin,
          zSpan = zMax - zMin,
          xCenter = xMax + xMin,
          yCenter = yMax + yMin,
          zCenter = zMax + zMin;
    const float values[16] = {
        2/xSpan, 0, 0, 0,
        0, 2/ySpan, 0, 0,
        0, 0, -2/zSpan, 0,
        -xCenter/xSpan, -yCenter/ySpan, zCenter/zSpan, 1
    };
    Matrix<float, 4, 4> ret(values);
    return ret;
}

Matrix<float, 4, 4> perspectiveMatrix(float fieldOfView, float aspectRatio,
                                      float zNear, float zFar) {
    float ct = 1/tan(fieldOfView/2);
    float zSpan = zFar - zNear,
          zCenter = zFar + zNear;
    const float values[16] = {
        ct/aspectRatio, 0, 0, 0,
        0, ct, 0, 0,
        0, 0, -zCenter/zSpan, -1,
        0, 0, -2*zNear*zFar/zSpan, 0
    };
    Matrix<float, 4, 4> ret(values);
    return ret;
}

Matrix<float, 4, 4> frustumMatrix(float xMin, float xMax,
                                  float yMin, float yMax,
                                  float zMin, float zMax) {

    float xSpan = xMax - xMin,
          ySpan = yMax - yMin,
          zSpan = zMax - zMin,
          xCenter = xMax + xMin,
          yCenter = yMax + yMin,
          zCenter = zMax + zMin;
    const float values[16] = {
        2*zMin/xSpan, 0, 0, 0,
        0, 2*zMin/ySpan, 0, 0,
        xCenter/xSpan, yCenter/ySpan, -zCenter/zSpan, -1,
        0, 0, -2*zMin*zMax/zSpan, 0
    };
    Matrix<float, 4, 4> ret(values);
    return ret;
}

Matrix<float, 4, 4> lookAtMatrix(const Matrix<float, 1, 3>& eye,
                                 const Matrix<float, 1, 3>& center,
                                 const Matrix<float, 1, 3>& _up) {

    Matrix<float, 1, 3> F = normalize(center - eye);
    Matrix<float, 1, 3> up = normalize(_up);
    Matrix<float, 1, 3> s = cross(F, up);
    Matrix<float, 1, 3> u = cross(s, F);

    const float values[16] = {
        s[0], u[0], -F[0], 0,
        s[1], u[1], -F[1], 0,
        s[2], u[2], -F[2], 0,
        0, 0, 0, 1
    };
    Matrix<float, 4, 4> ret(values);
    ret *= translationMatrix(-eye);
    return ret;
}

Matrix<float, 4, 4> pickMatrix(const Matrix<float, 1, 2>& position,
                               const Matrix<float, 1, 2>& radius) {

    const Matrix<float, 1, 2> radiusRecip = 1.0f/radius;
    const float values[16] = {
        radiusRecip[0], 0, 0, 0,
        0, radiusRecip[1], 0, 0,
        0, 0, 1, 0,
        -position[0]*radiusRecip[0], -position[1]*radiusRecip[1], 0, 1
    };
    Matrix<float, 4, 4> ret(values);
    return ret;
}
