/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#include "Asteroids.h"

#include "Application.h"
#include "Log.h"

#include <algorithm>
#include <time.h>

const Scene::Configuration Asteroids::AsteroidsConfiguration = {
    // 800x600 unresizable window. Compatibility profile.
    800, 600,
    false, false,
    true
};

const float Asteroids::Ship::maxSpeed = 0.1f;
const float Asteroids::Ship::minSpeed = -0.05f;
const float Asteroids::Ship::rotSpeed = 0.0012f;
const float Asteroids::Ship::shotSpeed = 0.3f;
const float Asteroids::Ship::lifeLoseRate = 0.0007f;
const long Asteroids::Ship::shotDelay = 100L;

const float Asteroids::Rock::lifeLoseRate = 0.002f;
const long Asteroids::Rock::protectionDelay = 1000L;

bool Asteroids::outOfWindow(const Matrix<float, 1, 2>& pos) {
    if (pos[0] > 400.0f) {
        return true;
    } else if (pos[0] < -400.0f) {
        return true;
    }

    if (pos[1] > 300.0f) {
        return true;
    } else if (pos[1] < -300.0f) {
        return true;
    }

    return false;
}

void Asteroids::Object::tickNoWrap(long diff) {
    float rotation = rotationalSpeed * diff;
    if (rotation != 0) {
        rotMatrix *= rotationMatrix(rotation);
    }

    position += (speedVector * (directionalSpeed * diff));
}

void Asteroids::Object::tick(long diff) {
    tickNoWrap(diff);
    if (position[0] > 400) {
        position[0] -= 800;
    } else if (position[0] < -400) {
        position[0] += 800;
    }

    if (position[1] > 300) {
        position[1] -= 600;
    } else if (position[1] < -300) {
        position[1] += 600;
    }
}

bool Asteroids::Object::collision(const Object& other) const {
    Matrix<float, 1, 2> delta = abs(position - other.position);
    if (delta[0] > 400.0f) {
        delta[0] = 800.0f - delta[0];
    }
    if (delta[1] > 300.0f) {
        delta[1] = 600.0f - delta[1];
    }

    const float len = length(delta);
    if (len < collisionSize + other.collisionSize) {
        return true;
    }

    return false;
}

bool Asteroids::Object::outOfWindow() const {
    Matrix<float, 1, 2> nearest = position;
    if (position[0] == 0) {
        nearest[1] -= sign(position[1])*visibleSize;
    } else {
        float a = position[1]/position[0];
        float dx = visibleSize / sqrt(1.0f + a*a);
        nearest[0] -= sign(position[0])*dx;
        nearest[1] -= sign(position[0])*dx*a;
    }

    return Asteroids::outOfWindow(nearest);
}

namespace {
    template <class RealObject>
    class ObjectRenderer : public Renderer<RealObject> {
    public:
        virtual ~ObjectRenderer() {}

        virtual void init(const RealObject* object, size_t num) {
            this->object = object;
            this->num = num;
        }

        virtual void draw(DefaultContext& context) {
            glMatrixMode(GL_PROJECTION);
            glLoadMatrixf(context.camera->matrix().data);
            glMatrixMode(GL_MODELVIEW);
            glPushMatrix();
            
            for (size_t i = 0; i < num; ++i) {
                const RealObject& object = this->object[i];
                const Matrix<float, 4, 4> rotMatrix = CMatrix<float, 4, 4>::create(
                    object.rotMatrix(0, 0), object.rotMatrix(0, 1), 0, 0,
                    object.rotMatrix(1, 0), object.rotMatrix(1, 1), 0, 0,
                    0, 0, 1, 0,
                    0, 0, 0, 1
                );
            
                glLoadIdentity();
                glLoadMatrixf((translationMatrix(object.position[0], object.position[1], 0) *
                               rotMatrix)
                              .data);

                ((Renderer_1_1<RealObject>*)this)->drawObject(object);
            }
            glPopMatrix();
        }

    protected:
        const RealObject* object;
        size_t num;
    };
}

void Asteroids::Ship::init(Missiles& missiles) {
    position = CMatrix<float, 1, 2>::create(0, 0);
    speedVector = CMatrix<float, 1, 2>::create(0, 1);
    rotMatrix = rotationMatrix(M_PI/2);
    directionalSpeed = maxSpeed;
    rotationalSpeed = 0;
    visibleSize = collisionSize = 15.0f;
    
    this->missiles = &missiles;
    life = 1.0f;
    shotTick = 0;
    newEvent = 0;
}

void Asteroids::Ship::rotate(float angle) {
    const Matrix<float, 2, 2> matrix = rotationMatrix(angle);
    rotMatrix *= matrix;
    speedVector = matrix * speedVector;
}

void Asteroids::Ship::tick(long diff) {
    if (newEvent > 0) {
        life -= diff*lifeLoseRate;
        newEvent -= diff;
        if (newEvent > 0) {
            return;
        }
        diff = -newEvent;
        init(*missiles);
    }

    Object::tick(diff);
    tryShooting();
}

void Asteroids::Ship::startShooting() {
    if (newEvent > 0) {
        return;
    }
    if (shotTick != 0) {
        return;
    }
    shotTick = GetTickCount64() - shotDelay;
}

void Asteroids::Ship::stopShooting() {
    tryShooting();
    shotTick = 0;
}

void Asteroids::Ship::kill() {
    if (newEvent > 0) {
        return;
    }
    shotTick = 0;
    newEvent = 2000;
}

void Asteroids::Ship::tryShooting() {
    if (shotTick == 0) {
        return;
    }

    ULONGLONG now = GetTickCount64();
    if (!alive()) {
        shotTick = now;
        return;
    }
    long diff = long(now - shotTick);
    
    while (diff >= shotDelay) {
        missiles->fire(position+speedVector*15.0f, speedVector*shotSpeed);
        diff -= shotDelay;
    }
    shotTick = now - diff;
}

template <>
class Renderer_1_1<Asteroids::Ship> : public ObjectRenderer<Asteroids::Ship> {
public:
    void drawObject(const Asteroids::Ship& ship) {
        if (ship.life == 0) {
            return;
        }
        glColor3f(ship.life, ship.life, ship.life);
        
        glPointSize(3.0f);
        glBegin(GL_POINTS);
            glVertex2f(0, 0);
        glEnd();

        glLineWidth(1.0f);
        glBegin(GL_LINE_LOOP);
            glVertex2f( 15.0f,   0.0f);
            glVertex2f(-15.0f, -10.0f);
            glVertex2f( -5.0f,   0.0f);
            glVertex2f(-15.0f, +10.0f);
            //glVertex3f(0.0f, 10.0f, 0.0f);
            //glVertex3f(10.0f, -10.0f, 0.0f);
            //glVertex3f(0.0f, -5.0f, 0.0f);
            //glVertex3f(-10.0f, -10.0f, 0.0f);
        glEnd();
    }
};

static float frand() {
    return float(rand()) / RAND_MAX;
}

void Asteroids::Rock::init() {
    unsigned side = rand();
    float rotation;
    if (side & 1) {
        position = CMatrix<float, 1, 2>::create(-450.0f, frand()*600.0f-300.0f);
        rotation = (frand() - 0.5f)*M_PI;
    } else {
        position = CMatrix<float, 1, 2>::create(frand()*800.0f-400.0f, -350.0f);
        rotation = frand()*M_PI;
    }
    if (side & 2) {
        position *= -1;
        rotation += M_PI;
    }

    speedVector = CMatrix<float, 1, 2>::create(cos(rotation), sin(rotation));
    directionalSpeed = (frand()*2+1)*Ship::maxSpeed;
    rotationalSpeed = Ship::rotSpeed*(frand() - 0.5f);
    collisionSize = 40.0f;
    visibleSize = 65.0f;

    life = 1.0f;
    newEvent = 0;
    protection = protectionDelay;
}

void Asteroids::Rock::tick(long diff) {
    if (newEvent > 0) {
        life -= diff*lifeLoseRate;
        newEvent -= diff;
        if (newEvent > 0) {
            return;
        }
        diff = -newEvent;
        init();
    }
    protection = std::max<long>(protection - diff, 0);
    Object::tickNoWrap(diff);

    if (protection == 0 && outOfWindow()) {
        kill();
    }
}

void Asteroids::Rock::kill() {
    if (newEvent > 0) {
        return;
    }
    newEvent = 1000 + long(frand() * 2000);
}

template <>
class Renderer_1_1<Asteroids::Rock> : public ObjectRenderer<Asteroids::Rock> {
public:
    void drawObject(const Asteroids::Rock& rock) {
        if (rock.life == 0) {
            return;
        }
        glColor3f(rock.life, rock.life, rock.life);

        glPointSize(3.0f);
        glBegin(GL_POINTS);
            glVertex2f(0, 0);
        glEnd();

        glLineWidth(1.0f);
        glBegin(GL_LINE_LOOP);
            glVertex2f( 50.0f, -10.0f);
            glVertex2f( 20.0f, -50.0f);
            glVertex2f( -5.0f, -50.0f);
            glVertex2f( -5.0f, -25.0f);
            glVertex2f(-30.0f, -50.0f);
            glVertex2f(-50.0f, -10.0f);
            glVertex2f(-25.0f,   0.0f);
            glVertex2f(-50.0f,  10.0f);
            glVertex2f(-15.0f, +45.0f);
            glVertex2f( 20.0f, +45.0f);
            /*glVertex2f( 0.0f,   52.0f);
            glVertex2f( 18.0f,  68.0f);
            glVertex2f( 32.0f,  51.0f);
            glVertex2f( 30.0f,  32.0f);
            glVertex2f( 55.0f,  27.0f);
            glVertex2f( 60.0f, -17.0f);
            glVertex2f( 26.0f, -35.0f);
            glVertex2f(  5.0f, -15.0f);
            glVertex2f(-19.0f, -17.0f);
            glVertex2f(-37.0f, -42.0f);
            glVertex2f(-59.0f, -13.0f);
            glVertex2f(-60.0f,  26.0f);
            glVertex2f(-36.0f,  19.0f);
            glVertex2f(-61.0f,  60.0f);
            glVertex2f(-38.0f,  87.0f);
            glVertex2f(-27.0f,  76.0f);
            glVertex2f(- 9.0f,  89.0f);*/
        glEnd();
    }
};

void Asteroids::Missiles::Missile::tick(long diff) {
    position += speed * float(diff);
}

bool Asteroids::Missiles::Missile::collision(const Object& obj) const {
    Matrix<float, 1, 2> delta = abs(position - obj.position);

    const float len = length(delta);
    if (len < obj.collisionSize) {
        return true;
    }

    return false;
}

void Asteroids::Missiles::init(const Matrix<float, 1, 3>& color) {
    clear();
    this->color = color;
}

void Asteroids::Missiles::clear() {
    missiles.clear();
}

namespace {
    class MoveMissile {
    public:
        const long diff;
        MoveMissile(long diff) : diff(diff) {}

        bool operator() (Asteroids::Missiles::Missile& missile) {
            missile.tick(diff);

            return Asteroids::outOfWindow(missile.position);
        }
    };
}

void Asteroids::Missiles::tick(long diff) {
    MoveMissile m(diff);
    forEach(m);
}

void Asteroids::Missiles::fire(const Matrix<float, 1, 2>& position,
                               const Matrix<float, 1, 2>& speed) {
    missiles.push_back(Missile(position, speed));
}

template <>
class Renderer_1_1<Asteroids::Missiles> : public Renderer<Asteroids::Missiles> {
public:
    virtual ~Renderer_1_1() {}

    virtual void init(const Asteroids::Missiles* object, size_t num) {
        missiles = object;
        this->num = num;
    }

    virtual void draw(DefaultContext& context) {
        glMatrixMode(GL_PROJECTION);
        glLoadMatrixf(context.camera->matrix().data);

        glPointSize(2.0f);
        glBegin(GL_POINTS);
        for (size_t i = 0; i < num; ++i) {
            const Asteroids::Missiles& missiles = this->missiles[i];

            glColor3f(missiles.color[0], missiles.color[1], missiles.color[2]);
            
            for (size_t j = 0, S = missiles.missiles.size(); j < S; ++j) {
                const Asteroids::Missiles::Missile& missile = missiles.missiles[j];

                glVertex2f(missile.position[0], missile.position[1]);
            }
        }
        glEnd();
    }

private:
    const Asteroids::Missiles* missiles;
    size_t num;
};

const Scene::Configuration& Asteroids::getConfiguration() const {
    return AsteroidsConfiguration;
}

void Asteroids::load(Application& app) {
    camera.setProjectionMatrix(orthogonalMatrix(-400, 400, -300, 300, -1, 1));

    ship.init(missiles);
    shipRenderer = app.state().rendererManager.create<Ship>();
    shipRenderer->init(&ship);

    rock.init();
    rockRenderer = app.state().rendererManager.create<Rock>();
    rockRenderer->init(&rock);
    
    missiles.init(CMatrix<float, 1, 3>::create(1.0f, 1.0f, 1.0f));
    missilesRenderer = app.state().rendererManager.create<Missiles>();
    missilesRenderer->init(&missiles);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    srand((unsigned)time(0));
}

void Asteroids::unload(Application& app) {
    delete missilesRenderer;
    delete rockRenderer;
    delete shipRenderer;

    missiles.clear();
}

void Asteroids::activate(Application&) {
}

void Asteroids::suspend(Application&) {
}

void Asteroids::resized(Application&) {
}

void Asteroids::keyPressed(Application& app, int which) {
    const Application::State& state = app.state();

    switch (which) {
    case VK_ESCAPE:
        app.quit();
        break;
    case VK_DOWN:
        ship.directionalSpeed = ship.minSpeed;
        break;
    case VK_UP:
        ship.directionalSpeed = ship.maxSpeed;
        break;
    case VK_SPACE:
        ship.startShooting();
        break;
    }
}

void Asteroids::keyReleased(Application&, int which) {
    switch (which) {
    case VK_SPACE:
        ship.stopShooting();
        break;
    }
}

namespace {
    class MissileCollision {
    public:
        Asteroids::Object& object;
        bool collided;
        MissileCollision(Asteroids::Object& object) :
            object(object), collided(false) {}

        bool operator() (Asteroids::Missiles::Missile& missile) {
            if (missile.collision(object)) {
                collided = true;
                return true;
            }

            return false;
        }
    };
}

void Asteroids::update(Application& app, long diff) {
    if (ship.alive()) {
        int dir = 0;
        if (app.state().keys[VK_LEFT]) {
            ++dir;
        }
        if (app.state().keys[VK_RIGHT]) {
            --dir;
        }
        ship.rotate(diff*dir*Ship::rotSpeed);

        if (!app.state().keys[VK_SPACE]) {
            ship.stopShooting();
        }
    }

    missiles.tick(diff);
    rock.tick(diff);
    ship.tick(diff);
    if (ship.alive() && rock.alive() && ship.collision(rock)) {
        ship.kill();
    }
    
    if (rock.alive()) {
        MissileCollision mc(rock);
        missiles.forEach(mc);
        if (mc.collided) {
            rock.kill();
        }
    }
}

void Asteroids::repaint(Application&) {
    camera.recomputeMatrices();

    glClear(GL_COLOR_BUFFER_BIT);

    DefaultContext ctx = {&camera};
    rockRenderer->draw(ctx);
    shipRenderer->draw(ctx);
    missilesRenderer->draw(ctx);
}
