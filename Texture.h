/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#ifndef Texture_h
#define Texture_h

#include "GL.h"

#include <assert.h>
#include <stdint.h>
#include <string.h>

namespace Texture {

class Texture {
public:
    Texture() : m_texture(0) {}

    void destroy();

    void bind(GLenum point);
    static void unbind(GLenum point);

private:
    GLuint m_texture;

    void create();
};

template <unsigned Dimensions>
class Size {
public:
	Size() { memset(size, 0, sizeof size); }
	Size(GLsizei size[Dimensions]) : size(size) {}
	Size(GLsizei);
	Size(GLsizei, GLsizei);
	Size(GLsizei, GLsizei, GLsizei);

	GLsizei width() const {
		static_assert(Dimensions > 0, "Wrong Dimensions");
		return size[0];
	}
	GLsizei height() const {
		static_assert(Dimensions > 1, "Wrong Dimensions");
		return size[1];
	}
	GLsizei depth() const {
		static_assert(Dimensions > 2, "Wrong Dimensions");
		return size[2];
	}

	GLsizei operator[] (unsigned x) {
		assert(x < Dimensions);
		return size[x];
	}

private:
	GLsizei size[Dimensions];
};

template <> inline
Size<1>::Size(GLsizei s) {
    size[0] = s;
}

template <> inline
Size<2>::Size(GLsizei s, GLsizei t) {
    size[0] = s;
    size[1] = t;
}

template <> inline
Size<3>::Size(GLsizei s, GLsizei t, GLsizei r) {
    size[0] = s;
    size[1] = t;
    size[2] = r;
}

class Data {
public:
	Data() : m_format(GL_RGBA), m_type(GL_UNSIGNED_BYTE), m_data(nullptr) {}
	Data(GLenum format, GLenum type, const void* data) :
		m_format(format), m_type(type), m_data(data) {}

	void free();

	static Data fromBMP(const std::string& name, unsigned width, unsigned height, uint32_t alphaColor = -1);

	GLenum format() const { return m_format; }
	GLenum type() const { return m_type; }
	const void* data() const { return m_data; }

protected:
	GLenum m_format, m_type;
	const void* m_data;
};

namespace detail {
	template <unsigned Dimensions>
	void allocate(GLenum point, Size<Dimensions> size, GLenum internalFormat, const Data& data);

	template <unsigned Dimensions>
	void update(GLenum point, GLint level, Size<Dimensions> offset, Size<Dimensions> size, const Data& data);

	template <typename Type>
	void setParameterS(GLenum point, GLenum name, Type param);

	template <typename Type>
	void setParameterV(GLenum point, GLenum name, const Type* param);
}

template <GLenum Point, unsigned Dimensions>
class Bind {
	typedef Size<Dimensions> SizeD;

protected:
    explicit Bind(Texture& t) : texture(t) { texture.bind(Point); }

public:
    ~Bind() { texture.unbind(Point); }

    static void bind(Texture& t) {
        t.bind(Point);
    }

    static void bind() {
        Texture::unbind(Point);
    }

    void allocate(SizeD size, GLenum internalFormat, const Data& data = Data()) {
		detail::allocate(Point, size, internalFormat, data);
	}
	void update(GLint level, SizeD offset, SizeD size, const Data& data) {
		detail::update(Point, level, offset, size, data);
	}
    void update(SizeD offset, SizeD size, const Data& data) {
		update(0, offset, size, data);
	}

	template <typename Type, size_t N>
	void setParameter(GLenum name, Type param[N]) { detail::setParameterV(Point, name, param); }

	template <typename Type>
	void setParameter(GLenum name, Type param) { detail::setParameterS(Point, name, param); }

	void setMinFilter(GLenum filter) { setParameter<GLint>(GL_TEXTURE_MIN_FILTER, filter); }
	void setMagFilter(GLenum filter) { setParameter<GLint>(GL_TEXTURE_MAG_FILTER, filter); }
	void setMinLod(GLint lod) { setParameter<GLint>(GL_TEXTURE_MIN_LOD, lod); }
	void setMaxLod(GLint lod) { setParameter<GLint>(GL_TEXTURE_MAX_LOD, lod); }
	void setBaseLevel(GLint lod) { setParameter<GLint>(GL_TEXTURE_BASE_LEVEL, lod); }
	void setMaxLevel(GLint lod) { setParameter<GLint>(GL_TEXTURE_MAX_LEVEL, lod); }
	void setPriority(GLclampf priority) { setParameter<GLfloat>(GL_TEXTURE_PRIORITY, priority); }
	void setGenerateMipMap(GLboolean c) { setParameter<GLint>(GL_GENERATE_MIPMAP, c); }
	//TODO
	void setAnisotropy(GLfloat a) { setParameter<GLfloat>(GL_TEXTURE_MAX_ANISOTROPY_EXT, a); }

protected:
    Texture& texture;
};

#ifndef TEXTURE_EXPLICIT
#pragma warning(push)
#pragma warning(disable: 4231)
extern template class Bind<GL_TEXTURE_1D, 1>;
extern template class Bind<GL_TEXTURE_2D, 2>;
extern template class Bind<GL_TEXTURE_3D, 3>;
#pragma warning(pop)
#endif

class Bind1D : public Bind<GL_TEXTURE_1D, 1> {
public:
    Bind1D(Texture& t) : Bind(t) {}

	void setWrapS(GLenum mode) {
		setParameter<GLint>(GL_TEXTURE_WRAP_S, mode);
	}
};

class Bind2D : public Bind<GL_TEXTURE_2D, 2> {
public:
    Bind2D(Texture& t) : Bind(t) {}

	void setWrapS(GLenum mode) {
		setParameter<GLint>(GL_TEXTURE_WRAP_S, mode);
	}
	void setWrapT(GLenum mode) {
		setParameter<GLint>(GL_TEXTURE_WRAP_T, mode);
	}
};

class Bind3D : public Bind<GL_TEXTURE_3D, 3> {
public:
    Bind3D(Texture& t) : Bind(t) {}

	void setWrapS(GLenum mode) {
		setParameter<GLint>(GL_TEXTURE_WRAP_S, mode);
	}
	void setWrapT(GLenum mode) {
		setParameter<GLint>(GL_TEXTURE_WRAP_T, mode);
	}
	void setWrapR(GLenum mode) {
		setParameter<GLint>(GL_TEXTURE_WRAP_R, mode);
	}
};

} // end of Texture

#endif
