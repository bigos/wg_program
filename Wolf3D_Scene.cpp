/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#include "Wolf3D_Scene.h"
//#include "Buffer.h"
//#include "Reserver.h"
//#include "Shader.h"
//#include "Vertex.h"
#include "Log.h"

template <>
class Renderer<Wolf3D::Level> {
public:
    struct Context {
        ::Camera* camera;
        Wolf3D::WallCatalog* wallCatalog;
        Wolf3D::SpriteCatalog* spriteCatalog;
        Wolf3D::DoorCatalog* doorCatalog;
    };

    virtual ~Renderer() {};

    virtual void init(const Wolf3D::Level* object, size_t num = 1) = 0;
    virtual void draw(Context& context) = 0;
};

template <>
class Renderer_1_1<Wolf3D::Level> : public Renderer<Wolf3D::Level> {
public:
    virtual ~Renderer_1_1() {}

    virtual void init(const Wolf3D::Level* object, size_t num) {
        assert(num == 1);
        level = object;
    }

    virtual void draw(Context& context) {
        glMatrixMode(GL_PROJECTION);
        glLoadMatrixf(context.camera->matrix().data);
        glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

        for (unsigned i = 0, S = level->rooms(); i < S; ++i) {
            drawRoom(context, level->room(i));
        }

        {
            Texture::Bind2D t(context.doorCatalog->texture());
            for (unsigned i = 0, S = level->doors(); i < S; ++i) {
                drawDoor(context, level->door(i));
            }
        }
    }

private:
    const Wolf3D::Level* level;

    void drawRoom(Context& context, const Wolf3D::Room& room) {
        drawWalls(context, room.walls());
    }

    void drawWalls(Context& context, const std::vector<Wolf3D::Wall>& walls) {
        typedef const Matrix<float, 1, 2> Position[2];
        static const Position Positions[4] = { {
                CMatrix<float, 1, 2>::create(1.0f, 0.0f),
                CMatrix<float, 1, 2>::create(0.0f, 0.0f)
            }, {
                CMatrix<float, 1, 2>::create(0.0f, 1.0f),
                CMatrix<float, 1, 2>::create(1.0f, 1.0f)
            }, {
                CMatrix<float, 1, 2>::create(0.0f, 0.0f),
                CMatrix<float, 1, 2>::create(0.0f, 1.0f)
            }, {
                CMatrix<float, 1, 2>::create(1.0f, 1.0f),
                CMatrix<float, 1, 2>::create(1.0f, 0.0f)
            }
        };

        Texture::Bind2D t(context.wallCatalog->texture());

        for (unsigned i = 0, S = walls.size(); i < S; ++i) {
            const Wolf3D::Wall& wall = walls[i];

            glPushMatrix();
            glTranslatef(wall.position().x(), 0.0f, wall.position().y());

            Wolf3D::TexturePosition<Wolf3D::WallCatalog> tpos(wall.texture());
            Position& P = Positions[(unsigned)wall.orientation()];
            Matrix<float, 1, 2> p;
            glBegin(GL_QUADS);
                p = context.wallCatalog->position(tpos, CMatrix<float, 1, 2>::create(0.0f, 0.0f));
                glTexCoord2f(p[0], p[1]);
                glVertex3f(P[0][0], 0.0f, P[0][1]);
        
                p = context.wallCatalog->position(tpos, CMatrix<float, 1, 2>::create(1.0f, 0.0f));
                glTexCoord2f(p[0], p[1]);
                glVertex3f(P[1][0], 0.0f, P[1][1]);
        
                p = context.wallCatalog->position(tpos, CMatrix<float, 1, 2>::create(1.0f, 1.0f));
                glTexCoord2f(p[0], p[1]);
                glVertex3f(P[1][0], 1.0f, P[1][1]);
        
                p = context.wallCatalog->position(tpos, CMatrix<float, 1, 2>::create(0.0f, 1.0f));
                glTexCoord2f(p[0], p[1]);
                glVertex3f(P[0][0], 1.0f, P[0][1]);
            glEnd();

            glPopMatrix();
        }
    }

    void drawDoor(Context& context, const Wolf3D::Door& door) {
        glPushMatrix();
        glTranslatef(door.position().x(), 0.0f, door.position().y());
        
        glPushMatrix();
        glTranslatef(-door.openess(), 0.0f, 0.0f);
        
        Matrix<float, 1, 2> p;

        if (door.orientation() == Wolf3D::FaceWest) {
            glTranslatef(0.0f, 0.0f, 1.0f);
            glRotatef(90, 0, 1, 0);
        }

        glBegin(GL_QUADS);
        {
            Wolf3D::TexturePosition<Wolf3D::DoorCatalog> tpos(door.texture());

            p = context.doorCatalog->position(tpos, CMatrix<float, 1, 2>::create(0.0f, 0.0f));
            glTexCoord2f(p[0], p[1]);
            glVertex3f(1.0f, 0.0f, 0.45f);

            p = context.doorCatalog->position(tpos, CMatrix<float, 1, 2>::create(1.0f, 0.0f));
            glTexCoord2f(p[0], p[1]);
            glVertex3f(0.0f, 0.0f, 0.45f);

            p = context.doorCatalog->position(tpos, CMatrix<float, 1, 2>::create(1.0f, 1.0f));
            glTexCoord2f(p[0], p[1]);
            glVertex3f(0.0f, 1.0f, 0.45f);

            p = context.doorCatalog->position(tpos, CMatrix<float, 1, 2>::create(0.0f, 1.0f));
            glTexCoord2f(p[0], p[1]);
            glVertex3f(1.0f, 1.0f, 0.45f);

            // -------------

            p = context.doorCatalog->position(tpos, CMatrix<float, 1, 2>::create(0.0f, 0.0f));
            glTexCoord2f(p[0], p[1]);
            glVertex3f(0.0f, 0.0f, 0.55f);

            p = context.doorCatalog->position(tpos, CMatrix<float, 1, 2>::create(1.0f, 0.0f));
            glTexCoord2f(p[0], p[1]);
            glVertex3f(1.0f, 0.0f, 0.55f);

            p = context.doorCatalog->position(tpos, CMatrix<float, 1, 2>::create(1.0f, 1.0f));
            glTexCoord2f(p[0], p[1]);
            glVertex3f(1.0f, 1.0f, 0.55f);

            p = context.doorCatalog->position(tpos, CMatrix<float, 1, 2>::create(0.0f, 1.0f));
            glTexCoord2f(p[0], p[1]);
            glVertex3f(0.0f, 1.0f, 0.55f);
        }
        glEnd();

        glPopMatrix();

        if (door.orientation() == Wolf3D::FaceWest) {
            glTranslatef(0.0f, 0.0f, 1.0f);
            glRotatef(90, 0, 1, 0);
        }

        glBegin(GL_QUADS);
        {
            Wolf3D::TexturePosition<Wolf3D::DoorCatalog> tpos(2);

            p = context.doorCatalog->position(tpos, CMatrix<float, 1, 2>::create(0.0f, 0.0f));
            glTexCoord2f(p[0], p[1]);
            glVertex3f(1.0f, 0.0f, 0.0f);

            p = context.doorCatalog->position(tpos, CMatrix<float, 1, 2>::create(1.0f, 0.0f));
            glTexCoord2f(p[0], p[1]);
            glVertex3f(1.0f, 0.0f, 1.0f);

            p = context.doorCatalog->position(tpos, CMatrix<float, 1, 2>::create(1.0f, 1.0f));
            glTexCoord2f(p[0], p[1]);
            glVertex3f(1.0f, 1.0f, 1.0f);

            p = context.doorCatalog->position(tpos, CMatrix<float, 1, 2>::create(0.0f, 1.0f));
            glTexCoord2f(p[0], p[1]);
            glVertex3f(1.0f, 1.0f, 0.0f);

            //------------------

            p = context.doorCatalog->position(tpos, CMatrix<float, 1, 2>::create(0.0f, 0.0f));
            glTexCoord2f(p[0], p[1]);
            glVertex3f(0.0f, 0.0f, 1.0f);

            p = context.doorCatalog->position(tpos, CMatrix<float, 1, 2>::create(1.0f, 0.0f));
            glTexCoord2f(p[0], p[1]);
            glVertex3f(0.0f, 0.0f, 0.0f);

            p = context.doorCatalog->position(tpos, CMatrix<float, 1, 2>::create(1.0f, 1.0f));
            glTexCoord2f(p[0], p[1]);
            glVertex3f(0.0f, 1.0f, 0.0f);

            p = context.doorCatalog->position(tpos, CMatrix<float, 1, 2>::create(0.0f, 1.0f));
            glTexCoord2f(p[0], p[1]);
            glVertex3f(0.0f, 1.0f, 1.0f);

            //------------------


        }
        glEnd();

        glPopMatrix();
    }
};

void Wolf3D::Scene::Camera::init(Level* level) {
    this->level = level;
}

void Wolf3D::Scene::Camera::recomputeMatrices() {
    Application& app = Application::instance();
    const float w = float(app.state().width);
    const float h = float(app.state().height);

    const Matrix<float, 1, 2>& position = level->playerPositionExact();
    float rotation = level->playerRotation();

    projectionMatrix = perspectiveMatrix(M_PI/3, w/h, 0.01f, 64.0f);
    viewMatrix = rotationMatrix(rotation * (M_PI/180), 0, 1, 0) *
                 translationMatrix(-position[0], -0.5f, -position[1]);
    
    ::Camera::recomputeMatrices();
}

void Wolf3D::Scene::load(Application& app) {
    //TODO
    palette.init();
    gameMaps.init();
    graphics.init();
    wallCatalog.init(graphics, palette);
    spriteCatalog.init(graphics, palette);
    doorCatalog.init(graphics, palette);

    level.init(gameMaps.map(0));
    camera.init(&level);

    renderer = app.state().rendererManager.create<Level>();
    renderer->init(&level);

    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glDepthMask(GL_TRUE);

    glEnable(GL_ALPHA_TEST);
    glAlphaFunc(GL_NOTEQUAL, 0.0f);

    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glFrontFace(GL_CCW);
    
    glEnable(GL_TEXTURE_2D);
}

void Wolf3D::Scene::unload(Application& app) {
    graphics.clear();
    gameMaps.clear();
    palette.clear();
}

void Wolf3D::Scene::activate(Application&) {
}

void Wolf3D::Scene::suspend(Application&) {
}

void Wolf3D::Scene::resized(Application&) {
}

void Wolf3D::Scene::keyPressed(Application& app, int which) {
    const Application::State& state = app.state();

    switch (which) {
    case VK_ESCAPE:
        app.quit();
        break;
    case VK_LEFT:
        level.rotate(-5);
        break;
    case VK_RIGHT:
        level.rotate(5);
        break;
    case VK_DOWN:
        level.move(-0.1f);
        break;
    case VK_UP:
        level.move(0.1f);
        break;
    /*case VK_PRIOR:
        camera.zoomZ(0.1f);
        break;
    case VK_NEXT:
        camera.zoomZ(-0.1f);
        break;*/
    }
}

void Wolf3D::Scene::keyReleased(Application&, int) {
}

void Wolf3D::Scene::update(Application&, long) {
}

void Wolf3D::Scene::repaint(Application& app) {
    camera.recomputeMatrices();

    glClear(GL_DEPTH_BUFFER_BIT);
    
    glEnable(GL_SCISSOR_TEST);

    glScissor(0, app.state().height/2, app.state().width, app.state().height/2);
    glClearColor(0.22f, 0.22f, 0.22f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    glScissor(0, 0, app.state().width, app.state().height/2);
    glClearColor(0.45f, 0.45f, 0.45f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    glDisable(GL_SCISSOR_TEST);

    Renderer<Level>::Context ctx = {
        &camera,
        &wallCatalog,
        &spriteCatalog,
        &doorCatalog
    };
    renderer->draw(ctx);
}
