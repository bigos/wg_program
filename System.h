/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#ifndef System_h
#define System_h

#include <windows.h>
#include <stdlib.h>
#include <string>

size_t doWidening(const char* str, size_t n, wchar_t* wstr, size_t wn);
size_t doNarrowing(const wchar_t* wstr, size_t wn, char* str, size_t n);

inline std::wstring widen(const char* str, size_t n) {
    std::wstring ret;
    ret.resize(n+1);
    n = doWidening(str, n, &ret[0], n);
    ret.resize(n);
    return ret;
}
inline std::wstring widen(const char* str) {
    return widen(str, strlen(str));
}
inline std::wstring widen(const std::string& str) {
    return widen(str.c_str(), str.size());
}
template <size_t N> inline
std::wstring widen(const char (&str)[N]) {
    return widen(str, N);
}

inline std::string narrow(const wchar_t* str, size_t n) {
    std::string ret;
    ret.resize(3*n+1);
    n = doNarrowing(str, n, &ret[0], 3*n);
    ret.resize(n);
    return ret;
}
inline std::string narrow(const wchar_t* str) {
    return narrow(str, wcslen(str));
}
inline std::string narrow(const std::wstring& str) {
    return narrow(str.c_str(), str.size());
}
template <size_t N> inline
std::string narrow(const wchar_t (&str)[N]) {
    return narrow(str, N);
}

template <size_t N>
class Widen {
public:
    Widen(const char* str) {
        size = doWidening(str, -1, data, N-1);
    }
    Widen(const char* str, size_t n) {
        size = doWidening(str, n, data, N-1);
    }
    Widen(const std::string& str) {
        size = doWidening(str.c_str(), str.size(), data, N-1);
    }
    template <size_t N2>
    Widen(const char (&str)[N2]) {
        size = doWidening(str, N2, data, N-1);
    }

    operator wchar_t*() { return data; }

    wchar_t data[N];
    size_t size;
};

template <size_t N>
class Narrow {
public:
    Narrow(const wchar_t* str) {
        size = doNarrowing(str, -1, data, N-1);
    }
    Narrow(const wchar_t* str, size_t n) {
        size = doNarrowing(str, n, data, N-1);
    }
    Narrow(const std::wstring& str) {
        size = doNarrowing(str.c_str(), str.size(), data, N-1);
    }
    template <size_t N2>
    Narrow(const wchar_t (&str)[N2]) {
        size = doNarrowing(str, N2, data, N-1);
    }

    operator char*() { return data; }

    char data[N];
    size_t size;
};

#endif
