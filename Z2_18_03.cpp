/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#include "Z2_18_03.h"

#include "Log.h"
#include "Message.h"

#include <algorithm>

void GameOfLife::init(unsigned width, unsigned height) {
    assert(width > 0 && height > 0);
    w = width;
    h = height;
    cells.resize(w*h);
    next.resize(w*h);
    alive.resize(w*h);
}

void GameOfLife::tick() {
    memset(&alive[0], 0, w*h);
    for (unsigned y = 0; y < h; ++y) {
        for (unsigned x = 0; x < w; ++x) {
            Cell cell(x, y);
            unsigned char& a = alive[y * w + x];
            if (cell.neighbour(*this, -1, -1).liveness(*this)) ++a;
            if (cell.neighbour(*this,  0, -1).liveness(*this)) ++a;
            if (cell.neighbour(*this,  1, -1).liveness(*this)) ++a;
            if (cell.neighbour(*this,  1,  0).liveness(*this)) ++a;
            if (cell.neighbour(*this,  1,  1).liveness(*this)) ++a;
            if (cell.neighbour(*this,  0,  1).liveness(*this)) ++a;
            if (cell.neighbour(*this, -1,  1).liveness(*this)) ++a;
            if (cell.neighbour(*this, -1,  0).liveness(*this)) ++a;
        }
    }
    for (unsigned y = 0; y < h; ++y) {
        for (unsigned x = 0; x < w; ++x) {
            Cell cell(x, y);
            const unsigned char l = cell.liveness(*this);
            CellRec& n = next[y * w + x];
            const unsigned char a = alive[y * w + x];
            if (l) {
                if (a < 2 || a > 3) {
                    n.liveness = 0;
                } else {
                    n.liveness = l - 1;
                }
            } else {
                if (a == 3) {
                    n.liveness = CellRec::lifespan;
                } else {
                    n.liveness = 0;
                }
            }
        }
    }

    cells.swap(next);
}

GameOfLife::CellRec* GameOfLife::getRec(unsigned x, unsigned y) {
    assert(x < w && y < h);
    return &cells[y * w + x];
}

GameOfLife::Cell GameOfLife::Cell::neighbour(GameOfLife& game, int dx, int dy) {
    assert(dx > -int(game.w) && dx < int(game.w));
    assert(dy > -int(game.h) && dy < int(game.h));
    int nx = x + dx;
    nx = (nx < 0 ? nx + game.w : (nx >= int(game.w) ? nx - game.w : nx));
    int ny = y + dy;
    ny = (ny < 0 ? ny + game.h : (ny >= int(game.h) ? ny - game.h : ny));

    return Cell(nx, ny);
}

void Z2_18_03::computeWindow() {
    const Application::State& state = Application::instance().state();
    width = (float(state.width)-0.5f)/cameraScale;
    height = (float(state.height)-0.5f)/cameraScale;

    if (std::min(width, height) < 1.0f) {
        cameraScale *= std::min(width, height);
        width = (float(state.width)-0.5f)/cameraScale;
        height = (float(state.height)-0.5f)/cameraScale;
    }

    if (2.0f*width > float(game.width())) {
        cameraScale *= (2.0f*width / float(game.width()));
        width = (float(state.width)-0.5f)/cameraScale;
        height = (float(state.height)-0.5f)/cameraScale;
    }

    if (2.0f*height > float(game.height())) {
        cameraScale *= (2.0f*height / float(game.height()));
        width = (float(state.width)-0.5f)/cameraScale;
        height = (float(state.height)-0.5f)/cameraScale;
    }
    
    focus.computeView();
}

void Z2_18_03::computeProjectionMatrix(int dx, int dy) {
    projectionMatrix = orthogonalMatrix(cameraX-width, cameraX+width, cameraY-height, cameraY+height, -1.0, 1.0) *
                       translationMatrix(float(dx)*game.width(), float(dy)*game.height(), 0);
}

void Z2_18_03::load(Application&) {
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

    cameraX = cameraY = 0.0f;
    cameraScale = 40.0f;

    secondsPerTick = 0.1f;
    accumulatedSeconds = 0;
    stopped = false;

    game.init(100, 100);
    GameOfLife::Cell(50, 50).liveness(game) = 5;
    GameOfLife::Cell(50, 51).liveness(game) = 5;
    GameOfLife::Cell(50, 52).liveness(game) = 5;
    GameOfLife::Cell(49, 52).liveness(game) = 5;
    GameOfLife::Cell(48, 51).liveness(game) = 5;

    {
        QuadVertex Quad[4] = {
            {-0.45f, -0.45f},
            {-0.45f,  0.45f},
            { 0.45f,  0.45f},
            { 0.45f, -0.45f}
        };
        Buffer::Array array(quad);
        array.allocate(sizeof Quad, GL_STATIC_DRAW, Quad);
    }
    {
        Buffer::Array array(cells);
        array.allocate(game.width()*game.height(), GL_STREAM_DRAW, 0);
    }
    {
        Buffer::ElementArray indices(this->indices);
        indices.allocate(sizeof(unsigned)*8*game.width()*game.height(), GL_STREAM_DRAW, 0);
    }
    
    simpleProgram.init();
#if MODERN
    cellProgram.init(false);
    frameProgram.init(true);
    drawCells.init(cellProgram, GL_TRIANGLE_FAN);
    drawFrames.init(frameProgram, GL_LINE_LOOP);
#else
    draw2.init();
#endif
    focus.init(game.width()/2, game.height()/2);

    updateTitle();
}

void Z2_18_03::unload(Application&) {
}

void Z2_18_03::updateTitle() {
    if (stopped) {
        Application::instance().changeWindowName("Game of Life: Stopped");
        return;
    }
    std::ostringstream oss;

    oss.precision(5);
    oss << "Game of Life: " << (1/secondsPerTick) << " Ticks/s";

    Application::instance().changeWindowName(oss.str());
}

#if MODERN
void Z2_18_03::GameCellProgram::init(bool frame) {
    Z2_18_03& scene = (Z2_18_03&)Application::instance().scene();

    Shader::Source* vss = Shader::Source::get("gameCell.vs");
    Shader::Source* fss = Shader::Source::get("gameCell.fs");

    if (frame) {
        defines["FRAME"] = "1";
        vss->setDefines(defines);
        fss->setDefines(defines);
    }

    vs.setSource(vss);
    fs.setSource(fss);
    program.addObject(vs);
    program.addObject(fs);
    program.bindAttribute(position, "aPosition");
    program.bindAttribute(liveness, "aLiveness");
    program.link();

    transformMatrix = program.getUniform("uTransformMatrix");
    gameWidth = program.getUniform("uGameWidth");
    gameHeight = program.getUniform("uGameHeight");

    {
        Shader::Bind shader(program);
        gameWidth = (int)scene.game.width();
        gameHeight = (int)scene.game.height();
    }
}
#endif

void Z2_18_03::GameCell2Program::init() {
    Shader::Source* vss = Shader::Source::get("gameCell.2.vs");
    Shader::Source* fss = Shader::Source::get("gameCell.2.fs");

    vs.setSource(vss);
    fs.setSource(fss);
    program.addObject(vs);
    program.addObject(fs);
    program.bindAttribute(position, "aPosition");
    program.bindAttribute(color, "aColor");
    program.link();

    transformMatrix = program.getUniform("uTransformMatrix");
}

#if MODERN
void Z2_18_03::GameDraw::init(GameCellProgram& program, GLenum mode) {
    this->program = &program;
    this->mode = mode;
    Z2_18_03& scene = (Z2_18_03&)Application::instance().scene();
    {
        using namespace Vertex;
        Bind array(a);
        Attribute position(GameCellProgram::position);
        Attribute liveness(GameCellProgram::liveness);

        position.enable();
        position.setPointer(GL_FLOAT, true, 3, sizeof QuadVertex, scene.quad, offsetof(QuadVertex, x));

        liveness.enable();
        liveness.setIPointer(GL_UNSIGNED_BYTE, 1, 0, scene.cells, 0);
        liveness.setDivisor(1);
    }
}
#else
void Z2_18_03::Game2Draw::init() {
    Z2_18_03& scene = (Z2_18_03&)Application::instance().scene();
    {
        using namespace Vertex;
        Bind array(a);
        Attribute position(GameCell2Program::position);
        Attribute color(GameCell2Program::color);

        position.enable();
        position.setPointer(GL_FLOAT, true, 2, sizeof CellVertex, scene.cells, offsetof(CellVertex, x));
        
        color.enable();
        color.setPointer(GL_UNSIGNED_BYTE, true, 3, sizeof CellVertex, scene.cells, offsetof(CellVertex, r));
    }
}
#endif

void Z2_18_03::Focus::init(int x, int y) {
    Z2_18_03& scene = (Z2_18_03&)Application::instance().scene();

    this->x = x;
    this->y = y;
    
    {
        float data[3] = {1.0f, 0.0f, 0.0f};
        Buffer::Array array(red);
        array.allocate(sizeof data, GL_STATIC_DRAW, data);
    }
    {
        using namespace Vertex;
        Bind array(a);
        Attribute position(GameCell2Program::position);
        Attribute color(GameCell2Program::color);

        position.enable();
        position.setPointer(GL_FLOAT, true, 2, sizeof QuadVertex, scene.quad, offsetof(QuadVertex, x));
    }
}

void Z2_18_03::prepareCells() {
    Buffer::Array array(cells);
    Buffer::ElementArray indices(this->indices);
#if MODERN
    //array.update(0, game.width()*game.height(), game.getRec(0, 0));
    array.allocate(game.width()*game.height(), GL_STREAM_DRAW, game.getRec(0, 0));
#else
    unsigned w = game.width();
    unsigned h = game.height();
    array.allocate(4*w*h*sizeof(CellVertex), GL_STREAM_DRAW, 0);
    bool cond;
    bool cond2;
    do {
        CellVertex* v = (CellVertex*)array.map(GL_WRITE_ONLY);
        struct Index {
            unsigned f0, t0;
            unsigned f1, t1;
            unsigned f2, t2;
            unsigned f3, t3;
        };
        Index* index = (Index*)indices.map(GL_WRITE_ONLY);
        //Message("") << glGetError();
        unsigned i = 0;
        limit = game.width()*game.height();

        for (unsigned y = 0; y < h; ++y) {
            for (unsigned x = 0; x < w; ++x) {
                unsigned char l = game.getRec(x, y)->liveness;
                float xC = float(x) - float(w-1)/2.0f;
                float yC = -(float(y) - float(h-1)/2.0f);
                CellVertex* av;
                float color;
                if (l > 0) {
                    av = &v[4*i];
                    color = 1.0f - float(l)*0.2f;
                    ++i;
                } else {
                    --limit;
                    av = &v[4*limit];
                    color = 0.9f;

                    index[limit].f0 = 4*limit;
                    index[limit].t0 = 4*limit+1;
                    index[limit].f1 = 4*limit+1;
                    index[limit].t1 = 4*limit+2;
                    index[limit].f2 = 4*limit+2;
                    index[limit].t2 = 4*limit+3;
                    index[limit].f3 = 4*limit+3;
                    index[limit].t3 = 4*limit;
                }

                av->x = xC - 0.45f;
                av->y = yC - 0.45f;
                av->r = av->g = av->b = (unsigned char)(color*255.0f);

                ++av;
                av->x = xC + 0.45f;
                av->y = yC - 0.45f;
                av->r = av->g = av->b = (unsigned char)(color*255.0f);
                    
                ++av;
                av->x = xC + 0.45f;
                av->y = yC + 0.45f;
                av->r = av->g = av->b = (unsigned char)(color*255.0f);
                    
                ++av;
                av->x = xC - 0.45f;
                av->y = yC + 0.45f;
                av->r = av->g = av->b = (unsigned char)(color*255.0f);
            }
        }
        cond = array.unmap();
        cond2 = indices.unmap();
    } while (!cond || !cond2);

    global() << "Limit: " << limit << '\n';
#endif
}

#if MODERN
void Z2_18_03::GameDraw::draw() {
    Z2_18_03& scene = (Z2_18_03&)Application::instance().scene();
    unsigned w = scene.game.width();
    unsigned h = scene.game.height();

    Vertex::Bind vertex(a);
    Shader::Bind shader(program->program);

    program->transformMatrix = scene.projectionMatrix;
    glLineWidth(1.0f);
    glDrawArraysInstanced(mode, 0, 4, w*h);
}
#else
void Z2_18_03::Game2Draw::draw() {
    Z2_18_03& scene = (Z2_18_03&)Application::instance().scene();
    unsigned w = scene.game.width();
    unsigned h = scene.game.height();

    Vertex::Bind vertex(a);
    Buffer::ElementArray indices(scene.indices);
    Shader::Bind shader(scene.simpleProgram.program);

    scene.simpleProgram.transformMatrix = scene.projectionMatrix;
    glLineWidth(1.0f);

    glDrawArrays(GL_QUADS, 0, scene.limit*4);
    //glDrawArrays(GL_LINE_STRIP, scene.limit*4, (w*h - scene.limit)*4);
    //glDrawElements(GL_LINES, scene.limit*8, GL_UNSIGNED_BYTE, 0);
    glDrawElements(GL_LINES, (w*h - scene.limit)*8, GL_UNSIGNED_INT, (unsigned*)0+scene.limit*8);
}
#endif

void Z2_18_03::Focus::draw() {
    Z2_18_03& scene = (Z2_18_03&)Application::instance().scene();
    unsigned w = scene.game.width();
    unsigned h = scene.game.height();

    Vertex::Bind vertex(a);
    Shader::Bind shader(scene.simpleProgram.program);
    
    float data[3] = {1.0f, 0.0f, 0.0f};
    glVertexAttrib3fv(GameCell2Program::color, data);

    scene.computeProjectionMatrix(0, 0);
    matrix = scene.projectionMatrix *
        translationMatrix(xView, yView, 0.0f);
    scene.simpleProgram.transformMatrix = matrix;
    glLineWidth(2.0f);
    glDrawArrays(GL_LINE_LOOP, 0, 4);
}

void Z2_18_03::Focus::computeView() {
    Z2_18_03& scene = (Z2_18_03&)Application::instance().scene();

    // Fix the camera.
    if (float(scene.game.width()-1)/2.0f - scene.cameraX - 0.5f < -scene.width) {
        scene.cameraX -= float(scene.game.width());
    } else if (-float(scene.game.width()-1)/2.0f - scene.cameraX + 0.5f > scene.width) {
        scene.cameraX += float(scene.game.width());
    }

    if (float(scene.game.height()-1)/2.0f - scene.cameraY - 0.5f < -scene.height) {
        scene.cameraY -= float(scene.game.height());
    } else if (-float(scene.game.height()-1)/2.0f - scene.cameraY + 0.5f > scene.height) {
        scene.cameraY += float(scene.game.height());
    }

    float xV = float(x) - float(scene.game.width()-1)/2.0f;
    float xC = xV - scene.cameraX;
    if (xC - 0.5f < -scene.width) {
        xV += float(scene.game.width());
    } else if (xC + 0.5f > scene.width) {
        xV -= float(scene.game.width());
    }

    float yV = -(float(y) - float(scene.game.height()-1)/2.0f);
    float yC = yV - scene.cameraY;
    if (yC - 0.5f < -scene.height) {
        yV += float(scene.game.height());
    } else if (yC + 0.5f > scene.height) {
        yV -= float(scene.game.height());
    }

    xView = xV;
    yView = yV;
    
    global() << "cameraX: " << scene.cameraX << " cameraY: " << scene.cameraY;
    global() << "x: " << x << " y: " << y;
    global() << "xV: " << xV << " yV: " << yV;
}

void Z2_18_03::activate(Application&) {
}

void Z2_18_03::suspend(Application&) {
}

void Z2_18_03::resized(Application&) {
    computeWindow();
}

void Z2_18_03::keyPressed(Application&, int which) {
    Application& app = Application::instance();
    const Application::State& state = app.state();

    switch (which) {
    case VK_ESCAPE:
        app.quit();
        break;
    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
        game.getRec(focus.x, focus.y)->liveness = which - '0';
        break;
    case VK_LEFT:
        if (state.keys[VK_SHIFT]) {
            if (focus.xView - cameraX + 1.5f > width) {
                if (--focus.x < 0) {
                    focus.x += game.width();
                }
            }
            cameraX -= 1.0f;
        } else {
            if (focus.xView - cameraX - 1.5f < -width) {
                cameraX -= 1.0f;
            }
            if (--focus.x < 0) {
                focus.x += game.width();
            }
        }
        focus.computeView();
        break;
    case VK_RIGHT:
        if (state.keys[VK_SHIFT]) {
            if (focus.xView - cameraX - 1.5f < -width) {
                if (++focus.x >= int(game.width())) {
                    focus.x -= game.width();
                }
            }
            cameraX += 1.0f;
        } else {
            if (focus.xView - cameraX + 1.5f > width) {
                cameraX += 1.0f;
            }
            if (++focus.x >= int(game.width())) {
                focus.x -= game.width();
            }
        }
        focus.computeView();
        break;
    case VK_DOWN:
        if (state.keys[VK_SHIFT]) {
            if (focus.yView - cameraY + 1.5f > height) {
                if (++focus.y >= int(game.height())) {
                    focus.y -= game.height();
                }
            }
            cameraY -= 1.0f;
        } else {
            if (focus.yView - cameraY - 1.5f < -height) {
                cameraY -= 1.0f;
            }
            if (++focus.y >= int(game.height())) {
                focus.y -= game.height();
            }
        }
        focus.computeView();
        break;
    case VK_UP:
        if (state.keys[VK_SHIFT]) {
            if (focus.yView - cameraY - 1.5f < -height) {
                if (--focus.y < 0) {
                    focus.y += game.height();
                }
            }
            cameraY += 1.0f;
        } else {
            if (focus.yView - cameraY + 1.5f > height) {
                cameraY += 1.0f;
            }
            if (--focus.y < 0) {
                focus.y += game.height();
            }
        }
        focus.computeView();
        break;
    case VK_OEM_COMMA:
        if (state.keys[VK_SHIFT]) {
            secondsPerTick *= 2;
            updateTitle();
        }
        break;
    case VK_OEM_PERIOD:
        if (state.keys[VK_SHIFT]) {
            secondsPerTick /= 2;
            updateTitle();
        }
        break;
    case 'S':
        stopped = !stopped;
        updateTitle();
        break;
    /*case VK_INSERT:
        circle.rotX += 0.2f;
        circle.computeModelMatrix();
        break;
    case VK_DELETE:
        circle.rotX -= 0.2f;
        circle.computeModelMatrix();
        break;
    case VK_HOME:
        circle.rotY += 0.2f;
        circle.computeModelMatrix();
        break;
    case VK_END:
        circle.rotY -= 0.2f;
        circle.computeModelMatrix();
        break;
    case VK_PRIOR:
        circle.rotZ += 0.2f;
        circle.computeModelMatrix();
        break;
    case VK_NEXT:
        circle.rotZ -= 0.2f;
        circle.computeModelMatrix();
        break;*/
    case VK_ADD:
        cameraScale *= 1.1f;
        computeWindow();
        break;
    case VK_SUBTRACT:
        cameraScale /= 1.1f;
        computeWindow();
        break;
    /*case VK_TAB:
        interpolate = !interpolate;
        chooseProgram();
        break;*/
    }
}

void Z2_18_03::keyReleased(Application&, int which) {
}

void Z2_18_03::update(Application&, long diff) {
    if (stopped) {
        return;
    }
    accumulatedSeconds += float(diff)*0.001f;
    while (accumulatedSeconds > secondsPerTick) {
        game.tick();
        accumulatedSeconds -= secondsPerTick;
    }
}

void Z2_18_03::repaint(Application&) {
    const Application::State& state = Application::instance().state();

    prepareCells();
    glClear(GL_COLOR_BUFFER_BIT);

    int xMin = 0, xMax = 0;
    int yMin = 0, yMax = 0;

    xMin = int(floor(-((width - cameraX)/float(game.width()) - 0.5f)));
    xMax = int(ceil((width + cameraX)/float(game.width()) - 0.5f));
    yMin = int(floor(-((height - cameraY)/float(game.height()) - 0.5f)));
    yMax = int(ceil((height + cameraY)/float(game.height()) - 0.5f));

    for (int x = xMin; x <= xMax; ++x) {
        for (int y = yMin; y <= yMax; ++y) {
            computeProjectionMatrix(x, y); 
#if MODERN
            drawCells.draw();
            drawFrames.draw();
#else
            draw2.draw();
#endif
        }
    }

    focus.draw();
}
