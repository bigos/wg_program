/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#ifndef Vertex_h
#define Vertex_h

#include "Buffer.h"
#include "Exceptions.h"
#include "GL.h"
#include "Matrix.h"

namespace Vertex {

class Array {
public:
    Array() : m_array(0) {}

    void destroy();

    void bind();
    static void unbind();

private:
    GLuint m_array;

    void create();
};

class Attribute {
public:
    explicit Attribute(GLuint index) : m_index(index) {}

    GLuint index() const { return m_index; }

    void enable();
    void disable();
    void setPointer(GLenum type, bool normalized, int size, ptrdiff_t stride,
                    Buffer::Buffer buffer, size_t offset);
    void setPointer(GLenum type, bool normalized, int size, ptrdiff_t stride,
                    size_t offset);
    void setIPointer(GLenum type, int size, ptrdiff_t stride,
                    Buffer::Buffer buffer, size_t offset);
    void setIPointer(GLenum type, int size, ptrdiff_t stride, size_t offset);
    void setDivisor(GLuint div);

    //TODO set

private:
    GLuint m_index;
};

class Bind {
public:
    explicit Bind(Array& a) : array(a) { array.bind(); }
    ~Bind() { array.unbind(); }

    void setPrimitiveRestartIndex(GLuint index);

    void enable(GLuint index);
    void disable(GLuint index);

private:
    Array& array;
};

inline signed char normalizedToInt8(float n) {
    assert(n >= -1.0f && n <= 1.0f);
    return (signed char)(n*0x7F);
}

inline unsigned char normalizedToUInt8(float n) {
    assert(n >= 0.0f && n <= 1.0f);
    return (unsigned char)(n*0xFFu);
}

inline signed short normalizedToInt16(float n) {
    assert(n >= -1.0f && n <= 1.0f);
    return (signed short)(n*0x7FFF);
}

inline unsigned short normalizedToUInt16(float n) {
    assert(n >= 0.0f && n <= 1.0f);
    return (unsigned short)(n*0xFFFFu);
}

inline signed int normalizedToInt32(float n) {
    assert(n >= -1.0f && n <= 1.0f);
    return (signed int)(n*0x7FFFFFFF);
}

inline unsigned int normalizedToUInt32(float n) {
    assert(n >= 0.0f && n <= 1.0f);
    return (unsigned int)(n*0xFFFFFFFF);
}

} // end of Vertex

#endif
