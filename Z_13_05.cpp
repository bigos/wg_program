/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#include "Z_13_05.h"

#include "Log.h"

#define OUTLINE_FONTS

template <>
class Renderer_1_1<Z_13_05::Surface> : public Renderer<Z_13_05::Surface> {
public:
    Renderer_1_1() {}

    virtual ~Renderer_1_1() {
        glDeleteLists(asciiList, 256);
    }

    virtual void init(const Z_13_05::Surface* object, size_t num = 1) {
        surface = object;
        this->num = num;

        const Application::State& state = Application::instance().state();
        
        HFONT font, lastFont;
        font = CreateFont(-16, 0, 0, 0, FW_NORMAL, false, false, false, ANSI_CHARSET,
                          OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, ANTIALIASED_QUALITY,
                          DEFAULT_PITCH | FF_DONTCARE, L"Arial");
        if (!font) {
            throw WinApiException("CreateFont");
        }
        lastFont = (HFONT)SelectObject(state.dc, font);

        asciiList = glGenLists(127-32);

#ifndef OUTLINE_FONTS
        if (!wglUseFontBitmaps(state.dc, 32, 127-32, asciiList)) {
            throw WinApiException("wglUseFontBitmaps");
        }
#else
        if (!wglUseFontOutlines(state.dc, 32, 127-32, asciiList, 0, 0,
                                WGL_FONT_LINES, nullptr)) {
            throw WinApiException("wglUseFontOutlines");
        }
#endif

        SelectObject(state.dc, lastFont);
        DeleteObject(font);
    }

    virtual void draw(DefaultContext& context) {
        typedef Z_13_05::Surface::Point Point;
        typedef Z_13_05::Surface::PointColorTexture PointCT;
        static const size_t ControlSize = Z_13_05::Surface::ControlSize;

        glEnable(GL_AUTO_NORMAL);
        glEnable(GL_MAP2_VERTEX_3);
        glEnable(GL_MAP2_COLOR_4);

        float ctKnots[4] = {0, 0, 1, 1};

        for (size_t i = 0; i < num; ++i) {
            const Z_13_05::Surface& surface = this->surface[i];

            gluNurbsProperty(surface.nurbs, GLU_DISPLAY_MODE, (float)surface.mode);
            gluNurbsProperty(surface.nurbs, GLU_SAMPLING_TOLERANCE, surface.pathLength);
            
            {
                Texture::Texture tex = surface.texture;
                Texture::Bind2D t(tex);
                gluBeginSurface(surface.nurbs);
                
                gluNurbsSurface(surface.nurbs,
                    ControlSize+surface.degree+1, (float*)surface.uKnots,
                    ControlSize+surface.degree+1, (float*)surface.vKnots,
                    sizeof(Point)/sizeof(float),
                    sizeof(Point)/sizeof(float)*ControlSize,
                    (float*)surface.points + offsetof(Point, vertex)/sizeof(float),
                    surface.degree+1, surface.degree+1, GL_MAP2_VERTEX_3);

                gluNurbsSurface(surface.nurbs,
                    4, ctKnots,
                    4, ctKnots,
                    sizeof(PointCT)/sizeof(float),
                    sizeof(PointCT)/sizeof(float)*2,
                    (float*)surface.colorTexture + offsetof(PointCT, color)/sizeof(float),
                    2, 2, GL_MAP2_COLOR_4);

                if (surface.useTexture) {
                    glEnable(GL_MAP2_TEXTURE_COORD_2);
                    glEnable(GL_TEXTURE_2D);
                
                    gluNurbsSurface(surface.nurbs,
                        4, ctKnots,
                        4, ctKnots,
                        sizeof(PointCT)/sizeof(float),
                        sizeof(PointCT)/sizeof(float)*2,
                        (float*)surface.colorTexture + offsetof(PointCT, texCoord)/sizeof(float),
                        2, 2, GL_MAP2_TEXTURE_COORD_2);
                }

                glPointSize(1.0f);
                glLineWidth(1.0f);


                gluEndSurface(surface.nurbs);
            }
            

            /*glMap2f(GL_MAP2_VERTEX_3,
                    0.0f, 1.0f, sizeof(Point)/sizeof(float), ControlSize,
                    0.0f, 1.0f, sizeof(Point)/sizeof(float)*ControlSize, ControlSize,
                    (const GLfloat*)surface.points + offsetof(Point, vertex)/sizeof(float));
            glMap2f(GL_MAP2_COLOR_4,
                    0.0f, 1.0f, sizeof(PointCT)/sizeof(float), 2,
                    0.0f, 1.0f, sizeof(PointCT)/sizeof(float)*2, 2,
                    (const GLfloat*)surface.colorTexture + offsetof(PointCT, color)/sizeof(float));

            if (surface.useTexture) {
                glEnable(GL_MAP2_TEXTURE_COORD_2);
                glEnable(GL_TEXTURE_2D);
                
                glMap2f(GL_MAP2_TEXTURE_COORD_2,
                    0.0f, 1.0f, sizeof(PointCT)/sizeof(float), 2,
                    0.0f, 1.0f, sizeof(PointCT)/sizeof(float)*2, 2,
                    (const GLfloat*)surface.colorTexture + offsetof(PointCT, texCoord)/sizeof(float));
            }

            const int nu = surface.nu;
            const int nv = surface.nv;
            glMapGrid2f(nu, 0.0f, 1.0f, nv, 0.0f, 1.0f);

            glPointSize(1.0f);
            glLineWidth(1.0f);
            {
                Texture::Texture tex = surface.texture;
                Texture::Bind2D t(tex);
                glEvalMesh2(surface.mode, 0, nu, 0, nv);
            }*/
            
            
            glDisable(GL_MAP2_TEXTURE_COORD_2);
            glDisable(GL_TEXTURE_2D);

            glDisable(GL_LIGHTING);

            if (surface.controlPoints) {
                drawControlPoints(*(Z_13_05::Camera*)context.camera, surface);
            }
            drawKnots(surface.axis ? -1 : surface.chosenU, surface.uKnots, surface.degree, CMatrix<float, 1, 2>::create(1, 0));
            drawKnots(!surface.axis ? -1 : surface.chosenV, surface.vKnots, surface.degree, CMatrix<float, 1, 2>::create(0, 1));
            
            glEnable(GL_LIGHTING);
        }
        
        glDisable(GL_MAP2_COLOR_4);
        glDisable(GL_MAP2_VERTEX_3);
        glDisable(GL_AUTO_NORMAL);
    }

private:
    const Z_13_05::Surface* surface;
    size_t num;
    GLuint asciiList;

    void drawControlPoints(Z_13_05::Camera& camera, const Z_13_05::Surface& surface) {
        static const size_t ControlSize = Z_13_05::Surface::ControlSize;
        char buf[32];

        glPointSize(4.0f);
        glBegin(GL_POINTS);
        for (int i = 0; i < ControlSize; ++i) {
            for (int j = 0; j < ControlSize; ++j) {
                if (i == surface.chosenY && j == surface.chosenX) {
                    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
                } else {
                    glColor4f(0.5f, 0.5f, 0.5f, 1.0f);
                }
                const Matrix<float, 1, 3> v = surface(j, i).vertex;
                glVertex3f(v[0], v[1], v[2]);
            }
        }
        glEnd();
        
        glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        glListBase(asciiList-32);
        for (int i = 0; i < ControlSize; ++i) {
            for (int j = 0; j < ControlSize; ++j) {
                const Matrix<float, 1, 3> v = surface(j, i).vertex;
                sprintf(buf, "%u %u", i+1, j+1);
#ifndef OUTLINE_FONTS
                glPushAttrib(GL_LIST_BIT);
                glRasterPos3f(v[0], v[1], v[2]);
                glCallLists(strlen(buf), GL_UNSIGNED_BYTE, buf);
                glPopAttrib();
#else
                glPushAttrib(GL_LIST_BIT);
                glPushMatrix();
                glTranslatef(v[0], v[1], v[2]);
                glScalef(0.2f, 0.2f, 0.2f);
                glRotatef(camera.getRotY(), 0.0f, -1.0f, 0.0f);
                glRotatef(camera.getRotX(), -1.0f, 0.0f, 0.0f);
                glCallLists(strlen(buf), GL_UNSIGNED_BYTE, buf);
                glPopMatrix();
                glPopAttrib();
#endif
            }
        }

        glLineWidth(1.0f);
        for (int i = 0; i < ControlSize; ++i) {
            glBegin(GL_LINE_STRIP);
            for (int j = 0; j < ControlSize; ++j) {
                glColor4f(0.7f, 0.7f, 0.7f, 1.0f);
                const Matrix<float, 1, 3> v = surface(j, i).vertex;
                glVertex3f(v[0], v[1], v[2]);
            }
            glEnd();

            glBegin(GL_LINE_STRIP);
            for (int j = 0; j < ControlSize; ++j) {
                glColor4f(0.7f, 0.7f, 0.7f, 1.0f);
                const Matrix<float, 1, 3> v = surface(i, j).vertex;
                glVertex3f(v[0], v[1], v[2]);
            }
            glEnd();
        }
    }

    void drawKnots(int chosen, const float* knots, int degree, const Matrix<float, 1, 2>& dir) {
        static const size_t ControlSize = Z_13_05::Surface::ControlSize;
        static const size_t MaxDegree = Z_13_05::Surface::MaxDegree;

        glLineWidth(2.0f);
        glColor4f(0.7f, 0.7f, 0.7f, 1.0f);
        glBegin(GL_LINES);
            glVertex3f(dir[0]*2.5f-dir[1]*2.0f, 0.0f, dir[1]*2.5f-dir[0]*2.0f);
            glVertex3f(dir[0]*2.5f+dir[1]*2.0f, 0.0f, dir[1]*2.5f+dir[0]*2.0f);
        glEnd();

        glPointSize(8.0f);
        glBegin(GL_POINTS);
        int degreeCutoff = ControlSize+degree+1;
        for (int i = 0; i < ControlSize+MaxDegree+1; ++i) {
            if (i >= degreeCutoff) {
                glColor4f(0.2f, 0.2f, 0.2f, 1.0f);
            } else if (i == chosen) {
                glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
            } else {
                glColor4f(0.5f, 0.5f, 0.5f, 1.0f);
            }
            //const Matrix<float, 1, 3> v = surface(j, i).vertex;
            const float val = knots[i];
            glVertex3f(dir[0]*2.5f+dir[1]*(-2.0f+val*4.0f), 0.0f, dir[1]*2.5f+dir[0]*(-2.0f+val*4.0f));
        }
        glEnd();
    }
};

template <>
class Renderer_1_1<Z_13_05::Objects> : public Renderer<Z_13_05::Objects> {
public:
    virtual ~Renderer_1_1() {}

    virtual void init(const Z_13_05::Objects* object, size_t num) {
        this->object = object;
        assert(num == 1);
        surface.init(&object->surface);
    }

    virtual void draw(DefaultContext& context) {
        glMatrixMode(GL_PROJECTION);
        glLoadMatrixf(((Z_13_05::Camera*)context.camera)->getProjectionMatrix().data);

        glMatrixMode(GL_MODELVIEW);
        glLoadMatrixf(((Z_13_05::Camera*)context.camera)->getViewMatrix().data);

        object->light->updateLight();

        glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, object->shininess);
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, object->ambient.data);
        glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, object->diffuse.data);
        glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, object->specular.data);
        
        {
            Matrix<float, 1, 4> pos =
                rotationMatrix(object->light->rotY * (M_PI/180), 0, 1, 0) *
                rotationMatrix(object->light->rotX * (M_PI/180), 1, 0, 0) *
                CMatrix<float, 1, 4>::create(0, 0, object->light->trZ, 1);
            glDisable(GL_LIGHTING);
            glPointSize(6.0f);
            glBegin(GL_POINTS);
                glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
                glVertex3f(pos[0], pos[1], pos[2]);
            glEnd();
            glEnable(GL_LIGHTING);
        }

        glTranslatef(0.0f, 1.0f, 0.0f);

        if (object->materialColor) {
            glEnable(GL_COLOR_MATERIAL);
        }
        if (object->depthTest) {
            glEnable(GL_DEPTH_TEST);
        }
        surface.draw(context);
        glDisable(GL_DEPTH_TEST);
        glDisable(GL_COLOR_MATERIAL);
    }

private:
    const Z_13_05::Objects* object;
    Renderer_1_1<Z_13_05::Surface> surface;
};

static void __stdcall nurbsError(GLenum err) {
    global() << "GLU NURBS error: " << (const char*)gluErrorString(err);
}

void Z_13_05::Surface::init() {
    nurbs = gluNewNurbsRenderer();
    gluNurbsCallback(nurbs, GLU_ERROR, (void (__stdcall*)(void))&nurbsError);

    for (int i = 0; i < ControlSize; ++i) {
        for (int j = 0; j < ControlSize; ++j) {
            (*this)(j, i).vertex = CMatrix<float, 1, 3>::create(i-(ControlSize-1)*0.5f, 0, j-(ControlSize-1)*0.5f);
        }
    }

    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 2; ++j) {
            ct(j, i).color = CMatrix<float, 1, 4>::create(1.0f, 0.0f, 1.0f, 1.0f);
            ct(j, i).texCoord = CMatrix<float, 1, 2>::create(float(i), float(j));
        }
    }

    uKnots[0] = vKnots[0] = 0.0f;

    for (int i = 1; i < ControlSize+MaxDegree; ++i) {
        uKnots[i] = vKnots[i] = (i-1)/float(ControlSize+MaxDegree-2);
    }

    uKnots[ControlSize+MaxDegree] = vKnots[ControlSize+MaxDegree] = 1.0f;

    pathLength = 32.0f;

    degree = MaxDegree;
    axis = false;

    chosenX = 0;
    chosenY = 0;
    chosenU = 0;
    chosenV = 0;

    mode = GLU_FILL;

    {
        using namespace Texture;
        Bind2D t(texture);
        t.setGenerateMipMap(GL_TRUE);
        t.setMagFilter(GL_LINEAR);
        t.setMinFilter(GL_LINEAR_MIPMAP_LINEAR);
        t.setAnisotropy(16.0f);
        Data data = Data::fromBMP("Image.bmp", 128, 128, 0x00A349A4);
        t.allocate(Size<2> (128, 128), 4, data);
        data.free();
    }

    useTexture = false;
    controlPoints = true;
}


void Z_13_05::Surface::changeChosen(int x, int y) {
    if (chosenX + x >= ControlSize || chosenX + x < 0) {
        return;
    }
    if (chosenY + y >= ControlSize || chosenY + y < 0) {
        return;
    }
    chosenX += x;
    chosenY += y;
}

void Z_13_05::Surface::changeVertex(const Matrix<float, 1, 3>& vertex) {
    (*this)(chosenX, chosenY).vertex += vertex;
}

void Z_13_05::Surface::changeColor(const Matrix<float, 1, 4>& color) {
    Matrix<float, 1, 4>& c = ct(float(chosenX), float(chosenY)).color;
    c += color;
    for (int i = 0; i < 4; ++i) {
        if (c[i] < 0.0f) {
            c[i] = 0.0f;
        }
        if (c[i] > 1.0f) {
            c[i] = 1.0f;
        }
    }
}

void Z_13_05::Surface::changeN(float factor) {
    pathLength *= factor;
    if (pathLength < 8.0f) {
        pathLength = 8.0f;
    } else if (pathLength > 512.0f) {
        pathLength = 512.0f;
    }
}

void Z_13_05::Surface::resetColors() {
    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 2; ++j) {
            ct(j, i).color = CMatrix<float, 1, 4>::create(1.0f, 1.0f, 1.0f, 1.0f);
        }
    }
}

void Z_13_05::Surface::changeAxis() {
    axis ^= 1;
}

void Z_13_05::Surface::changeChosen(int i) {
    if (chosen() + i >= ControlSize+degree+1 || chosen() + i < 0) {
        return;
    }
    chosen() += i;
}

void Z_13_05::Surface::changeValue(float v) {
    float* k = knots();
    int c = chosen();
    if (k[c] + v < 0.0f) {
        v = -k[c];
    } else if (k[c] + v > 1.0f) {
        v = 1.0f - k[c];
    }
    if (v == 0.0f) {
        return;
    }

    k[c] += v;
    if (v < 0.0f) {
        for (int i = c-1; i >= 0; --i) {
            if (k[i] > k[c]) {
                k[i] = k[c];
            } else {
                break;
            }
        }
    } else {
        for (int i = c+1; i < ControlSize+degree+1; ++i) {
            if (k[i] < k[c]) {
                k[i] = k[c];
            } else {
                break;
            }
        }
    }
}

void Z_13_05::Surface::changeDegree(int d) {
    if (degree == d) {
        return;
    }
    degree = d;
}

Z_13_05::Surface::Point& Z_13_05::Surface::operator() (int x, int y) {
    assert(x >= 0 && x < ControlSize);
    assert(y >= 0 && y < ControlSize);

    return points[y*ControlSize + x];
}

Z_13_05::Surface::PointColorTexture& Z_13_05::Surface::ct(int x, int y) {
    assert(x >= 0 && x < 2);
    assert(y >= 0 && y < 2);

    return colorTexture[y*2 + x];
}

void Z_13_05::Objects::init(Light* light) {
    surface.init();

    shininess = 5;
    ambient = CMatrix<float, 1, 4>::create(0.5f, 0.5f, 0.5f, 0.5f);
    diffuse = CMatrix<float, 1, 4>::create(0.5f, 0.5f, 0.5f, 0.5f);
    specular = CMatrix<float, 1, 4>::create(0.4f, 0.4f, 0.4f, 0.4f);
    materialColor = true;
    depthTest = false;

    this->light = light;
}

void Z_13_05::Objects::changeMaterialColor(const Matrix<float, 1, 4>& color) {
    ambient += color;
    for (int i = 0; i < 4; ++i) {
        if (ambient[i] < 0.0f) {
            ambient[i] = 0.0f;
        }
        if (ambient[i] > 1.0f) {
            ambient[i] = 1.0f;
        }
    }

    diffuse += color;
    for (int i = 0; i < 4; ++i) {
        if (diffuse[i] < 0.0f) {
            diffuse[i] = 0.0f;
        }
        if (diffuse[i] > 1.0f) {
            diffuse[i] = 1.0f;
        }
    }
}

void Z_13_05::Light::init(int number) {
    this->number = number;
    rotX = rotY = 0;
    rotX = -90;
    trZ = 5.0f;
    attenuation = CMatrix<float, 1, 3>::create(1, 0, 0);
    ambient = CMatrix<float, 1, 4>::create(0.2f, 0.2f, 0.2f, 1.0f);
    diffuse = CMatrix<float, 1, 4>::create(0.8f, 0.8f, 0.8f, 1.0f);
    specular = CMatrix<float, 1, 4>::create(1, 1, 1, 1);
    spotCutoff = 90;
    spotExponent = 10;
    
    glEnable(GL_LIGHT0+number);
}

void Z_13_05::Light::updateLight() {
    Matrix<float, 1, 4> pos =
            rotationMatrix(rotY * (M_PI/180), 0, 1, 0) *
            rotationMatrix(rotX * (M_PI/180), 1, 0, 0)
        * CMatrix<float, 1, 4>::create(0, 0, trZ, 1);
    glLightfv(GL_LIGHT0+number, GL_POSITION, pos.data);
    pos = -pos;
    glLightfv(GL_LIGHT0+number, GL_SPOT_DIRECTION, pos.data);

    glLightfv(GL_LIGHT0+number, GL_AMBIENT, ambient.data);
    glLightfv(GL_LIGHT0+number, GL_DIFFUSE, diffuse.data);
    glLightfv(GL_LIGHT0+number, GL_SPECULAR, specular.data);

    glLightfv(GL_LIGHT0+number, GL_SPOT_CUTOFF, &spotCutoff);
    glLightfv(GL_LIGHT0+number, GL_SPOT_EXPONENT, &spotExponent);
    
    glLightfv(GL_LIGHT0+number, GL_CONSTANT_ATTENUATION, &attenuation[0]);
    glLightfv(GL_LIGHT0+number, GL_LINEAR_ATTENUATION, &attenuation[1]);
    glLightfv(GL_LIGHT0+number, GL_QUADRATIC_ATTENUATION, &attenuation[2]);
}

void Z_13_05::Light::rotateX(float degrees) {
    if (rotX + degrees > 90) {
        rotX = 90;
        return;
    }
    if (rotX + degrees < -90) {
        rotX = -90;
        return;
    }
    rotX += degrees;
}

void Z_13_05::Light::rotateY(float degrees) {
    rotY += degrees;
    if (rotY > 180) {
        rotY -= 360;
        return;
    }
    if (rotY < -180) {
        rotY += 360;
        return;
    }
}

void Z_13_05::Light::zoomZ(float meters) {
    if (trZ + meters > 16) {
        trZ = 16;
        return;
    }
    if (trZ + meters < 0) {
        trZ = 0;
        return;
    }
    trZ += meters;
}

void Z_13_05::Camera::init() {
    scale = 100000.0f;
    rotX = rotY = 0;
    trZ = 5.0f;
}

void Z_13_05::Camera::recomputeMatrices() {
    Application& app = Application::instance();
    float w = app.state().width/scale;
    float h = app.state().height/scale;

    //projectionMatrix = frustumMatrix(-w, w, -h, h, 0.01f, 25.0f);
    projectionMatrix = perspectiveMatrix(2*atan(h*100.0f), w/h, 0.01f, 25.0f);
    viewMatrix = translationMatrix(0, 0, -trZ) *
                 rotationMatrix(rotX * (M_PI/180), 1, 0, 0) *
                 rotationMatrix(rotY * (M_PI/180), 0, 1, 0);
    Matrix<float, 1, 4> eye4 = rotationMatrix(rotY * (M_PI/180), 0, -1, 0) *
                               rotationMatrix(rotX * (M_PI/180), -1, 0, 0) *
                               CMatrix<float, 1, 4>::create(0, 0, trZ, 1);
    Matrix<float, 1, 4> up4 = rotationMatrix(rotY * (M_PI/180), 0, -1, 0) *
                              rotationMatrix(rotX * (M_PI/180), -1, 0, 0) *
                              CMatrix<float, 1, 4>::create(0, 1, 0, 1);
    viewMatrix = lookAtMatrix(Matrix<float, 1, 3> (eye4.data),
                              CMatrix<float, 1, 3>::create(0, 0, 0),
                              Matrix<float, 1, 3> (up4.data));

    ::Camera::recomputeMatrices();
}

void Z_13_05::Camera::rotateX(float degrees) {
    if (rotX + degrees > 90) {
        rotX = 90;
        return;
    }
    if (rotX + degrees < -90) {
        rotX = -90;
        return;
    }
    rotX += degrees;
}

void Z_13_05::Camera::rotateY(float degrees) {
    rotY += degrees;
    if (rotY > 180) {
        rotY -= 360;
        return;
    }
    if (rotY < -180) {
        rotY += 360;
        return;
    }
}

void Z_13_05::Camera::zoomZ(float meters) {
    if (trZ + meters > 16) {
        trZ = 16;
        return;
    }
    if (trZ + meters < 0) {
        trZ = 0;
        return;
    }
    trZ += meters;
}

void Z_13_05::load(Application& app) {
    camera.init();
    light.init(0);

    objects.init(&light);
    renderer = app.state().rendererManager.create<Objects>();
    renderer->init(&objects);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    glEnable(GL_LIGHTING);

    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);

    glAlphaFunc(GL_NOTEQUAL, 0.0f);
    glEnable(GL_ALPHA_TEST);

    glDepthFunc(GL_LESS);
    glDepthMask(GL_TRUE);
    glClearDepth(1.0f);
}

void Z_13_05::unload(Application& app) {
}

void Z_13_05::activate(Application&) {
}

void Z_13_05::suspend(Application&) {
}

void Z_13_05::resized(Application&) {
}

void Z_13_05::keyPressed(Application& app, int which) {
    const Application::State& state = app.state();

    switch (which) {
    case VK_ESCAPE:
        app.quit();
        break;
    case VK_LEFT:
        if (state.keys[VK_SHIFT]) {
            light.rotateY(4);
        } else if (state.keys[VK_CONTROL]) {
            objects.surface.changeVertex(CMatrix<float, 1, 3>::create(-0.05f, 0.0f, 0.0f));
        } else {
            camera.rotateY(1);
        }
        break;
    case VK_RIGHT:
        if (state.keys[VK_SHIFT]) {
            light.rotateY(-4);
        } else if (state.keys[VK_CONTROL]) {
            objects.surface.changeVertex(CMatrix<float, 1, 3>::create(0.05f, 0.0f, 0.0f));
        } else {
            camera.rotateY(-1);
        }
        break;
    case VK_DOWN:
        if (state.keys[VK_SHIFT]) {
            light.rotateX(4);
        } else if (state.keys[VK_CONTROL]) {
            objects.surface.changeVertex(CMatrix<float, 1, 3>::create(0.0f, -0.05f, 0.0f));
        } else {
            camera.rotateX(1);
        }
        break;
    case VK_UP:
        if (state.keys[VK_SHIFT]) {
            light.rotateX(-4);
        } else if (state.keys[VK_CONTROL]) {
            objects.surface.changeVertex(CMatrix<float, 1, 3>::create(0.0f, 0.05f, 0.0f));
        } else {
            camera.rotateX(-1);
        }
        break;
    case VK_PRIOR:
        if (state.keys[VK_SHIFT]) {
            light.zoomZ(0.1f);
        } else if (state.keys[VK_CONTROL]) {
            objects.surface.changeVertex(CMatrix<float, 1, 3>::create(0.0f, 0.0f, -0.05f));
        } else {
            camera.zoomZ(0.1f);
        }
        break;
    case VK_NEXT:
        if (state.keys[VK_SHIFT]) {
            light.zoomZ(-0.1f);
        } else if (state.keys[VK_CONTROL]) {
            objects.surface.changeVertex(CMatrix<float, 1, 3>::create(0.0f, 0.0f, 0.05f));
        } else {
            camera.zoomZ(-0.1f);
        }
        break;
    case '1':
        if (state.keys[VK_SHIFT]) {
            if (light.ambient[0] > 0) {
                light.ambient -= 0.05f;
            }
        } else {
            if (light.ambient[0] < 1) {
                light.ambient += 0.05f;
            }
        }
        break;
    case '2':
        if (state.keys[VK_SHIFT]) {
            if (light.diffuse[0] > 0) {
                light.diffuse -= 0.05f;
            }
        } else {
            if (light.diffuse[0] < 1) {
                light.diffuse += 0.05f;
            }
        }
        break;
    case '3':
        if (state.keys[VK_SHIFT]) {
            if (light.specular[0] > 0) {
                light.specular -= 0.05f;
            }
        } else {
            if (light.specular[0] < 1) {
                light.specular += 0.05f;
            }
        }
        break;
    case '4':
        if (state.keys[VK_SHIFT]) {
            if (objects.shininess > 0) {
                objects.shininess -= 1;
            }
        } else {
            if (objects.shininess < 128) {
                objects.shininess += 1;
            }
        }
        break;
    case '5':
        if (state.keys[VK_SHIFT]) {
            if (objects.ambient[0] > 0) {
                objects.ambient -= 0.05f;
            }
        } else {
            if (objects.ambient[0] < 1) {
                objects.ambient += 0.05f;
            }
        }
        break;
    case '6':
        if (state.keys[VK_SHIFT]) {
            if (objects.diffuse[0] > 0) {
                objects.diffuse -= 0.05f;
            }
        } else {
            if (objects.diffuse[0] < 1) {
                objects.diffuse += 0.05f;
            }
        }
        break;
    case '7':
        if (state.keys[VK_SHIFT]) {
            if (objects.specular[0] > 0) {
                objects.specular -= 0.05f;
            }
        } else {
            if (objects.specular[0] < 1) {
                objects.specular += 0.05f;
            }
        }
        break;
    case 'A':
        objects.surface.changeChosen(0, -1);
        break;
    case 'D':
        objects.surface.changeChosen(0, 1);
        break;
    case 'W':
        objects.surface.changeChosen(-1, 0);
        break;
    case 'S':
        objects.surface.changeChosen(1, 0);
        break;
    case 'Z': {
        Matrix<float, 1, 4> color;
        if (!state.keys[VK_SHIFT]) {
            color = CMatrix<float, 1, 4>::create(0.1f, 0.0f, 0.0f, 0.0f);
        } else {
            color = CMatrix<float, 1, 4>::create(-0.1f, 0.0f, 0.0f, 0.0f);
        }
        if (objects.materialColor) {
            objects.surface.changeColor(color);
        } else {
            objects.changeMaterialColor(color);
        }
        } break;
    case 'X': {
        Matrix<float, 1, 4> color;
        if (!state.keys[VK_SHIFT]) {
            color = CMatrix<float, 1, 4>::create(0.0f, 0.1f, 0.0f, 0.0f);
        } else {
            color = CMatrix<float, 1, 4>::create(0.0f, -0.1f, 0.0f, 0.0f);
        }
        if (objects.materialColor) {
            objects.surface.changeColor(color);
        } else {
            objects.changeMaterialColor(color);
        }
        } break;
    case 'C': {
        Matrix<float, 1, 4> color;
        if (!state.keys[VK_SHIFT]) {
            color = CMatrix<float, 1, 4>::create(0.0f, 0.0f, 0.1f, 0.0f);
        } else {
            color = CMatrix<float, 1, 4>::create(0.0f, 0.0f, -0.1f, 0.0f);
        }
        if (objects.materialColor) {
            objects.surface.changeColor(color);
        } else {
            objects.changeMaterialColor(color);
        }
        } break;
    case 'V': {
        Matrix<float, 1, 4> color;
        if (!state.keys[VK_SHIFT]) {
            color = CMatrix<float, 1, 4>::create(0.0f, 0.0f, 0.0f, 0.1f);
        } else {
            color = CMatrix<float, 1, 4>::create(0.0f, 0.0f, 0.0f, -0.1f);
        }
        if (objects.materialColor) {
            objects.surface.changeColor(color);
        } else {
            objects.changeMaterialColor(color);
        }
        } break;
    case 'B': {
        Matrix<float, 1, 4> color;
        if (!state.keys[VK_SHIFT]) {
            color = CMatrix<float, 1, 4>::create(0.1f, 0.1f, 0.1f, 0.0f);
        } else {
            color = CMatrix<float, 1, 4>::create(-0.1f, -0.1f, -0.1f, 0.0f);
        }
        if (objects.materialColor) {
            objects.surface.changeColor(color);
        } else {
            objects.changeMaterialColor(color);
        }
        } break;
    case '8':
        objects.surface.mode =  GLU_OUTLINE_PATCH;
        break;
    case '9':
        objects.surface.mode =  GLU_OUTLINE_POLYGON;
        break;
    case '0':
        objects.surface.mode = GLU_FILL;
        break;
    case 'Q':
        if (!state.keys[VK_SHIFT]) {
            objects.surface.changeN(2.0f);
        } else {
            objects.surface.changeN(0.5f);
        }
        break;
    case 'R':
        objects.surface.resetColors();
        break;
    case 'T':
        objects.surface.useTexture ^= 1;
        break;
    case VK_TAB:
        objects.surface.changeAxis();
        break;
    case 'O':
        objects.surface.changeChosen(-1);
        break;
    case 'P':
        objects.surface.changeChosen(1);
        break;
    case 'K':
        objects.surface.changeValue(-0.05f);
        break;
    case 'L':
        objects.surface.changeValue(0.05f);
        break;
    case VK_NUMPAD1:
        objects.surface.changeDegree(1);
        break;
    case VK_NUMPAD2:
        objects.surface.changeDegree(2);
        break;
    case VK_NUMPAD3:
        objects.surface.changeDegree(3);
        break;
    case VK_NUMPAD4:
        objects.surface.changeDegree(4);
        break;
    case 'E':
        objects.materialColor ^= 1;
        break;
    case 'F':
        objects.surface.controlPoints ^= 1;
        break;
    case 'G':
        objects.depthTest ^= 1;
        break;
    }
}

void Z_13_05::keyReleased(Application&, int) {
}

void Z_13_05::update(Application&, long) {
}

void Z_13_05::repaint(Application&) {
    camera.recomputeMatrices();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    DefaultContext ctx = {&camera};
    renderer->draw(ctx);
}
