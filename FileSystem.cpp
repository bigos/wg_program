/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#include "FileSystem.h"

#include "Application.h"

#include <fstream>

FileSystem fs;

void FileSystem::init() {
    getRootDirectory();
    getOtherDirectories();
}

void FileSystem::getRootDirectory() {
    const std::string& root = Application::instance().options().root.value();
    bool Absolute = false;
    if (!root.empty()) {
        if (root[0] == '/') {
            Absolute = true;
        } else if (root.size() >= 2 && root[1] == ':') {
            Absolute = true;
        }
    }

    if (Absolute) {
        m_root = root;
    } else {
        const size_t size = 2048;
        wchar_t buffer[size];
        GetModuleFileName(NULL, buffer, size);
        buffer[size-1] = 0;
        m_root = narrow(buffer);
        size_t pos = m_root.find_last_of("/\\");
        if (pos == std::string::npos) {
            m_root = "./";
        } else {
            m_root.erase(pos);
            m_root += '/';
        }
        m_root += root;
    }

    if (m_root[m_root.size()-1] != '/') {
        m_root += '/';
    }
}

void FileSystem::getOtherDirectories() {
    m_shaders = m_root + "shaders/";
}

std::string readFile(const std::string& name) {
    std::ifstream file;
    file.exceptions(std::ios::failbit | std::ios::badbit);
    file.open(widen(name).c_str(), std::ios::in | std::ios::binary);

    file.seekg(0, std::ios::end);
    size_t size = (size_t)file.tellg();
    file.seekg(0, std::ios::beg);

    std::string ret;
    ret.resize(size);

    file.read(&ret[0], size);

    return ret;
}
