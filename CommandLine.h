/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#ifndef CommandLine_h
#define CommandLine_h

#include "Exceptions.h"

#include <string>
#include <unordered_map>

namespace CommandLine {

class Parser;

class OptionBase {
    friend class Parser;
    
public:
    OptionBase() : m_shortName(0), m_longName(nullptr) {}
    
    void registerIn(Parser& parser);

protected:
    char m_shortName;
    const char* m_longName;

    virtual void parseLong(size_t position, const std::string& argument) = 0;
    virtual void parseShort(size_t position) = 0;
};

template <typename Type>
class Option : public OptionBase {
    friend class Parser;

public:
    Option() : m_value() {}

    const Type& value() const { return m_value; }
    
    Option& shortName(char name) {
        m_shortName = name;
        return *this;
    }

    Option& longName(const char* name) {
        m_longName = name;
        return *this;
    }

    Option& byDefault(const Type& def) {
        m_value = def;
        return *this;
    }

private:
    Type m_value;
    
    virtual void parseLong(size_t position, const std::string& argument);
    virtual void parseShort(size_t position);
};

class Parser {
    friend class OptionBase;

public:
    Parser(const char* cmdLine) : line(cmdLine) {}
    
    const std::string line;

    void parse();

private:
    typedef std::unordered_map<char, OptionBase*> ShortNames;
    typedef std::unordered_map<std::string, OptionBase*> LongNames;

    ShortNames shortNames;
    LongNames longNames;
};

class ParserException : public Exception {
public:
    const size_t position;

protected:
    ParserException(const char* title, size_t position) :
        ::Exception(title), position(position) {}

    void describeWhere(std::ostream& os) const throw();
};

class UnknownArgument : public ParserException {
public:
    UnknownArgument(size_t position) :
        ParserException("Unknown Argument", position) {}

    virtual void describe(std::ostream& os) const throw();

protected:
    UnknownArgument(const char* title, size_t position) :
        ParserException(title, position) {}
};

class EmptyOption : public ParserException {
public:
    EmptyOption(size_t position, const std::string& longName) :
        ParserException("Empty Long Option", position),
        longName(longName) {}

    virtual void describe(std::ostream& os) const throw();

    const std::string longName;

protected:
    EmptyOption(const char* title, size_t position, const std::string& longName) :
        ParserException(title, position),
        longName(longName) {}
};

class WrongOption : public ParserException {
public:
    WrongOption(size_t position, const std::string& longName,
                const std::string& argument) :
        ParserException("Wrong Long Option", position),
        longName(longName), argument(argument) {}

    virtual void describe(std::ostream& os) const throw();

    const std::string longName, argument;

protected:
    WrongOption(const char* title, size_t position,
                const std::string& longName, const std::string& argument) :
        ParserException(title, position),
        longName(longName), argument(argument) {}
};

class UnknownOption : public ParserException {
public:
    UnknownOption(size_t position, const std::string& longName) :
        ParserException("Unknown Long Option", position),
        shortName(0), longName(longName) {}
    UnknownOption(size_t position, char shortName) :
        ParserException("Unknown Long Option", position),
        shortName(shortName), longName() {}

    virtual void describe(std::ostream& os) const throw();

    const char shortName;
    const std::string longName;

    bool isShort() const { return shortName != 0; }
    bool isLong() const { return !isShort(); }

protected:
    UnknownOption(const char* title, size_t position, const std::string& longName) :
        ParserException(title, position),
        shortName(0), longName(longName) {}
    UnknownOption(const char* title, size_t position, char shortName) :
        ParserException(title, position),
        shortName(shortName), longName() {}
};

} // end of CommandLine

#endif
