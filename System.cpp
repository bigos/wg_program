/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#include "System.h"

#include <string.h>

size_t doWidening(const char* str, size_t n, wchar_t* wstr, size_t wn) {
    wn = (size_t)MultiByteToWideChar(CP_UTF8, 0, str, (int)n, wstr, (int)wn);
    wstr[wn] = 0;
    return wn;
}

size_t doNarrowing(const wchar_t* wstr, size_t wn, char* str, size_t n) {
    n = (size_t)WideCharToMultiByte(CP_UTF8, 0, wstr, (int)wn, str, (int)n, NULL, NULL);
    str[n] = 0;
    return n;
}
