/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#include "Z_25_03.h"
#include "Buffer.h"
#include "Reserver.h"
#include "Shader.h"
#include "Vertex.h"

template <>
class Renderer_1_1<Z_25_03::Plane> : public Renderer<Z_25_03::Plane> {
public:
    virtual ~Renderer_1_1() {}

    virtual void init(const Z_25_03::Plane* object, size_t num) {
        plane = object;
        this->num = num;
    }

    virtual void draw(DefaultContext& context) {
        glLineWidth(1.0f);
        for (size_t i = 0; i < num; ++i) {
            const Z_25_03::Plane& plane = this->plane[i];
            glColor3f(plane.color[0], plane.color[1], plane.color[2]);
            
            const bool x0 = (plane.dimen[0] == 0);
            
            glBegin(GL_LINE_LOOP);
                glVertex3f(plane.dimen[0], plane.dimen[1], plane.dimen[2]);
                if (x0) {
                    glVertex3f(0, -plane.dimen[1], plane.dimen[2]);
                } else {
                    glVertex3f(plane.dimen[0], -plane.dimen[1], -plane.dimen[2]);
                }
                glVertex3f(-plane.dimen[0], -plane.dimen[1], -plane.dimen[2]);
                if (x0) {
                    glVertex3f(0, plane.dimen[1], -plane.dimen[2]);
                } else {
                    glVertex3f(-plane.dimen[0], plane.dimen[1], plane.dimen[2]);
                }
            glEnd();
        }
    }

private:
    const Z_25_03::Plane* plane;
    size_t num;
};

template <>
class Renderer_1_1<Z_25_03::Axis> : public Renderer<Z_25_03::Axis> {
public:
    virtual ~Renderer_1_1() {}

    virtual void init(const Z_25_03::Axis* object, size_t num) {
        axis = object;
        this->num = num;
    }

    virtual void draw(DefaultContext& context) {
        glLineWidth(2.0f);
        glBegin(GL_LINES);
        for (size_t i = 0; i < num; ++i) {
            const Z_25_03::Axis& axis = this->axis[i];
            glColor3f(axis.axis[0], axis.axis[1], axis.axis[2]);

            Matrix<float, 1, 3> rev(1);
            rev -= axis.axis;
            rev *= 0.1f;


            glVertex3f(-axis.axis[0], -axis.axis[1], -axis.axis[2]);
            glVertex3f(axis.axis[0], axis.axis[1], axis.axis[2]);
                
            glVertex3f(axis.axis[0], axis.axis[1], axis.axis[2]);
            Matrix<float, 1, 3> axis2(axis.axis * 0.9f + rev);
            glVertex3f(axis2[0], axis2[1], axis2[2]);

            glVertex3f(axis.axis[0], axis.axis[1], axis.axis[2]);
            axis2 = (axis.axis * 0.9f - rev);
            glVertex3f(axis2[0], axis2[1], axis2[2]);
        }
        glEnd();
    }

private:
    const Z_25_03::Axis* axis;
    size_t num;
};

template <>
class Renderer_1_1<Z_25_03::Objects> : public Renderer<Z_25_03::Objects> {
public:
    virtual ~Renderer_1_1() {}

    virtual void init(const Z_25_03::Objects* object, size_t num) {
        assert(num == 1);
        plane.init(&object->planeX, 3);
        axis.init(&object->axisX, 3);
    }

    virtual void draw(DefaultContext& context) {
        glMatrixMode(GL_PROJECTION);
        glLoadMatrixf(context.camera->matrix().data);

        plane.draw(context);
        axis.draw(context);
    }

private:
    Renderer_1_1<Z_25_03::Plane> plane;
    Renderer_1_1<Z_25_03::Axis> axis;
};

namespace {
    class FlatColorProgram {
    public:
        void init() {
            Shader::Source* vss = Shader::Source::get("flatColor.4.3.vs");
            Shader::Source* fss = Shader::Source::get("flatColor.4.3.fs");

            vs.setSource(vss);
            fs.setSource(fss);
            program.addObject(vs);
            program.addObject(fs);
            program.link();

            transformMatrix = program.getUniform("uTransformMatrix");
        }
        
        Shader::Object vs, fs;
        Shader::Program program;
        Shader::Uniform transformMatrix;

        static const GLuint position = 0;
        static const GLuint color = 1;
    };
}

namespace {
    struct PlaneOptions {
        struct Vertex {
            signed char x, y, z, pad1;
            unsigned char r, g, b, pad2;
        };
        static_assert(sizeof(Vertex) == 8, "Vertex is not packed");

        typedef Z_25_03::Plane Object;

        static const bool PrimitiveRestart = true;
        static const unsigned NumVertices = 4;
        static const unsigned NumIndices = 5;
    };
}

template <>
class Renderer_4_3<Z_25_03::Plane> : public ReserveRenderer<Renderer_4_3<Z_25_03::Plane>, PlaneOptions> {
    friend class ReserveRenderer<Renderer_4_3<Z_25_03::Plane>, PlaneOptions>;
    typedef PlaneOptions::Vertex VertexStruct;
    
public:
    void draw(DefaultContext& context) {
        Vertex::Bind array(this->array);
        
        glEnable(GL_PRIMITIVE_RESTART);
        glDrawElements(GL_LINE_LOOP, 5*num, indexType(), (char*)0 + indexOffset);
        glDisable(GL_PRIMITIVE_RESTART);
    }

private:
    void initSpecification() {
        using namespace Vertex;
        Bind array(this->array);
        Buffer::ElementArray::bind(indices->buffer);

        Attribute position(FlatColorProgram::position);
        Attribute color(FlatColorProgram::color);

        position.enable();
        position.setPointer(GL_BYTE, false, 3, sizeof VertexStruct, vertexOffset + offsetof(VertexStruct, x));

        color.enable();
        color.setPointer(GL_UNSIGNED_BYTE, true, 3, sizeof VertexStruct, vertexOffset + offsetof(VertexStruct, r));
    }

    void initObject(const Z_25_03::Plane& plane, VertexStruct* V) {
        unsigned char r = Vertex::normalizedToUInt8(plane.color[0]);
        unsigned char g = Vertex::normalizedToUInt8(plane.color[1]);
        unsigned char b = Vertex::normalizedToUInt8(plane.color[2]);
        for (int j = 0; j < 4; ++j) {
            V[j].r = r;
            V[j].g = g;
            V[j].b = b;
        }
            
        const bool x0 = (plane.dimen[0] == 0);

        V[0].x = (signed char)plane.dimen[0];
        V[0].y = (signed char)plane.dimen[1];
        V[0].z = (signed char)plane.dimen[2];

        if (x0) {
            V[1].x = 0;
            V[1].y = (signed char)-plane.dimen[1];
            V[1].z = (signed char)plane.dimen[2];
        } else {
            V[1].x = (signed char)plane.dimen[0];
            V[1].y = (signed char)-plane.dimen[1];
            V[1].z = (signed char)-plane.dimen[2];
        }
        V[2].x = (signed char)-plane.dimen[0];
        V[2].y = (signed char)-plane.dimen[1];
        V[2].z = (signed char)-plane.dimen[2];
        if (x0) {
            V[3].x = 0;
            V[3].y = (signed char)plane.dimen[1];
            V[3].z = (signed char)-plane.dimen[2];
        } else {
            V[3].x = (signed char)-plane.dimen[0];
            V[3].y = (signed char)plane.dimen[1];
            V[3].z = (signed char)plane.dimen[2];
        }
    }

    template <typename Number>
    void addIndices(Number* buf, unsigned num) {
        buf[0] = num++;
        buf[1] = num++;
        buf[2] = num++;
        buf[3] = num;
        buf[4] = Number(~0ULL);
    }
};

namespace {
    struct AxisOptions {
        struct Vertex {
            signed char x, y, z, pad1;
            unsigned char r, g, b, pad2;
        };
        static_assert(sizeof(Vertex) == 8, "Vertex is not packed");

        typedef Z_25_03::Axis Object;

        static const bool PrimitiveRestart = false;
        static const unsigned NumVertices = 4;
        static const unsigned NumIndices = 6;
    };
}

template <>
class Renderer_4_3<Z_25_03::Axis> : public ReserveRenderer<Renderer_4_3<Z_25_03::Axis>, AxisOptions> {
    friend class ReserveRenderer<Renderer_4_3<Z_25_03::Axis>, AxisOptions>;
    typedef AxisOptions::Vertex VertexStruct;

public:
    void draw(DefaultContext& context) {
        Vertex::Bind array(this->array);

        glDrawElements(GL_LINES, 6*num, indexType(), (char*)0 + indexOffset);
    }

private:
    void initSpecification() {
        using namespace Vertex;
        Bind array(this->array);
        Buffer::ElementArray::bind(indices->buffer);

        Attribute position(FlatColorProgram::position);
        Attribute color(FlatColorProgram::color);

        position.enable();
        position.setPointer(GL_BYTE, true, 3, sizeof VertexStruct, vertexOffset + offsetof(VertexStruct, x));

        color.enable();
        color.setPointer(GL_UNSIGNED_BYTE, true, 3, sizeof VertexStruct, vertexOffset + offsetof(VertexStruct, r));
    }

    void initObject(const Z_25_03::Axis& axis, VertexStruct* V) {
        unsigned char r = Vertex::normalizedToUInt8(axis.axis[0]);
        unsigned char g = Vertex::normalizedToUInt8(axis.axis[1]);
        unsigned char b = Vertex::normalizedToUInt8(axis.axis[2]);
        for (int j = 0; j < 4; ++j) {
            V[j].r = r;
            V[j].g = g;
            V[j].b = b;
        }

        Matrix<float, 1, 3> rev(1);
        rev -= axis.axis;
        rev *= 0.1f;

        V[0].x = Vertex::normalizedToInt8(axis.axis[0]);
        V[0].y = Vertex::normalizedToInt8(axis.axis[1]);
        V[0].z = Vertex::normalizedToInt8(axis.axis[2]);
            
        V[1].x = Vertex::normalizedToInt8(-axis.axis[0]);
        V[1].y = Vertex::normalizedToInt8(-axis.axis[1]);
        V[1].z = Vertex::normalizedToInt8(-axis.axis[2]);
            
        Matrix<float, 1, 3> axis2(axis.axis * 0.9f + rev);
        V[2].x = Vertex::normalizedToInt8(axis2[0]);
        V[2].y = Vertex::normalizedToInt8(axis2[1]);
        V[2].z = Vertex::normalizedToInt8(axis2[2]);
            
        axis2 = (axis.axis * 0.9f - rev);
        V[3].x = Vertex::normalizedToInt8(axis2[0]);
        V[3].y = Vertex::normalizedToInt8(axis2[1]);
        V[3].z = Vertex::normalizedToInt8(axis2[2]);
    }

    template <typename Number>
    void addIndices(Number* buf, unsigned num) {
        buf[0] = num;
        buf[2] = num;
        buf[4] = num;
        buf[1] = ++num;
        buf[3] = ++num;
        buf[5] = ++num;
    }
};

template <>
class Renderer_4_3<Z_25_03::Objects> : public Renderer<Z_25_03::Objects> {
public:
    Renderer_4_3() : vertexReserver(vertices), indexReserver(indices) {}
    virtual ~Renderer_4_3() {}

    virtual void init(const Z_25_03::Objects* object, size_t num) {
        assert(num == 1);
        
        program.init();
        plane.reserve(vertexReserver, indexReserver, 3);
        axis.reserve(vertexReserver, indexReserver, 3);
        
        {
            Buffer::Array V(vertices);
            vertexReserver.allocate(V);
            Buffer::ElementArray I(indices);
            indexReserver.allocate(I);
            Buffer::Map<GL_ARRAY_BUFFER> mapV(V, GL_WRITE_ONLY);
            Buffer::Map<GL_ELEMENT_ARRAY_BUFFER> mapI(I, GL_WRITE_ONLY);
            vertexReserver.map(mapV);
            indexReserver.map(mapI);

            plane.init(&object->planeX);
            axis.init(&object->axisX);
        }
    }

    virtual void draw(DefaultContext& context) {
        Shader::Bind program(this->program.program);
        this->program.transformMatrix = context.camera->matrix();

        plane.draw(context);
        axis.draw(context);
    }

private:
    Renderer_4_3<Z_25_03::Plane> plane;
    Renderer_4_3<Z_25_03::Axis> axis;
    FlatColorProgram program;
    Buffer::Buffer vertices, indices;
    Reserver vertexReserver, indexReserver;
};

void Z_25_03::Objects::init() {
    planeX.init(CMatrix<float, 1, 3>::create(0, 3, 4),
                CMatrix<float, 1, 3>::create(1, 0, 0));
    planeY.init(CMatrix<float, 1, 3>::create(4, 0, 4),
                CMatrix<float, 1, 3>::create(0, 1, 0));
    planeZ.init(CMatrix<float, 1, 3>::create(4, 3, 0),
                CMatrix<float, 1, 3>::create(0, 0, 1));
    axisX.init(CMatrix<float, 1, 3>::create(1, 0, 0));
    axisY.init(CMatrix<float, 1, 3>::create(0, 1, 0));
    axisZ.init(CMatrix<float, 1, 3>::create(0, 0, 1));
}

void Z_25_03::Camera::init() {
    scale = 100000.0f;
    rotX = rotY = 0;
    trZ = 5.0f;
}

void Z_25_03::Camera::recomputeMatrices() {
    Application& app = Application::instance();
    float w = app.state().width/scale;
    float h = app.state().height/scale;

    //projectionMatrix = frustumMatrix(-w, w, -h, h, 0.01f, 25.0f);
    projectionMatrix = perspectiveMatrix(2*atan(h*100.0f), w/h, 0.01f, 25.0f);
    viewMatrix = translationMatrix(0, 0, -trZ) *
                 rotationMatrix(rotX * (M_PI/180), 1, 0, 0) *
                 rotationMatrix(rotY * (M_PI/180), 0, 1, 0);
    Matrix<float, 1, 4> eye4 = rotationMatrix(rotY * (M_PI/180), 0, -1, 0) *
                               rotationMatrix(rotX * (M_PI/180), -1, 0, 0) *
                               CMatrix<float, 1, 4>::create(0, 0, trZ, 1);
    Matrix<float, 1, 4> up4 = rotationMatrix(rotY * (M_PI/180), 0, -1, 0) *
                              rotationMatrix(rotX * (M_PI/180), -1, 0, 0) *
                              CMatrix<float, 1, 4>::create(0, 1, 0, 1);
    viewMatrix = lookAtMatrix(Matrix<float, 1, 3> (eye4.data),
                              CMatrix<float, 1, 3>::create(0, 0, 0),
                              Matrix<float, 1, 3> (up4.data));

    ::Camera::recomputeMatrices();
}

void Z_25_03::Camera::rotateX(float degrees) {
    if (rotX + degrees > 90) {
        rotX = 90;
        return;
    }
    if (rotX + degrees < -90) {
        rotX = -90;
        return;
    }
    rotX += degrees;
}

void Z_25_03::Camera::rotateY(float degrees) {
    rotY += degrees;
    if (rotY > 180) {
        rotY -= 360;
        return;
    }
    if (rotY < -180) {
        rotY += 360;
        return;
    }
}

void Z_25_03::Camera::zoomZ(float meters) {
    if (trZ + meters > 16) {
        trZ = 16;
        return;
    }
    if (trZ + meters < 0) {
        trZ = 0;
        return;
    }
    trZ += meters;
}

void Z_25_03::load(Application& app) {
    camera.init();

    objects.init();
    renderer = app.state().rendererManager.create<Objects>();
    renderer->init(&objects);

    glClearColor(0.9f, 0.9f, 0.9f, 1.0f);
}

void Z_25_03::unload(Application& app) {
}

void Z_25_03::activate(Application&) {
}

void Z_25_03::suspend(Application&) {
}

void Z_25_03::resized(Application&) {
    //TODO
    //computeProjectionMatrix();
}

void Z_25_03::keyPressed(Application& app, int which) {
    const Application::State& state = app.state();

    switch (which) {
    case VK_ESCAPE:
        app.quit();
        break;
    case VK_LEFT:
        camera.rotateY(1);
        break;
    case VK_RIGHT:
        camera.rotateY(-1);
        break;
    case VK_DOWN:
        camera.rotateX(1);
        break;
    case VK_UP:
        camera.rotateX(-1);
        break;
    case VK_PRIOR:
        camera.zoomZ(0.1f);
        break;
    case VK_NEXT:
        camera.zoomZ(-0.1f);
        break;
    }
}

void Z_25_03::keyReleased(Application&, int) {
}

void Z_25_03::update(Application&, long) {
}

void Z_25_03::repaint(Application&) {
    camera.recomputeMatrices();

    glClear(GL_COLOR_BUFFER_BIT);

    DefaultContext ctx = {&camera};
    renderer->draw(ctx);
}
