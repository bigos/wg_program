/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#ifndef Z_13_05_h
#define Z_13_05_h

#include "Application.h"
#include "Camera.h"
#include "GL.h"
#include "Matrix.h"
#include "Scene.h"
#include "Texture.h"

class Z_13_05 : public Scene {
public:
    class Camera : public ::Camera {
    public:
        void init();
        
        const Matrix<float, 4, 4>& getProjectionMatrix() const { return projectionMatrix; }
        const Matrix<float, 4, 4>& getViewMatrix() const { return viewMatrix; }

        void recomputeMatrices();
        
        void rotateX(float degrees);
        void rotateY(float degrees);
        void zoomZ(float meters);

        float getRotX() const { return rotX; }
        float getRotY() const { return rotY; }

    private:
        float scale, rotX, rotY, trZ;
    };

    class Light {
    public:
        void init(int number);
        void updateLight();
        
        void rotateX(float degrees);
        void rotateY(float degrees);
        void zoomZ(float meters);

        int number;
        float rotX, rotY, trZ;
        Matrix<float, 1, 3> attenuation;
        Matrix<float, 1, 4> ambient, diffuse, specular;
        float spotCutoff, spotExponent;
    };

public:
    class Surface {
    public:
        void init();

        void changeChosen(int x, int y);
        void changeVertex(const Matrix<float, 1, 3>& vertex);
        void changeColor(const Matrix<float, 1, 4>& color);
        void changeN(float factor);
        void resetColors();

        void changeAxis();
        void changeChosen(int i);
        void changeValue(float v);
        void changeDegree(int d);

        static const size_t ControlSize = 5;
        static const size_t MaxDegree = 4;

        struct Point {
            Matrix<float, 1, 3> vertex;
        } points[ControlSize*ControlSize];

        float uKnots[ControlSize+MaxDegree+1];
        float vKnots[ControlSize+MaxDegree+1];

        struct PointColorTexture {
            Matrix<float, 1, 4> color;
            Matrix<float, 1, 2> texCoord;
        } colorTexture[2*2];

        GLUnurbs* nurbs;

        Point& operator() (int x, int y);
        const Point& operator() (int x, int y) const {
            return const_cast<Surface&> (*this)(x, y);
        }

        PointColorTexture& ct(int x, int y);
        const PointColorTexture& ct(int x, int y) const {
            return const_cast<Surface&> (*this).ct(x, y);
        }

        PointColorTexture& ct(float x, float y) {
            x /= float(ControlSize-1);
            y /= float(ControlSize-1);

            int xd, yd;

            if (x < 0.5f) {
                xd = 0;
            } else if (x > 0.5f) {
                xd = 1;
            } else {
                return ctDummy;
            }

            if (y < 0.5f) {
                yd = 0;
            } else if (y > 0.5f) {
                yd = 1;
            } else {
                return ctDummy;
            }

            return ct(xd, yd);
        }
        const PointColorTexture& ct(float x, float y) const {
            return const_cast<Surface&> (*this).ct(x, y);
        }

        float pathLength;
        int chosenX, chosenY;
        bool axis;
        int chosenU, chosenV;
        int degree;
        GLenum mode;
        bool controlPoints;

        Texture::Texture texture;
        bool useTexture;

    private:
        PointColorTexture ctDummy;

        float* knots() {
            if (!axis) {
                return uKnots;
            } else {
                return vKnots;
            }
        }

        int& chosen() {
            if (!axis) {
                return chosenU;
            } else {
                return chosenV;
            }
        }
    };

    struct Objects {
        Surface surface;
        Light* light;
        
        Matrix<float, 1, 4> ambient, diffuse, specular;
        float shininess;
        bool materialColor, depthTest;

        void init(Light* light);

        void changeMaterialColor(const Matrix<float, 1, 4>& color);
    };

    Z_13_05() : Scene("Z 13.05.2013") {}

    virtual void load(Application&);
    virtual void unload(Application&);
    virtual void activate(Application&);
    virtual void suspend(Application&);
    virtual void resized(Application&);
    virtual void keyPressed(Application&, int which);
    virtual void keyReleased(Application&, int which);
    virtual void update(Application&, long diff);
    virtual void repaint(Application&);

private:
    Camera camera;
    Light light;
    Objects objects;
    Renderer<Objects>* renderer;
};

#endif
