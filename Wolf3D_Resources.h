/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#ifndef Wolf3D_Resources_h
#define Wolf3D_Resources_h

#include "Matrix.h"
#include "Texture.h"
#include "Wolf3D.h"

#include <stdint.h>
#include <vector>

namespace Wolf3D {

class Palette {
public:
	void init();
    void clear();

	uint32_t resolve(Color color) const { return m_colors[color]; }

private:
	std::vector<uint32_t> m_colors;
};

class Graphics;

template <typename Child>
class Catalog {
public:
	Texture::Texture texture() const { return m_texture; }

	Matrix<float, 1, 2> position(const TexturePosition<Child>& pos,
							        const Matrix<float, 1, 2>& off) const {
		//TODO
		return CMatrix<float, 1, 2>::create(
			(float(pos.x()) + off[0])/float(Child::MaxX+1),
			(float(pos.y()) + off[1])/float(Child::MaxY+1)
		);
	}

protected:
	Texture::Texture m_texture;
};

class WallCatalog : public Catalog<WallCatalog> {
public:
	// There are less than 64 Wall textures of 64x64.
	// We arrange them in 8 rows times 8 columns.

	static const unsigned Width = 64;
	static const unsigned Height = 64;
	static const uint8_t MaxX = 7;
	static const uint8_t MaxY = 7;

    void init(const Graphics&, const Palette&);
};

class SpriteCatalog : public Catalog<SpriteCatalog> {
public:
	// The maximum number of Sprite texture is something near 512.
	// That's a lot, but in reality less than 256 are used.
	// We remove the holes to make a catalog of 256 textures of 64x64.
	// We arrange them in 16 rows times 16 columns.
		
	static const unsigned Width = 64;
	static const unsigned Height = 64;
	static const uint8_t MaxX = 15;
	static const uint8_t MaxY = 15;

	void init(const Graphics&, const Palette&);
};

class DoorCatalog : public Catalog<DoorCatalog> {
public:
	// There are only 8 door textures of 64x64.
	// We arrange them in 2 rows times 4 columns.
		
	static const unsigned Width = 64;
	static const unsigned Height = 64;
	static const uint8_t MaxX = 3;
	static const uint8_t MaxY = 1;

	void init(const Graphics&, const Palette&);
};

class Map {
public:
    typedef uint16_t Tile;

    Map() {}
    Map(unsigned width, unsigned height, unsigned planes) :
        m_width(width), m_height(height), planes(planes, nullptr) {}
    ~Map();

    unsigned width() const { return m_width; }
    unsigned height() const { return m_height; }

    // The plane must be new[]-allocated and is now owned by Map.
    void loadPlane(unsigned num, Tile* plane);

    class Plane {
        friend class Map;

        Plane(const Map* map, Tile* tiles) : map(map), tiles(tiles) {}

    public:
        Plane() : map(nullptr), tiles(nullptr) {}

        Tile& operator() (unsigned x, unsigned y) {
            assert(x < map->width());
            assert(y < map->height());
            return tiles[y*map->width() + x];
        }

        Tile& operator() (const Position& pos) {
            assert(pos.x() < map->width());
            assert(pos.y() < map->height());
            return tiles[pos.y()*map->width() + pos.x()];
        }

        const Tile& operator() (unsigned x, unsigned y) const {
            return (*const_cast<Plane*> (this))(x, y);
        }

        const Tile& operator() (const Position& pos) const {
            return (*const_cast<Plane*> (this))(pos);
        }

    private:
        const Map* map;
        Tile* tiles;
    };

    class Wall {
    public:
        Wall(Tile t) : tile(t) {}
        
        const Tile tile;

        TexturePosition<WallCatalog> tpos() const;
        TexturePosition<DoorCatalog> doorTpos() const;
        
        Orientation doorOrientation() const;

        bool isWall() const { return tile <= 28; }
        bool isConcrete() const { return tile < 106; }
        bool isFloor() const { return !isConcrete(); }
        bool isDoor() const { return (tile >= 90 && tile < 96) | (tile >= 100 && tile < 102); }
    };

    class Object {
    public:
        Object(Tile t) : tile(t) {}

        const Tile tile;

        bool isSpawnPoint() const { return tile >= 19 && tile <= 22; }
        Orientation spawnOrientation() const;
    };

    Plane operator[] (unsigned num) const {
        assert(num < planes.size());
        return Plane(this, planes[num]);
    }

private:
    unsigned m_width, m_height;
    std::vector<Tile*> planes;
};

class GameMaps {
    GameMaps(const GameMaps&);
    void operator = (const GameMaps&);

public:
    GameMaps() : pointers(new uint32_t[100]) {}
    ~GameMaps() { delete[] pointers; }

    void init();
    void clear();

    const Map& map(unsigned num) {
        assert(num < m_maps.size());
        return m_maps[num];
    }

private:
    std::vector<Map> m_maps;

    uint16_t rlewMagic;
    uint32_t* pointers;
    std::string gameMaps;

    void loadHead();
    void loadMap(Map& map, uint32_t pointer);
    Map::Tile* loadPlane(uint32_t pointer, uint32_t length);
};

struct SpriteStruct;

class Graphics {
public:
    class Texture {
    public:
        static const unsigned Width = 64;
        static const unsigned Height = 64;

        Texture() : m_data(0) {}
        ~Texture() { delete[] m_data; }

        // It allocates a data on it's own.
        void loadTexture(const uint8_t* data);

        const Color* data() const { return m_data; }
        void resolve(uint32_t* buffer, uint32_t stride, const Palette& palette) const;

    private:
        Color* m_data;
    };

    class Sprite {
    public:
        static const unsigned Width = 64;
        static const unsigned Height = 64;

        Sprite() : m_data(0), m_mask(0) {}
        ~Sprite() {
            delete[] m_data;
            delete[] m_mask;
        }

        // It allocates a data on it's own.
        void loadSprite(const SpriteStruct* data);

        const Color* data() const { return m_data; }
        const uint8_t* mask() const { return m_mask; }
        void resolve(uint32_t* buffer, uint32_t stride, const Palette& palette) const;

    private:
        Color* m_data;
        uint8_t* m_mask;
    };

    Graphics() : pointers(nullptr) {}

    void init();
    void clear();

    const Texture& texture(unsigned num) const {
        assert(num < m_textures.size());
        return m_textures[num];
    }

    const Sprite& sprite(unsigned num) const {
        assert(num < m_sprites.size());
        return m_sprites[num];
    }

private:
    std::vector<Texture> m_textures;
    std::vector<Sprite> m_sprites;

    std::string vswap;
    unsigned chunks, spritesOffset, soundsOffset;
    uint32_t* pointers;

    void loadHead();
};

} // end of Wolf3D

#endif
