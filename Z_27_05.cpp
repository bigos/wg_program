/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#include "Z_27_05.h"

#include "Log.h"

#define OUTLINE_FONTS

const Scene::Configuration Z_27_05::TheConfiguration = {
    // 600x600 unresizable window. Compatibility profile.
    600, 600,
    false, false,
    true
};

template <>
class Renderer_1_1<Z_27_05::Floor> : public Renderer<Z_27_05::Floor> {
public:
    virtual ~Renderer_1_1() {}

    virtual void init(const Z_27_05::Floor* object, size_t num = 1) {
        floor = object;
        this->num = num;
    }

    virtual void draw(DefaultContext& context) {
        for (size_t i = 0; i < num; ++i) {
            const Z_27_05::Floor& floor = this->floor[i];

            glColor4f(0.7f, 0.7f, 0.7f, 1.0f);
            glBegin(GL_QUADS);
                glVertex3f(0.0f, 0.0f, 0.0f);
                glVertex3f(float(floor.w), 0.0f, 0.0f);
                glVertex3f(float(floor.w), 0.0f, float(floor.h));
                glVertex3f(0.0f, 0.0f, float(floor.h));
            glEnd();

            glColor4f(0.4f, 0.4f, 0.4f, 1.0f);
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            glBegin(GL_QUADS);
            for (int x = 0; x < floor.w; ++x) {
                for (int z = 0; z < floor.h; ++z) {
                    glVertex3f(float(x)+0.05f, 0.0f, float(z)+0.05f);
                    glVertex3f(float(x)+0.95f, 0.0f, float(z)+0.05f);
                    glVertex3f(float(x)+0.95f, 0.0f, float(z)+0.95f);
                    glVertex3f(float(x)+0.05f, 0.0f, float(z)+0.95f);
                }
            }
            glEnd();
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
    }

private:
    const Z_27_05::Floor* floor;
    size_t num;
};

template <>
class Renderer_1_1<Z_27_05::Cuboid> : public Renderer<Z_27_05::Cuboid> {
public:
    virtual ~Renderer_1_1() {}

    virtual void init(const Z_27_05::Cuboid* object, size_t num = 1) {
        cuboid = object;
        this->num = num;
    }

    virtual void draw(DefaultContext& context) {
        for (size_t i = 0; i < num; ++i) {
            const Z_27_05::Cuboid& cuboid = this->cuboid[i];

            glColor4f(0.8f, 0.8f, 0.8f, 1.0f);
            glBegin(GL_QUADS);
                drawSides(cuboid);
                drawTop(cuboid);
            glEnd();

            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            glColor4f(0.0f, 0.0f, 0.0f, 1.0f);
            glBegin(GL_QUADS);
                drawSides(cuboid);
            glEnd();
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
    }

private:
    const Z_27_05::Cuboid* cuboid;
    size_t num;

    void drawSides(const Z_27_05::Cuboid& cuboid) {
        float x = float(cuboid.x);
        float z = float(cuboid.z);
        float h = float(cuboid.h);

        glVertex3f(x, 0, z);
        glVertex3f(x+1, 0, z);
        glVertex3f(x+1, h, z);
        glVertex3f(x, h, z);

        glVertex3f(x, 0, z+1);
        glVertex3f(x+1, 0, z+1);
        glVertex3f(x+1, h, z+1);
        glVertex3f(x, h, z+1);

        glVertex3f(x, 0, z);
        glVertex3f(x, 0, z+1);
        glVertex3f(x, h, z+1);
        glVertex3f(x, h, z);
        
        glVertex3f(x+1, 0, z);
        glVertex3f(x+1, 0, z+1);
        glVertex3f(x+1, h, z+1);
        glVertex3f(x+1, h, z);
    }

    void drawTop(const Z_27_05::Cuboid& cuboid) {
        float x = float(cuboid.x);
        float z = float(cuboid.z);
        float h = float(cuboid.h);

        glVertex3f(x, h, z);
        glVertex3f(x+1, h, z);
        glVertex3f(x+1, h, z+1);
        glVertex3f(x, h, z+1);
    }
};

template <>
class Renderer_1_1<Z_27_05::Objects> : public Renderer<Z_27_05::Objects> {
public:
    virtual ~Renderer_1_1() {}

    virtual void init(const Z_27_05::Objects* object, size_t num) {
        this->object = object;
        assert(num == 1);
        floor.init(&object->floor);
    }

    virtual void draw(DefaultContext& context) {
        glMatrixMode(GL_PROJECTION);
        glLoadMatrixf(((Z_27_05::Camera*)context.camera)->getProjectionMatrix().data);

        glMatrixMode(GL_MODELVIEW);
        glLoadMatrixf(((Z_27_05::Camera*)context.camera)->getViewMatrix().data);

        floor.draw(context);

        Renderer_1_1<Z_27_05::Cuboid> cuboid;
        cuboid.init(&object->cuboids[0], object->cuboids.size());
        cuboid.draw(context);
    }

private:
    const Z_27_05::Objects* object;
    Renderer_1_1<Z_27_05::Floor> floor;
};

void Z_27_05::Objects::init() {
    floor.init(64, 64);

    cuboids.push_back(Cuboid(30, 33, 3));
    cuboids.push_back(Cuboid(31, 33, 2));
    cuboids.push_back(Cuboid(33, 30, 4));
    cuboids.push_back(Cuboid(32, 32, 1));
}

void Z_27_05::Camera::init() {
    scale = 100000.0f;
    rot = 0;
    trX = trZ = 32.0f;
    zm = 16.0f;
}

void Z_27_05::Camera::recomputeMatrices() {
    Application& app = Application::instance();
    float w = app.state().width/scale;
    float h = app.state().height/scale;

    const float s3 = sqrt(3.0f);
    const float s2 = sqrt(2.0f);

    float invSinB = (rot & 1 ? -1.0f : 1.0f);
    float invDir = (rot & 2 ? -1.0f : 1.0f);
    projectionMatrix = CMatrix<float, 4, 4>::create(
        s3*invDir, 1*invSinB*invDir, s2*0.1f*invSinB*invDir, 0.0f,
        0, 2, -s2*0.1f, 0.0f,
        -s3*invSinB*invDir, 1*invDir, s2*0.1f*invDir, 0.0f,
        -trX*s3*invDir+trZ*s3*invSinB*invDir, -trX*invSinB*invDir-trZ*invDir, -trX*s2*0.1f*invSinB*invDir-trZ*s2*0.1f*invDir, zm
    );
    viewMatrix = Matrix<float, 4, 4>();

    ::Camera::recomputeMatrices();
}

void Z_27_05::Camera::rotate(int rights) {
    rot = (rot + rights) & 3;
}

void Z_27_05::Camera::moveX(float x) {
    trX += x;
    if (trX < 0.0f) {
        trX = 0.0f;
    } else if (trX > 64.0f) {
        trX = 64.0f;
    }
}

void Z_27_05::Camera::moveZ(float z) {
    trZ += z;
    if (trZ < 0.0f) {
        trZ = 0.0f;
    } else if (trZ > 64.0f) {
        trZ = 64.0f;
    }
}

void Z_27_05::Camera::moveXZ(float x, float z) {
    moveX(x);
    moveZ(z);
}

void Z_27_05::Camera::zoom(float meters) {
    zm *= meters;
    if (zm < 2.0f) {
        zm = 2.0f;
    } else if (zm > 64.0f) {
        zm = 64.0f;
    }
}

const Scene::Configuration& Z_27_05::getConfiguration() const {
    return TheConfiguration;
}

void Z_27_05::load(Application& app) {
    camera.init();

    objects.init();
    renderer = app.state().rendererManager.create<Objects>();
    renderer->init(&objects);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    glDepthFunc(GL_LESS);
    glDepthMask(GL_TRUE);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    
    glLineWidth(1.0f);
    glEnable(GL_LINE_SMOOTH);
    glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
    
    glPolygonOffset(1.0f, 1.0f);
    glEnable(GL_POLYGON_OFFSET_FILL);
}

void Z_27_05::unload(Application& app) {
}

void Z_27_05::activate(Application&) {
}

void Z_27_05::suspend(Application&) {
}

void Z_27_05::resized(Application&) {
}

void Z_27_05::keyPressed(Application& app, int which) {
    const Application::State& state = app.state();

    switch (which) {
    case VK_ESCAPE:
        app.quit();
        break;
    case VK_LEFT:
        if (state.keys[VK_SHIFT]) {
            camera.moveX(-0.1f);
        } else {
            camera.moveX(-1);
        }
        break;
    case VK_RIGHT:
        if (state.keys[VK_SHIFT]) {
            camera.moveX(0.1f);
        } else {
            camera.moveX(1);
        }
        break;
    case VK_DOWN:
        if (state.keys[VK_SHIFT]) {
            camera.moveZ(-0.1f);
        } else {
            camera.moveZ(-1);
        }
        break;
    case VK_UP:
        if (state.keys[VK_SHIFT]) {
            camera.moveZ(0.1f);
        } else {
            camera.moveZ(1);
        }
        break;
    case 'Q':
        camera.rotate(-1);
        break;
    case 'E':
        camera.rotate(1);
        break;
    case VK_PRIOR:
        camera.zoom(2.0f);
        break;
    case VK_NEXT:
        camera.zoom(0.5f);
        break;
    }
}

void Z_27_05::keyReleased(Application&, int) {
}

void Z_27_05::update(Application&, long) {
}

void Z_27_05::repaint(Application&) {
    camera.recomputeMatrices();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    DefaultContext ctx = {&camera};
    renderer->draw(ctx);
}
