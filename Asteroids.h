/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#ifndef Asteroids_h
#define Asteroids_h

#include "GL.h"
#include "Renderer.h"
#include "Scene.h"

#include <vector>

class Asteroids : public Scene {
    static const Configuration AsteroidsConfiguration;

public:
    static bool outOfWindow(const Matrix<float, 1, 2>& position);

    class Object {
    public:
        void tickNoWrap(long diff);
        void tick(long diff);
        bool collision(const Object& other) const;
        bool outOfWindow() const;

        Matrix<float, 2, 2> rotMatrix;
        Matrix<float, 1, 2> position, speedVector;
        float directionalSpeed, rotationalSpeed;
        float collisionSize, visibleSize;
    };

    class Missiles {
    public:
        struct Missile {
            Missile() {}
            Missile(const Matrix<float, 1, 2>& position,
                    const Matrix<float, 1, 2>& speed) :
                position(position), speed(speed) {}
            Matrix<float, 1, 2> position, speed;

            void tick(long diff);
            bool collision(const Object& obj) const;
        };

        void init(const Matrix<float, 1, 3>& color);
        void clear();
        void tick(long diff);
        void fire(const Matrix<float, 1, 2>& position,
                  const Matrix<float, 1, 2>& speed);

        template <typename Func>
        void forEach(Func& func) {
            int j = 0;
            for (size_t i = 0, S = missiles.size(); i < S; ++i) {
                Missile& missile = missiles[i];
                if (func(missile)) {
                    continue;
                }
                missiles[j++] = missile;
            }
            missiles.resize(j);
        }

        std::vector<Missile> missiles;
        Matrix<float, 1, 3> color;
    };

    class Ship : public Object {
    public:
        void init(Missiles& missiles);
        void rotate(float angle);
        void tick(long diff);
        void startShooting();
        void stopShooting();
        void kill();
        
        bool alive() const { return life == 1.0f; }
        
        Missiles* missiles;
        float life;
        ULONGLONG shotTick;
        long newEvent;

        static const float maxSpeed, minSpeed, rotSpeed, shotSpeed;
        static const float lifeLoseRate;
        static const long shotDelay;

    private:
        void tryShooting();
    };

    class Rock : public Object {
    public:
        void init();
        void tick(long diff);
        void kill();
        
        bool alive() const { return life == 1.0f; }

        float life;
        long newEvent, protection;
        
        static const float lifeLoseRate;
        static const long protectionDelay;
    };

    Asteroids() : Scene("Asteroids") {}

    virtual const Configuration& getConfiguration() const;
    virtual void load(Application&);
    virtual void unload(Application&);
    virtual void activate(Application&);
    virtual void suspend(Application&);
    virtual void resized(Application&);
    virtual void keyPressed(Application&, int which);
    virtual void keyReleased(Application&, int which);
    virtual void update(Application&, long diff);
    virtual void repaint(Application&);

private:
    Camera camera;
    Ship ship;
    Rock rock;
    Missiles missiles;
    Renderer<Ship>* shipRenderer;
    Renderer<Rock>* rockRenderer;
    Renderer<Missiles>* missilesRenderer;
};

#endif
