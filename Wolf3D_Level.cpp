/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#include "Wolf3D_Level.h"

#include "Log.h"

#include <algorithm>

const float Wolf3D::Door::OpenRate = 0.001f;

bool Wolf3D::Door::open(long diff) {
    if (m_openess == 1.0f) {
        return true;
    }

    m_openess += (diff * OpenRate);
    if (m_openess >= 1.0f) {
        m_openess = 1.0f;
        return true;
    }

    return false;
}

bool Wolf3D::Door::close(long diff) {
    if (m_openess == 0.0f) {
        return true;
    }

    m_openess -= (diff * OpenRate);
    if (m_openess < 0.0f) {
        m_openess = 0.0f;
        return true;
    }

    return false;
}

const float Wolf3D::SecretDoor::OpenRate = 0.0006f;

void Wolf3D::SecretDoor::setDirection(Orientation d) {
    if (isMoving()) {
        return;
    }
    m_velocity = d;
}

void Wolf3D::SecretDoor::stop() {
    m_velocity = FaceNone;
    m_openess = 0.0f;
}

bool Wolf3D::SecretDoor::move(long diff) {
    if (!isMoving()) {
        return true;
    }

    m_openess += (diff * OpenRate);
    if (m_openess >= 1.0f) {
        m_openess -= 1.0f;
        m_position = m_position.moved(m_velocity);
        return true;
    }

    return false;
}


void Wolf3D::Level::rotate(float r) {
    m_playerRotation += r;
    if (m_playerRotation > 180.0f) {
        m_playerRotation -= 360.0f;
    } else if (m_playerRotation < -180.0f) {
        m_playerRotation += 360.0f;
    }
}

void Wolf3D::Level::move(float d) {
    m_playerPositionExact += rotationMatrix(m_playerRotation * (M_PI/180)) *
                             CMatrix<float, 1, 2>::create(0.0f, -d);
}

void Wolf3D::Level::init(const Map& map) {
    this->map = &map;
    clear();

    Map::Plane walls = map[0];
    Map::Plane objects = map[1];

    // Start looking for spawn point.
    m_playerOrientation = FaceNone;
    for (unsigned y = 0; y < map.height(); ++y) {
        for (unsigned x = 0; x < map.width(); ++x) {
            Map::Object o(objects(x, y));
            if (o.isSpawnPoint()) {
                m_playerPosition = Position(x, y);
                m_playerOrientation = o.spawnOrientation();
                break;
            }
        }
    }
    assert(m_playerOrientation != FaceNone);

    m_playerPositionExact[0] = m_playerPosition.x() + 0.5f;
    m_playerPositionExact[1] = m_playerPosition.y() + 0.5f;

    switch (m_playerOrientation) {
    case FaceNorth:
        m_playerRotation = 0;
        break;
    case FaceSouth:
        m_playerRotation = 180;
        break;
    case FaceWest:
        m_playerRotation = -90;
        break;
    case FaceEast:
        m_playerRotation = 90;
        break;
    }
    
    m_playerRoom = 0;
    loadRoom(m_playerPosition);

    while (!doorStack.empty()) {
        unsigned d = doorStack.back();
        Door& door = m_doors[d];

        Position wrongPos = Position(door.rooms()[1] & 0xFF, door.rooms()[1] >> 8);
        Position pos;
        switch (door.orientation()) {
        case FaceNorth:
            pos = door.position().moved(FaceNorth);
            if (pos == wrongPos) {
                pos = door.position().moved(FaceSouth);
            }
            break;
        case FaceWest:
            pos = door.position().moved(FaceWest);
            if (pos == wrongPos) {
                pos = door.position().moved(FaceEast);
            }
            break;
        }

        loadRoom(pos);
    }
}

void Wolf3D::Level::clear() {
    for (unsigned i = 0, S = m_rooms.size(); i < S; ++i) {
        delete m_rooms[i];
    }
    m_rooms.clear();
}

void Wolf3D::Level::loadRoom(const Position& spos) {
    Map::Plane walls = (*map)[0];
    Map::Plane objects = (*map)[1];

    m_rooms.push_back(new Room);
    Room& room = *m_rooms.back();

    assert(dfsStack.empty());
    dfsStack.push_back(spos);

    while (!dfsStack.empty()) {
        StackEntry st = dfsStack.back();
        dfsStack.pop_back();

        if (tilesMet.find(st.pos) != tilesMet.cend()) {
            continue;
        }

        Map::Wall w(walls(st.pos));
        Map::Object o(objects(st.pos));

        global() << "Tile " << (unsigned)st.pos.x() << "x" << (unsigned)st.pos.y() << ": " << w.tile << "/" << o.tile;

        if (o.isSpawnPoint()) {
            assert(st.pos == m_playerPosition);
        }

        if (w.isWall()) {
            TexturePosition<WallCatalog> tpos(w.tpos());
            Orientation o = FaceNone;
            if (st.prev.y() == st.pos.y()-1) {
                o = FaceNorth;
            } else if (st.prev.y() == st.pos.y()+1) {
                o = FaceSouth;
            } else if (st.prev.x() == st.pos.x()-1) {
                o = FaceWest;
            } else if (st.prev.x() == st.pos.x()+1) {
                o = FaceEast;
            }

            global() << "Wall with orientation " << o;
            room.m_walls.push_back(Wall(st.pos, o, tpos));

            continue;
        }

        if (w.isDoor()) {
            auto p = doorsT.insert(std::make_pair(st.pos, m_doors.size()));
            if (p.second) {
                m_doors.push_back(Door(st.pos, w.doorOrientation(), w.doorTpos()));
                m_doors.back().setRoom(0, m_rooms.size()-1);
                m_doors.back().setRoom(1, st.prev.x() | (st.prev.y() << 8));
                doorStack.push_back(m_doors.size()-1);
                room.m_doors.push_back(m_doors.size()-1);
            } else {
                m_doors[p.first->second].setRoom(1, m_rooms.size()-1);
                room.m_doors.push_back(p.first->second);
                doorStack.erase(std::remove(doorStack.begin(), doorStack.end(), p.first->second));
            }

            continue;
        }

        tilesMet.insert(st.pos);

        if (w.isFloor()) {
            dfsStack.push_back(StackEntry(st.pos.moved(FaceNorth), st.pos));
            dfsStack.push_back(StackEntry(st.pos.moved(FaceSouth), st.pos));
            dfsStack.push_back(StackEntry(st.pos.moved(FaceWest), st.pos));
            dfsStack.push_back(StackEntry(st.pos.moved(FaceEast), st.pos));
        }
    }
}
