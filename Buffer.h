/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#ifndef Buffer_h
#define Buffer_h

#include "GL.h"

namespace Buffer {

class Buffer {
public:
    Buffer() : m_buffer(0) {}

    void destroy();

    void bind(GLenum point);
    static void unbind(GLenum point);

private:
    GLuint m_buffer;

    void create();
};

template <GLenum Point>
class Bind {
public:
    explicit Bind(Buffer& b) : buffer(b) { buffer.bind(Point); }
    ~Bind() { buffer.unbind(Point); }

    static void bind(Buffer& b) {
        b.bind(Point);
    }

    static void bind() {
        Buffer::unbind(Point);
    }

    void allocate(size_t size, GLenum usage, const void* data = 0);
    void update(size_t offset, size_t size, const void* data);
    char* map(GLenum access);
    static bool unmap();

protected:
    Buffer& buffer;
};

#ifndef BUFFER_EXPLICIT
#pragma warning(push)
#pragma warning(disable: 4231)
extern template class Bind<GL_ARRAY_BUFFER>;
extern template class Bind<GL_ELEMENT_ARRAY_BUFFER>;
#pragma warning(pop)
#endif

template <GLenum Point>
class Map {
public:
    explicit Map(Bind<Point>& b, GLenum access) : bind(b) { data = bind.map(access); }
    ~Map() { bind.unmap(); }

    template <typename T>
    operator T* () { return (T*)data; }

private:
    Bind<Point>& bind;
    char* data;
};

class Array : public Bind<GL_ARRAY_BUFFER> {
public:
    Array(Buffer& b) : Bind(b) {}
};

class ElementArray : public Bind<GL_ELEMENT_ARRAY_BUFFER> {
public:
    ElementArray(Buffer& b) : Bind(b) {}
};

} // end of Buffer

#endif
