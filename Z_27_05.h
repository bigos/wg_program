/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#ifndef Z_27_05_h
#define Z_27_05_h

#include "Application.h"
#include "Camera.h"
#include "GL.h"
#include "Matrix.h"
#include "Scene.h"
#include "Texture.h"

class Z_27_05 : public Scene {
    static const Configuration TheConfiguration;

public:
    class Camera : public ::Camera {
    public:
        void init();
        
        const Matrix<float, 4, 4>& getProjectionMatrix() const { return projectionMatrix; }
        const Matrix<float, 4, 4>& getViewMatrix() const { return viewMatrix; }

        void recomputeMatrices();
        
        void rotate(int rights);
        void zoom(float meters);
        void moveX(float x);
        void moveZ(float z);
        void moveXZ(float x, float z);

    private:
        int rot;
        float scale, trX, trZ, zm;
    };

public:
    class Floor {
    public:
        void init(int width, int height) { w = width; h = height; }

        int w, h;
    };

    class Cuboid {
    public:
        Cuboid() {}
        Cuboid(int x, int z, int h) { init(x, z, h); }
        void init(int x, int z, int height) { this->x = x; this->z = z; h = height; }

        int x, z, h;
    };

    struct Objects {
        Floor floor;
        std::vector<Cuboid> cuboids;
        
        void init();
    };

    Z_27_05() : Scene("Z 27.05.2013") {}
    
    virtual const Configuration& getConfiguration() const;
    virtual void load(Application&);
    virtual void unload(Application&);
    virtual void activate(Application&);
    virtual void suspend(Application&);
    virtual void resized(Application&);
    virtual void keyPressed(Application&, int which);
    virtual void keyReleased(Application&, int which);
    virtual void update(Application&, long diff);
    virtual void repaint(Application&);

private:
    Camera camera;
    Objects objects;
    Renderer<Objects>* renderer;
};

#endif
