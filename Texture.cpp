/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#define TEXTURE_EXPLICIT
#include "Texture.h"

#include "Application.h"
#include "FileSystem.h"

void Texture::Texture::destroy() {
    glDeleteTextures(1, &m_texture);
}

void Texture::Texture::bind(GLenum point) {
    create();
    glBindTexture(point, m_texture);
}

void Texture::Texture::unbind(GLenum point) {
    glBindTexture(point, 0);
}

void Texture::Texture::create() {
    if (!m_texture) {
        glGenTextures(1, &m_texture);
    }
}

void Texture::Data::free() {
	::free((void*)m_data);
}

Texture::Data Texture::Data::fromBMP(const std::string& name, unsigned width, unsigned height, uint32_t alphaColor) {
	HBITMAP img = (HBITMAP)LoadImage(nullptr, widen(fs.root() + name).c_str(), IMAGE_BITMAP,
		width, height, LR_LOADFROMFILE);
    if (!img) {
        throw WinApiException("LoadImage");
    }

	void* buffer = malloc(width*height*4);

	BITMAPINFO info = {{
		sizeof(BITMAPINFO),
		width,
		height,
		1,
		32,
		BI_RGB,
		0,
		0,
		0,
		0,
		0
	}};
	if (!GetDIBits(Application::instance().state().dc, img, 0, height, buffer, &info, DIB_RGB_COLORS)) {
        throw WinApiException("GetDIBits");
    }

    for (unsigned y = 0; y < height; ++y) {
        for (unsigned x = 0; x < width; ++x) {
            uint32_t& texel = ((uint32_t*)buffer)[y*width+x];
            if (alphaColor != -1 && texel == alphaColor) {
                texel = 0;
            } else {
                texel |= 0xFF000000;
            }
        }
    }

	return Data(GL_BGRA, GL_UNSIGNED_INT_8_8_8_8_REV, buffer);
}

template <>
void ::Texture::detail::allocate(GLenum point, Size<1> size, GLenum internalFormat, const Data& data) {
    glTexImage1D(point, 0, internalFormat, size[0], 0, data.format(), data.type(), data.data());
}

template <>
void ::Texture::detail::allocate(GLenum point, Size<2> size, GLenum internalFormat, const Data& data) {
    glTexImage2D(point, 0, internalFormat, size[0], size[1], 0, data.format(), data.type(), data.data());
}

template <>
void ::Texture::detail::allocate(GLenum point, Size<3> size, GLenum internalFormat, const Data& data) {
    glTexImage3D(point, 0, internalFormat, size[0], size[1], size[2], 0, data.format(), data.type(), data.data());
}

template <>
void ::Texture::detail::update(GLenum point, GLint level, Size<1> offset, Size<1> size, const Data& data) {
    glTexSubImage1D(point, level, offset[0], size[0], data.format(), data.type(), data.data());
}

template <>
void ::Texture::detail::update(GLenum point, GLint level, Size<2> offset, Size<2> size, const Data& data) {
    glTexSubImage2D(point, level, offset[0], offset[1], size[0], size[1], data.format(), data.type(), data.data());
}

template <>
void ::Texture::detail::update(GLenum point, GLint level, Size<3> offset, Size<3> size, const Data& data) {
    glTexSubImage3D(point, level, offset[0], offset[1], offset[2], size[0], size[1], size[2], data.format(), data.type(), data.data());
}

template <>
void Texture::detail::setParameterS(GLenum point, GLenum name, GLint param) {
	glTexParameteri(point, name, param);
}

template <>
void Texture::detail::setParameterS(GLenum point, GLenum name, GLfloat param) {
	glTexParameterf(point, name, param);
}

template <>
void Texture::detail::setParameterV(GLenum point, GLenum name, const GLint* param) {
	glTexParameteriv(point, name, param);
}

template <>
void Texture::detail::setParameterV(GLenum point, GLenum name, const GLfloat* param) {
	glTexParameterfv(point, name, param);
}

template class Texture::Bind<GL_TEXTURE_1D, 1>;
template class Texture::Bind<GL_TEXTURE_2D, 2>;
template class Texture::Bind<GL_TEXTURE_3D, 3>;
