/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#define BUFFER_EXPLICIT
#include "Buffer.h"

void Buffer::Buffer::destroy() {
    glDeleteBuffers(1, &m_buffer);
}

void Buffer::Buffer::bind(GLenum point) {
    create();
    glBindBuffer(point, m_buffer);
}

void Buffer::Buffer::unbind(GLenum point) {
    glBindBuffer(point, 0);
}

void Buffer::Buffer::create() {
    if (!m_buffer) {
        glGenBuffers(1, &m_buffer);
    }
}

template <GLenum Point>
void ::Buffer::Bind<Point>::allocate(size_t size, GLenum usage, const void* data = 0) {
    glBufferData(Point, size, data, usage);
}

template <GLenum Point>
void ::Buffer::Bind<Point>::update(size_t offset, size_t size, const void* data) {
    glBufferSubData(Point, offset, size, data);
}

template <GLenum Point>
char* ::Buffer::Bind<Point>::map(GLenum access) {
    return (char*)glMapBuffer(Point, access);
}

template <GLenum Point>
bool ::Buffer::Bind<Point>::unmap() {
    return !!glUnmapBuffer(Point);
}

template class Buffer::Bind<GL_ARRAY_BUFFER>;
template class Buffer::Bind<GL_ELEMENT_ARRAY_BUFFER>;
