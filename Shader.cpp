/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#include "Shader.h"

#include "FileSystem.h"
#include "Log.h"

#include <system_error>
#include <assert.h>

Shader::DataBase Shader::db;

static Shader::Type getTypeFromExtension(const std::string& name) {
    Shader::Type type = Shader::TypeHeader;
    size_t pos = name.rfind('.');
    if (pos != std::string::npos) {
        if (!name.compare(pos+1, std::string::npos, "vs")) {
            type = Shader::TypeVertex;
        } else if (!name.compare(pos+1, std::string::npos, "fs")) {
            type = Shader::TypeFragment;
        }
    }
    return type;
}

static bool checkType(Shader::Type t) {
    switch (t) {
    case Shader::TypeVertex:
    case Shader::TypeFragment:
        return true;
    default:
        return false;
    }
}

static bool checkTypes(Shader::Object& o, Shader::Source* s) {
    if (!checkType(s->type)) {
        return false;
    }

    GLint type = 0;
    glGetShaderiv(o.object(), GL_SHADER_TYPE, &type);
    if (type != s->type) {
        return false;
    }
    return true;
}

static void describeObject(std::ostream& os, Shader::Object o, Shader::Source* s, const std::string& i) {
    os << "Shader #" << o.object();
    if (s) {
        os << " from " << s->name;
    }
    os << " with result:\n" << i;
    if (s) {
        os << "\nWhere:\n";
        int n = 0;
        for (std::vector<std::string>::const_iterator it = s->fileNumbers().begin(), E = s->fileNumbers().end(); it != E; ++it) {
            os << " " << *it << ": " << n << '\n';
            ++n;
        }
    }
}

static void describeProgram(std::ostream& os, Shader::Program p, const std::vector<Shader::Object>& o, const std::string& i) {
    os << "Program #" << p.program() << " from sources:\n";
    for (std::vector<Shader::Object>::const_iterator it = o.begin(), E = o.end(); it != E; ++it) {
        Shader::Object o = *it;
        os << " - " << o.source()->name << '\n';
    }
    global() << "With result:";
    global() << i;
}

void Shader::SourceException::describeWhere(std::ostream& os) const {
    if (!file) {
        return;
    }
    os << file->name;
    if (line > 0) {
        os << ":" << line;
    }
    os << ": ";
}

void Shader::NotFoundError::describe(std::ostream& os) const {
    describeWhere(os);
    os << "File not found.";
}

void Shader::IncludeError::describe(std::ostream& os) const {
    describeWhere(os);
    os << "Malformed #include directive.";
}

void Shader::IncludeNotFoundError::describe(std::ostream& os) const {
    describeWhere(os);
    os << "Could not find " << include << ".";
}

void Shader::CompileError::describe(std::ostream& os) const {
    os << "Could not compile ";
    describeObject(os, object, source, infoLog);
}

void Shader::LinkError::describe(std::ostream& os) const {
    os << "Could not link ";
    describeProgram(os, program, objects, infoLog);
}

Shader::Source* Shader::Source::get(const std::string& name) {
    DataBase::SourceMap::const_iterator it = db.sources.find(name);
    if (it != db.sources.end()) {
        return it->second;
    }
    
    Type type = getTypeFromExtension(name);

    Source* source = new Source(name, type);
    db.sources.insert(std::make_pair(name, source));
    return source;
}

Shader::Source* Shader::Source::get(const std::string& name, Type type) {
    DataBase::SourceMap::const_iterator it = db.sources.find(name);
    if (it != db.sources.end()) {
        return it->second;
    }

    Source* source = new Source(name, type);
    db.sources.insert(std::make_pair(name, source));
    return source;
}

void Shader::Source::load() {
    if (state >= Loaded) {
        return;
    }

    try {
        m_source = readFile(fs.shader(name));
    } catch (std::ios::failure e) {
        /*Message("failure") << e.code() << "!=" << std::errc::no_such_file_or_directory;
        if (e.code() == std::errc::no_such_file_or_directory) {
            throw NotFoundError(this);
        }
        throw;*/
        //TODO check, in some way, whether the file is really not there
        throw NotFoundError(this);
    }

    state = Loaded;
}

void Shader::Source::setDefines(const Defines& defines) {
    this->defines = &defines;
    if (state == Preprocessed) {
        state = Loaded;
        preprocess();
    }
}

void Shader::Source::preprocess() {
    if (state >= Preprocessed) {
        return;
    }
    load();

    Preprocessor preprocessor(*this);
    preprocessor.include(this, false);
    preprocessor.finish();

    state = Preprocessed;
}

Shader::Source::Preprocessor::Preprocessor(Source& source) :
  source(source) {
    directory = source.name;
    size_t pos = directory.rfind('/');
    if (pos == std::string::npos) {
        directory = "";
    } else {
        directory.erase(pos+1);
    }
}

void Shader::Source::Preprocessor::include(Source* subsource, bool internal) {
    if (included.find(subsource->name) != included.end()) {
        return;
    }
    included.insert(subsource->name);
    size_t number = source.m_fileNumbers.size();
    source.m_fileNumbers.push_back(subsource->name);
    
    if (internal) {
        output << "#line 0 " << number << '\n';
    }
    const std::string& input = subsource->source();
    const size_t size = input.size();
    size_t line = 0, pos = 0, npos, p, p2;
    bool inComment = false, hadVersion = false;
    for (; pos < size; pos = npos) {
        ++line;
        npos = input.find('\n', pos);
        if (npos != std::string::npos) {
            ++npos;
        } else {
            npos = size;
        }

        if (hadVersion) {
            const char* type = 0;
            if (source.type == TypeVertex) {
                type = "VERTEX";
            } else if (source.type == TypeFragment) {
                type = "FRAGMENT";
            }
            if (type) {
                output << "#define " << type << "_SHADER\n";
            }
            if (source.defines) {
                for (Defines::const_iterator it = source.defines->begin(), E = source.defines->end(); it != E; ++it) {
                    output << "#define " << it->first << " " << it->second << '\n';
                }
            }
            output << "#line " << (line-1) << " " << number << '\n';
            hadVersion = false;
        }

        if (!inComment) {
            p = input.find_first_not_of(" \t", pos);
            if (input[p] == '#') {
                p2 = input.find_first_not_of(" \t", p+1);
                if (p2 + 7 < npos && !input.compare(p2, 7, "include")) {
                    p = input.find_first_not_of(" \t", p2+7);
                    bool local;
                    if (input[p] == '"') {
                        local = true;
                    } else if (input[p] == '<') {
                        local = false;
                    } else {
                        throw IncludeError(subsource, line);
                    }
                    p2 = input.find(local ? '"' : '>', p+1);
                    if (p2 >= npos) {
                        throw IncludeError(subsource, line);
                    }
                    std::string name = input.substr(p+1, p2-p-1);

                    Source* incsource = 0;
                    if (local) {
                        try {
                            incsource = Source::get(directory + name);
                            incsource->load();
                        } catch (NotFoundError e) {
                            incsource = 0;
                        }
                    }
                    if (!incsource) {
                        try {
                            incsource = Source::get(name);
                            incsource->load();
                        } catch (NotFoundError e) {
                            incsource = 0;
                        }
                    }

                    if (!incsource) {
                        if (local) {
                            name = "\"" + name + "\"";
                        } else {
                            name = "<" + name + ">";
                        }
                        throw IncludeNotFoundError(name, subsource, line);
                    }
                    include(incsource, true);
                    output << "#line " << (line+1) << " " << number << '\n';
                    continue;
                } else if (p2 + 7 < npos && !input.compare(p2, 7, "version")) {
                    hadVersion = true;
                }
            }
        }

        output.write(&input[pos], npos-pos);
        while (pos < npos) {
            const void* b = memchr(&input[pos], '/', npos-pos-1);
            if (!b) {
                break;
            }
            p = (const char*)b - &input[0];

            if (inComment && p > pos && input[p-1] == '*') {
                inComment = false;
                pos = p + 1;
            } else if (!inComment && p < npos-1) {
                if (input[p+1] == '/') {
                    break;
                }
                if (input[p+1] == '*') {
                    inComment = true;
                    pos = p + 3;
                } else {
                    pos = p + 1;
                }
            }
        }
    }
}

void Shader::Source::Preprocessor::finish() {
    source.m_preprocessed = output.str();
}

Shader::Object::Object(Source* s) {
    setSource(s);
}

void Shader::Object::destroy() {
    assert(m_object);
    glDeleteShader(m_object);
    db.objects.erase(m_object);
}

void Shader::Object::setSource(Source* s) {
    if (!m_object) {
        create(s->type);
    } else {
        assert(checkTypes(*this, s));
    }

    s->preprocess();
    const GLchar* tab[] = {s->preprocessed().c_str()};
    const GLint ltab[] = {(int)s->preprocessed().size()};
    glShaderSource(m_object, 1, tab, ltab);

    db.objects[m_object] = DataBase::ObjectRec(s);
}

Shader::Source* Shader::Object::source() {
    assert(m_object);
    return db.objects[m_object].source;
}

bool Shader::Object::compiled() {
    assert(m_object);
    return db.objects[m_object].compiled;
}

void Shader::Object::compile() {
    assert(m_object);

    if (compiled()) {
        return;
    }

    DataBase::ObjectRec& rec = db.objects[m_object];
    rec.compiled = false;
    glCompileShader(m_object);

#ifndef NDEBUG
    describeObject(global(), *this, source(), infoLog());
#endif

    GLint result = 0;
    glGetShaderiv(m_object, GL_COMPILE_STATUS, &result);
    if (!result) {
        throw CompileError(*this);
    }
    rec.compiled = true;
}

std::string Shader::Object::infoLog() {
    std::string str;
    GLint length = 0;
    glGetShaderiv(m_object, GL_INFO_LOG_LENGTH, &length);
    if (!length) {
        return str;
    }
    
    str.resize(length);
    glGetShaderInfoLog(m_object, length, 0, &str[0]);
    str.resize(length-1);

    return str;
}

void Shader::Object::add() {
    db.objects[m_object];
}

void Shader::Object::create(Type t) {
    assert(checkType(t));
    m_object = glCreateShader(t);

    add();
}

void Shader::Uniform::set(float f) const {
    glUniform1f(m_location, f);
}

void Shader::Uniform::set(int i) const {
    glUniform1i(m_location, i);
}

template <>
void Shader::Uniform::setA<1>(const float* v, size_t count) const {
    glUniform1fv(m_location, (int)count, v);
}
template <>
void Shader::Uniform::setA<2>(const float* v, size_t count) const {
    glUniform2fv(m_location, (int)count, v);
}
template <>
void Shader::Uniform::setA<3>(const float* v, size_t count) const {
    glUniform3fv(m_location, (int)count, v);
}
template <>
void Shader::Uniform::setA<4>(const float* v, size_t count) const {
    glUniform4fv(m_location, (int)count, v);
}

template <>
void Shader::Uniform::setA<1>(const int* v, size_t count) const {
    glUniform1iv(m_location, (int)count, v);
}
template <>
void Shader::Uniform::setA<2>(const int* v, size_t count) const {
    glUniform2iv(m_location, (int)count, v);
}
template <>
void Shader::Uniform::setA<3>(const int* v, size_t count) const {
    glUniform3iv(m_location, (int)count, v);
}
template <>
void Shader::Uniform::setA<4>(const int* v, size_t count) const {
    glUniform4iv(m_location, (int)count, v);
}

template <>
void Shader::Uniform::setM<2, 2>(const float* v, bool transpose, size_t count) const {
    glUniformMatrix2fv(m_location, (int)count, transpose, v);
}
template <>
void Shader::Uniform::setM<2, 3>(const float* v, bool transpose, size_t count) const {
    glUniformMatrix2x3fv(m_location, (int)count, transpose, v);
}
template <>
void Shader::Uniform::setM<2, 4>(const float* v, bool transpose, size_t count) const {
    glUniformMatrix2x4fv(m_location, (int)count, transpose, v);
}
template <>
void Shader::Uniform::setM<3, 2>(const float* v, bool transpose, size_t count) const {
    glUniformMatrix3x2fv(m_location, (int)count, transpose, v);
}
template <>
void Shader::Uniform::setM<3, 3>(const float* v, bool transpose, size_t count) const {
    glUniformMatrix3fv(m_location, (int)count, transpose, v);
}
template <>
void Shader::Uniform::setM<3, 4>(const float* v, bool transpose, size_t count) const {
    glUniformMatrix3x4fv(m_location, (int)count, transpose, v);
}
template <>
void Shader::Uniform::setM<4, 2>(const float* v, bool transpose, size_t count) const {
    glUniformMatrix4x2fv(m_location, (int)count, transpose, v);
}
template <>
void Shader::Uniform::setM<4, 3>(const float* v, bool transpose, size_t count) const {
    glUniformMatrix4x3fv(m_location, (int)count, transpose, v);
}
template <>
void Shader::Uniform::setM<4, 4>(const float* v, bool transpose, size_t count) const {
    glUniformMatrix4fv(m_location, (int)count, transpose, v);
}

void Shader::Program::destroy() {
    assert(m_program);
    glDeleteShader(m_program);
    db.objects.erase(m_program);
}

std::string Shader::Program::infoLog() {
    std::string str;
    GLint length = 0;
    glGetProgramiv(m_program, GL_INFO_LOG_LENGTH, &length);
    if (!length) {
        return str;
    }
    
    str.resize(length);
    glGetProgramInfoLog(m_program, length, 0, &str[0]);
    str.resize(length-1);

    return str;
}

const std::vector<Shader::Object>& Shader::Program::objects() {
    assert(m_program);
    return db.programs[m_program].objects;
}

bool Shader::Program::linked() {
    assert(m_program);
    return db.programs[m_program].linked;
}

void Shader::Program::addObject(Object o) {
    create();
    o.compile();
    glAttachShader(m_program, o.object());

    DataBase::ProgramRec& rec = db.programs[m_program];
    rec.objects.push_back(o);
    rec.linked = false;
}

void Shader::Program::bindAttribute(GLuint index, const char* attribute) {
    create();
    glBindAttribLocation(m_program, index, attribute);
}

Shader::Uniform Shader::Program::getUniform(const char* name) {
    link();

    GLint location = glGetUniformLocation(m_program, name);

    return Uniform(location);
}

void Shader::Program::link() {
    assert(m_program);

    if (linked()) {
        return;
    }

    DataBase::ProgramRec& rec = db.programs[m_program];
    rec.linked = false;
    glLinkProgram(m_program);

#ifndef NDEBUG
    describeProgram(global(), *this, objects(), infoLog());
#endif

    GLint result = 0;
    glGetProgramiv(m_program, GL_LINK_STATUS, &result);
    if (!result) {
        throw LinkError(*this);
    }
    rec.linked = true;
}

void Shader::Program::use() {
    link();
    glUseProgram(m_program);
}

void Shader::Program::unuse() {
    glUseProgram(0);
}

void Shader::Program::add() {
    db.programs[m_program];
}

void Shader::Program::create() {
    if (!m_program) {
        m_program = glCreateProgram();
        add();
    }
}

void Shader::DataBase::init() {

}

void Shader::DataBase::finit() {
    {
        SourceMap::const_iterator it = sources.begin(), E = sources.end();
        for (; it != E; ++it) {
            delete it->second;
        }
    }
    {
        ObjectMap::const_iterator it = objects.begin(), E = objects.end();
        for (; it != E; ++it) {
            glDeleteShader(it->first);
        }
    }
    {
        ProgramMap::const_iterator it = programs.begin(), E = programs.end();
        for (; it != E; ++it) {
            glDeleteProgram(it->first);
        }
    }
}

