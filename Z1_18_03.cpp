/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#include "Z1_18_03.h"

void Z1_18_03::load(Application&) {
    glClearColor(0.9f, 0.9f, 0.9f, 1.0f);

    cameraX = cameraY = 0.0f;
    cameraScale = 200.0f;
    interpolate = false;
    chooseProgram();

    flatColor.init(false);
    interpolatedColor.init(true);
    circle.init();
    xAxis.init(1.0f, 0.0f);
    yAxis.init(0.0f, 1.0f);
}

void Z1_18_03::unload(Application&) {
}

void Z1_18_03::activate(Application&) {
}

void Z1_18_03::suspend(Application&) {
}

void Z1_18_03::resized(Application&) {
    computeProjectionMatrix();
}

void Z1_18_03::keyPressed(Application&, int which) {
    Application& app = Application::instance();
    const Application::State& state = app.state();
    static const GLenum modes[] = {
        GL_POINTS,
        GL_LINES,
        GL_LINE_STRIP,
        GL_LINE_LOOP,
        GL_TRIANGLES,
        GL_TRIANGLE_STRIP,
        GL_TRIANGLE_FAN,
        GL_QUADS,
        GL_QUAD_STRIP,
        GL_POLYGON
    };
    switch (which) {
    case VK_ESCAPE:
        app.quit();
        break;
    case VK_RETURN:
        if (nextScene) {
            app.setScene(*nextScene);
        }
        break;
    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
        circle.mode = modes[which - '0'];
        break;
    case VK_LEFT:
        if (state.keys[VK_SHIFT]) {
            cameraX -= 0.1f;
            computeProjectionMatrix();
        } else {
            circle.x -= 0.1f;
            circle.computeModelMatrix();
        }
        break;
    case VK_RIGHT:
        if (state.keys[VK_SHIFT]) {
            cameraX += 0.1f;
            computeProjectionMatrix();
        } else {
            circle.x += 0.1f;
            circle.computeModelMatrix();
        }
        break;
    case VK_DOWN:
        if (state.keys[VK_SHIFT]) {
            cameraY -= 0.1f;
            computeProjectionMatrix();
        } else {
            circle.y -= 0.1f;
            circle.computeModelMatrix();
        }
        break;
    case VK_UP:
        if (state.keys[VK_SHIFT]) {
            cameraY += 0.1f;
            computeProjectionMatrix();
        } else {
            circle.y += 0.1f;
            circle.computeModelMatrix();
        }
        break;
    case VK_INSERT:
        circle.rotX += 0.2f;
        circle.computeModelMatrix();
        break;
    case VK_DELETE:
        circle.rotX -= 0.2f;
        circle.computeModelMatrix();
        break;
    case VK_HOME:
        circle.rotY += 0.2f;
        circle.computeModelMatrix();
        break;
    case VK_END:
        circle.rotY -= 0.2f;
        circle.computeModelMatrix();
        break;
    case VK_PRIOR:
        circle.rotZ += 0.2f;
        circle.computeModelMatrix();
        break;
    case VK_NEXT:
        circle.rotZ -= 0.2f;
        circle.computeModelMatrix();
        break;
    case VK_ADD:
        cameraScale *= 1.1f;
        computeProjectionMatrix();
        break;
    case VK_SUBTRACT:
        cameraScale /= 1.1f;
        computeProjectionMatrix();
        break;
    case VK_TAB:
        interpolate = !interpolate;
        chooseProgram();
        break;
    }
}

void Z1_18_03::keyReleased(Application&, int which) {
}

void Z1_18_03::update(Application&, long diff) {
}

void Z1_18_03::repaint(Application&) {
    glClear(GL_COLOR_BUFFER_BIT);

    {
        Shader::Bind shader(program->p);
        
        const Application::State& state = Application::instance().state();
        float w = state.width/cameraScale, h = state.height/cameraScale;
        glBegin(GL_QUADS);
            glVertexAttrib3f(1, 0.0f, 0.0f, 1.0f);
            //orthogonalMatrix(cameraX-w, cameraX+w, cameraY-h, cameraY+h, -10.0f, 10.0f);
            glVertex2f(cameraX-w, cameraY-h);
            glVertex2f(cameraX-w, cameraY+h);
            glVertex2f(cameraX+w, cameraY+h);
            glVertex2f(cameraX+w, cameraY-h);
            //PD 
        glEnd();

        circle.draw();
        xAxis.draw();
        yAxis.draw();
    }
}

void Z1_18_03::computeProjectionMatrix() {
    const Application::State& state = Application::instance().state();
    float w = state.width/cameraScale, h = state.height/cameraScale;
    //projectionMatrix = orthogonalMatrix(cameraX-w, cameraX+w, cameraY-h, cameraY+h, -10.0f, 10.0f);
    projectionMatrix = frustumMatrix(cameraX-w, cameraX+w, cameraY-h, cameraY+h, 1.0f, 10.0f)
        * translationMatrix(0.0f, 0.0f, -5.0f);
}

void Z1_18_03::FlatColorProgram::init(bool interpolate) {
    Shader::Source* vss = Shader::Source::get("flatColor.vs");
    Shader::Source* fss = Shader::Source::get("flatColor.fs");

    if (interpolate) {
        defines["INTERPOLATE"] = "1";
        vss->setDefines(defines);
        fss->setDefines(defines);
    }

    vs.setSource(vss);
    fs.setSource(fss);
    p.addObject(vs);
    p.addObject(fs);
    p.bindAttribute(position, "aPosition");
    p.bindAttribute(color, "aColor");
    p.link();

    transformMatrix = p.getUniform("uTransformMatrix");
}

void Z1_18_03::chooseProgram() {
    if (interpolate) {
        program = &interpolatedColor;
    } else {
        program = &flatColor;
    }
}

void Z1_18_03::CirclePrimitive::init() {
    static const float colors[] = {
        0.0f, 0.0f, 0.0f,
        0.5f, 0.0f, 0.0f,
        0.0f, 0.5f, 0.0f,
        0.0f, 0.0f, 0.5f,
    };
    const int COLOR_NUM = sizeof colors / (3*sizeof(float));

    struct CircleVertex {
        float x, y, z, pad0;
        float r, g, b, pad1;
    };

    CircleVertex data[Size];
    memset(data, 0, sizeof data);

    for (int i = 0; i < Size; ++i) {
        data[i].x = cosf((float(i)/Size)*2*M_PI)*2.0f;
        data[i].y = sinf((float(i)/Size)*2*M_PI)*2.0f;
        data[i].z = 0.0f;//-5.0f;

        int j = (i % COLOR_NUM) * 3;
        data[i].r = colors[j];
        data[i].g = colors[j+1];
        data[i].b = colors[j+2];
    }

    {
        Buffer::Array array(b);
        array.allocate(sizeof data, GL_STATIC_DRAW, data);
    }

    {
        using namespace Vertex;
        Bind array(a);
        Attribute position(FlatColorProgram::position);
        Attribute color(FlatColorProgram::color);

        position.enable();
        position.setPointer(GL_FLOAT, true, 3, sizeof CircleVertex, b, offsetof(CircleVertex, x));

        color.enable();
        color.setPointer(GL_FLOAT, true, 3, sizeof CircleVertex, b, offsetof(CircleVertex, r));
    }

    x = y = 0.0f;
    rotX = rotY = rotZ = 0.0f;
    mode = GL_POINTS;
}

void Z1_18_03::CirclePrimitive::draw() {
    Z1_18_03& scene = (Z1_18_03&)Application::instance().scene();
    Matrix<float, 4, 4> matrix = scene.projectionMatrix * modelMatrix;
    scene.program->transformMatrix = matrix;

    Vertex::Bind vertex(a);
    glPointSize(4.0f);
    glLineWidth(2.0f);
    glDrawArrays(mode, 0, Size);
    if (mode > GL_LINE_STRIP) {
        glDrawArrays(GL_LINE_LOOP, 0, Size);
    }
    if (mode > GL_POINTS) {
        glDrawArrays(GL_POINTS, 0, Size);
    }
}

void Z1_18_03::CirclePrimitive::computeModelMatrix() {
    modelMatrix = translationMatrix(x, y, 0.0f) *
                  rotationMatrix(rotX, 1, 0, 0) *
                  rotationMatrix(rotY, 0, 1, 0) *
                  rotationMatrix(rotZ, 0, 0, 1);
}

void Z1_18_03::Axis::init(float x, float y) {
    struct LineVertex {
        float x, y, z;
        unsigned char r, g, b, pad;
    };

    LineVertex data[Size];
    memset(data, 0, sizeof data);

    for (int i = 0; i < Size; ++i) {
        data[i].z = 0.0f;//-5.0f;
        data[i].r = (unsigned char)(200*x);
        data[i].g = (unsigned char)(200*y);
    }

    data[0].x = -x;
    data[0].y = -y;
    data[1].x = x;
    data[1].y = y;

    data[2].x = x*0.95f + y*0.05f;
    data[2].y = x*0.05f + y*0.95f;
    data[3].x = x;
    data[3].y = y;

    data[4].x = x*0.95f - y*0.05f;
    data[4].y = -x*0.05f + y*0.95f;
    data[5].x = x;
    data[5].y = y;

    {
        Buffer::Array array(b);
        array.allocate(sizeof data, GL_STATIC_DRAW, data);
    }

    {
        using namespace Vertex;
        Bind array(a);
        Attribute position(FlatColorProgram::position);
        Attribute color(FlatColorProgram::color);

        position.enable();
        position.setPointer(GL_FLOAT, true, 3, sizeof LineVertex, b, offsetof(LineVertex, x));
        
        color.enable();
        color.setPointer(GL_UNSIGNED_BYTE, true, 3, sizeof LineVertex, b, offsetof(LineVertex, r));
    }
}

void Z1_18_03::Axis::draw() {
    Z1_18_03& scene = (Z1_18_03&)Application::instance().scene();
    scene.program->transformMatrix = scene.projectionMatrix;

    Vertex::Bind vertex(a);
    glLineWidth(1.0f);
    glDrawArrays(GL_LINES, 0, Size);
}
