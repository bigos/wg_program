/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#ifndef Z2_18_03_h
#define Z2_18_03_h

#include "Application.h"
#include "Buffer.h"
#include "GL.h"
#include "Matrix.h"
#include "Scene.h"
#include "Shader.h"
#include "Vertex.h"

#define MODERN 0

class GameOfLife {
public:
    void init(unsigned w, unsigned h);

    struct Cell {
        Cell(unsigned x, unsigned y) : x(x), y(y) {}
        unsigned x, y;

        unsigned char& liveness(GameOfLife& game) const { return game.getRec(x, y)->liveness; }
        Cell neighbour(GameOfLife& game, int dx, int dy);
    };

    unsigned width() const { return w; }
    unsigned height() const { return h; }

    void tick();

    struct CellRec {
        static const unsigned char lifespan = 5;
        unsigned char liveness;
    };
    CellRec* getRec(unsigned x, unsigned y);

private:
    std::vector<CellRec> cells, next;
    std::vector<unsigned char> alive;
    unsigned w, h;
};

class Z2_18_03 : public Scene {
public:
    Z2_18_03() : Scene("Z2 18.03.2013") {}

    virtual void load(Application&);
    virtual void unload(Application&);
    virtual void activate(Application&);
    virtual void suspend(Application&);
    virtual void resized(Application&);
    virtual void keyPressed(Application&, int which);
    virtual void keyReleased(Application&, int which);
    virtual void update(Application&, long diff);
    virtual void repaint(Application&);

private:
    float width, height;
    float cameraX, cameraY, cameraScale;
    Matrix<float, 4, 4> projectionMatrix;
    void computeWindow();
    void computeProjectionMatrix(int dx, int dy);

    float secondsPerTick;
    float accumulatedSeconds;
    bool stopped;

    GameOfLife game;

    void updateTitle();

    class GameCell2Program {
    public:
        void init();
        
        Shader::Object vs, fs;
        Shader::Program program;
        Shader::Uniform transformMatrix;

        static const GLuint position = 0;
        static const GLuint color = 1;
    } simpleProgram;
    
    struct QuadVertex {
        float x, y;
    };
    Buffer::Buffer quad, cells;
    void prepareCells();

#if MODERN
    struct GameCellProgram {
        void init(bool frame);

        Shader::Defines defines;

        Shader::Object vs, fs;
        Shader::Program program;

        Shader::Uniform transformMatrix;
        Shader::Uniform gameWidth, gameHeight;

        static const GLuint position = 0;
        static const GLuint liveness = 1;
    } cellProgram, frameProgram;

    struct GameDraw {
        void init(GameCellProgram& program, GLenum mode);
        void draw();
        
        GameCellProgram* program;
        GLenum mode;
        Vertex::Array a;
    } drawCells, drawFrames;
#else
    struct CellVertex {
        float x, y;
        unsigned char r, g, b, pad0;
        unsigned pad1;
    };

    unsigned limit;
    Buffer::Buffer indices;

    struct Game2Draw {
        void init();
        void draw();
        
        Vertex::Array a, f;
    } draw2;
#endif

    struct Focus {
        void init(int x, int y);
        void draw();

        int x, y;
        float xView, yView;
        void computeView();

        Matrix<float, 4, 4> matrix;
        Buffer::Buffer red;
        Vertex::Array a;
    } focus;
};

#endif
