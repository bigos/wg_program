#ifdef VERTEX_SHADER
// Attributes
in vec2 aPosition;
in int aLiveness;

#define VARYING out
#else
#define VARYING in
#endif

// Varyings
VARYING float vColor;

// Uniforms
uniform mat4 uTransformMatrix;
uniform int uGameWidth, uGameHeight;
