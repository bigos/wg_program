#ifdef VERTEX_SHADER
#define varying out
// Attributes
layout(location = 0) in vec3 aPosition;
layout(location = 1) in vec4 aColor;
#else
#define varying in
#endif

// Varyings
varying vec4 vColor;

// Uniforms
uniform mat4 uTransformMatrix;
