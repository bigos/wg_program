#version 120

#include "flatColor.h"

void main(void) {
    vColor = aColor;
    gl_Position = uTransformMatrix * vec4(aPosition, 1.0);
}
