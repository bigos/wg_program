#version 420

#include "flatColor.4.3.h"

void main(void) {
    vColor = aColor;
    gl_Position = uTransformMatrix * vec4(aPosition, 1.0);
}
