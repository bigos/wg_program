#ifdef VERTEX_SHADER
// Attributes
attribute vec3 aPosition;
attribute vec4 aColor;
#endif

// Varyings
#ifdef INTERPOLATE
varying vec4 vColor;
#else
flat varying vec4 vColor;
#endif

// Uniforms
uniform mat4 uTransformMatrix;
