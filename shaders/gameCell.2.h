#ifdef VERTEX_SHADER
// Attributes
attribute vec2 aPosition;
attribute vec3 aColor;
#endif

// Varyings
varying vec3 vColor;

// Uniforms
uniform mat4 uTransformMatrix;
