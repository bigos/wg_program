#version 330

#include "gameCell.h"

void main(void) {
    int x = gl_InstanceID % uGameWidth;
    int y = gl_InstanceID / uGameWidth;
    vec2 off = vec2(float(x) - float(uGameWidth-1)/2.0,
                  -(float(y) - float(uGameHeight-1)/2.0));
    vec4 pos = vec4(aPosition + off, 0.0, 1.0);
    
#ifdef FRAME
    vColor = 0.9;
    pos = vec4(pos.xy, -1.0 - float(aLiveness), 1.0);
#else
    vColor = 1.0 - float(aLiveness)*0.2;
#endif
    
    gl_Position = uTransformMatrix * pos;
}
