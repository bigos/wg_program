/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#ifndef Shader_h
#define Shader_h

#include "Exceptions.h"
#include "GL.h"
#include "Matrix.h"

#include <sstream>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

namespace Shader {

enum Type {
    TypeHeader = 0,
    TypeVertex = GL_VERTEX_SHADER,
    TypeFragment = GL_FRAGMENT_SHADER
};

typedef std::unordered_map<std::string, std::string> Defines;

class Source {
    enum State {
        Empty = 0,
        Loaded,
        Preprocessed
    };

public:
    static Source* get(const std::string& name, Type type);
    static Source* get(const std::string& name);

    const std::string name;
    const Type type;
    const std::string& source() const { return m_source; }
    const std::string& preprocessed() const { return m_preprocessed; }
    const std::vector<std::string>& fileNumbers() const { return m_fileNumbers; }
    
    void load();
    void setDefines(const Defines& defines);
    void preprocess();

private:
    Source(const std::string& name, Type type) :
        name(name), type(type), state(Empty), defines(0) {}

    class Preprocessor {
    public:
        Preprocessor(Source& source);

        void include(Source* subsource, bool internal);
        void finish();

    private:
        Source& source;
        std::string directory;

        std::ostringstream output;
        std::unordered_set<std::string> included;
    };

    std::string m_source, m_preprocessed;
    std::vector<std::string> m_fileNumbers;
    const Defines* defines;
    State state;
};

class Object {
public:
    Object() : m_object(0) {}
    explicit Object(GLuint o) : m_object(o) {}
    explicit Object(Type t) { create(t); }
    explicit Object(Source* source);

    void destroy();
    
    GLuint object() const { return m_object; }
    std::string infoLog();
    Source* source();
    bool compiled();
    
    void setSource(Source* source);
    void compile();

private:
    GLuint m_object;

    void add();
    void create(Type t);
};

class Uniform {
public:
    Uniform() : m_location(0) {}
    explicit Uniform(GLint l) : m_location(l) {}

    void set(float f) const;
    void set(int i) const;

    template <size_t N>
    void setA(const float* v, size_t count = 1) const;
    template <size_t N>
    void setA(const int* v, size_t count = 1) const;
    
    template <size_t W, size_t H>
    void setM(const float* v, bool transpose = false, size_t count = 1) const;
    template <size_t W, size_t H>
    void setM(const int* v, bool transpose = false, size_t count = 1) const;

    template <typename T, size_t N>
    void set(const Matrix<T, 1, N>& m) const { setA<N> (&m[0]); }
    template <typename T, size_t N>
    void set(const Transpose<T, 1, N>& m) const { setA<N> (&m[0]); }
    template <typename T, size_t N>
    void set(const Matrix<T, N, 1>& m) const { setA<N> (&m[0]); }
    template <typename T, size_t N>
    void set(const Transpose<T, N, 1>& m) const { setA<N> (&m[0]); }
    
    template <typename T, size_t W, size_t H>
    void set(const Matrix<T, W, H>& m) const { setM<W, H> (&m(0, 0)); }
    template <typename T, size_t W, size_t H>
    void set(const Transpose<T, H, W>& m) const { setM<W, H> (&m(0, 0), true); }

    template <typename T>
    void operator= (const T& v) const { set(v); }

private:
    GLint m_location;
};

class Program {
public:
    Program() : m_program(0) {}
    explicit Program(GLuint p) : m_program(p) {}

    void destroy();

    GLuint program() const { return m_program; }
    std::string infoLog();
    const std::vector<Object>& objects();
    bool linked();

    void addObject(Object o);
    void bindAttribute(GLuint index, const char* attribute);
    Uniform getUniform(const char* name);
    void link();
    void use();
    static void unuse();

private:
    GLuint m_program;

    void add();
    void create();
};

class Bind {
public:
    explicit Bind(Program& p) : program(p) { program.use(); }
    ~Bind() { program.unuse(); }

private:
    Program& program;
};

class DataBase {
    friend class Source;
    friend class Object;
    friend class Program;

public:
    void init();
    void finit();

private:
    struct ObjectRec {
        ObjectRec() : source(0), compiled(false) {}
        ObjectRec(Source* s) : source(s), compiled(false) {}

        Source* source;
        bool compiled;
    };

    struct ProgramRec {
        ProgramRec() : linked(false) {}

        std::vector<Object> objects;
        bool linked;
    };

    typedef std::unordered_map<std::string, Source*> SourceMap;
    SourceMap sources;
    
    typedef std::unordered_map<GLuint, ObjectRec> ObjectMap;
    ObjectMap objects;

    typedef std::unordered_map<GLuint, ProgramRec> ProgramMap;
    ProgramMap programs;
};

class SourceException : public Exception {
public:
    Source* const file;
    const ptrdiff_t line;

protected:
    SourceException(const char* title, Source* file, ptrdiff_t line = -1) :
        ::Exception(title), file(file), line(line) {}

    void describeWhere(std::ostream& os) const throw();
};

class NotFoundError : public SourceException {
public:
    NotFoundError(Source* file) :
        SourceException("Shader Not Found Error", file) {}

    virtual void describe(std::ostream& os) const throw();

protected:
    NotFoundError(const char* title, Source* file) :
        SourceException(title, file) {}
};

class IncludeError : public SourceException {
public:
    IncludeError(Source* file, ptrdiff_t line) :
        SourceException("Shader Include Error", file, line) {}

    virtual void describe(std::ostream& os) const throw();

protected:
    IncludeError(const char* title, Source* file, ptrdiff_t line) :
        SourceException(title, file, line) {}
};

class IncludeNotFoundError : public IncludeError {
public:
    IncludeNotFoundError(const std::string& include, Source* file, ptrdiff_t line) :
        IncludeError("Shader Include Not Found Error", file, line),
        include(include) {}

    const std::string include;

    virtual void describe(std::ostream& os) const throw();

protected:
    IncludeNotFoundError(const char* title, const std::string& include, Source* file, ptrdiff_t line) :
        IncludeError(title, file, line),
        include(include) {}
};

class CompileError : public Exception {
public:
    CompileError(Object object) :
        Exception("Shader Compile Error"),
        object(object),
        source(object.source()),
        infoLog(object.infoLog()) {}

    const Object object;
    Source* const source;
    const std::string infoLog;

    virtual void describe(std::ostream& os) const throw();

protected:
    CompileError(const char* title,  Object object) :
        Exception(title),
        object(object),
        source(object.source()),
        infoLog(object.infoLog()) {}
};

class LinkError : public ::Exception {
public:
    LinkError(Program program) :
        ::Exception("Shader Link Error"),
        program(program),
        objects(program.objects()),
        infoLog(program.infoLog()) {}

    const Program program;
    const std::vector<Object>& objects;
    const std::string infoLog;

    virtual void describe(std::ostream& os) const throw();

protected:
    LinkError(const char* title, Program program) :
        ::Exception(title),
        program(program),
        objects(program.objects()),
        infoLog(program.infoLog()) {}
};

extern DataBase db;

} // end of Shader

#endif
