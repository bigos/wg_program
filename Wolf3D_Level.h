/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#ifndef Wolf3D_Level_h
#define Wolf3D_Level_h

#include <assert.h>
#include <stdint.h>
#include <vector>
#include <unordered_map>
#include <unordered_set>

#include "Wolf3D.h"
#include "Wolf3D_Resources.h"

namespace Wolf3D {

class Wall {
public:
	typedef TexturePosition<WallCatalog> TPosition;

	Wall() {}
	Wall(const Position& pos, Orientation orient, const TPosition& tex) :
		m_position(pos), m_texture(tex), m_orientation(orient) {}

	const Position& position() const { return m_position; }
	Orientation orientation() const { return m_orientation; }
	const TPosition& texture() const { return m_texture; }

private:
	Position m_position;
	TPosition m_texture;
	Orientation m_orientation;
};

class Sprite {
public:
	typedef TexturePosition<SpriteCatalog> TPosition;

	Sprite() {}
	Sprite(const Position& pos, const TPosition& tex) :
		m_position(pos), m_texture(tex) {}

	const Position& position() const { return m_position; }
	const TPosition& texture() const { return m_texture; }

private:
	Position m_position;
	TPosition m_texture;
};

class DynamicSprite : public Sprite {
public:
	DynamicSprite() {}
	DynamicSprite(const Position& pos, const TPosition& tex) :
		Sprite(pos, tex), m_offset(0.0f) {}

	const Matrix<float, 1, 2>& offset() const { return m_offset; }

	void updateOffset(const Matrix<float, 1, 2>& off) {
		m_offset += off;
	}

private:
	Matrix<float, 1, 2> m_offset;
};

class Door {
public:
	typedef TexturePosition<DoorCatalog> TPosition;
	static const float OpenRate;

	Door() {}
	Door(const Position& pos, Orientation orient, const TPosition& tex) :
		m_position(pos), m_texture(tex), m_orientation(orient),
        m_openess(0.0f) { memset(m_rooms, 0, sizeof m_rooms); }

	const Position& position() const { return m_position; }
	Orientation orientation() const { return m_orientation; }
	const TPosition& texture() const { return m_texture; }
	float openess() const { return m_openess; }
	bool isOpen() const { return m_openess == 1.0f; }

	bool open(long diff);
	bool close(long diff);

    const unsigned* rooms() const { return m_rooms; }

    void setRoom(unsigned i, unsigned room) {
        assert(i < 2);
        m_rooms[i] = room;
    }

private:
	Position m_position;
	TPosition m_texture;
	Orientation m_orientation;
	float m_openess;

    unsigned m_rooms[2];
};

class SecretDoor {
public:
	typedef TexturePosition<WallCatalog> TPosition;
	static const float OpenRate;

	SecretDoor() {}
	SecretDoor(const Position& pos, const TPosition& tex) :
		m_position(pos), m_texture(tex), m_openess(0.0f), m_velocity(FaceNone) {}

	const Position& position() const { return m_position; }
	Orientation velocity() const { return m_velocity; }
	const TPosition& texture() const { return m_texture; }
	float openess() const { return m_openess; }
	bool isMoving() const { return m_velocity != FaceNone; }

    void setDirection(Orientation);
    void stop();
	bool move(long diff);

private:
	Position m_position;
	TPosition m_texture;
	Orientation m_velocity;
	float m_openess;
};

class Level;

class Room {
	friend class Level;

public:
	const std::vector<Wall>& walls() const { return m_walls; }
	const std::vector<Sprite>& sprites() const { return m_sprites; }
	const std::vector<DynamicSprite*>& dynamicSprites() const { return m_dynamicSprites; }
	const std::vector<unsigned>& doors() const { return m_doors; }
	const std::vector<SecretDoor>& secretDoors() const { return m_secretDoors; }

private:
	std::vector<Wall> m_walls;
	std::vector<Sprite> m_sprites;
	std::vector<DynamicSprite*> m_dynamicSprites;
	std::vector<unsigned> m_doors;
	std::vector<SecretDoor> m_secretDoors;
};

class Map;

class Level {
public:
	void init(const Map& map);
    void clear();

    Matrix<float, 1, 2> playerPositionExact() const { return m_playerPositionExact; }
    Orientation playerOrientation() const { return m_playerOrientation; }
    float playerRotation() const { return m_playerRotation; }
    unsigned playerRoom() const { return m_playerRoom; }

    const Room& room(unsigned i) const {
        assert(i < m_rooms.size());
        return *m_rooms[i];
    }

    unsigned rooms() const { return m_rooms.size(); }

    const Door& door(unsigned i) const {
        assert(i < m_doors.size());
        return m_doors[i];
    }

    unsigned doors() const { return m_doors.size(); }

    void rotate(float r);
    void move(float d);

private:
	std::vector<Room*> m_rooms;
	std::vector<Door> m_doors;
    const Map* map;

    Position m_playerPosition;
    Matrix<float, 1, 2> m_playerPositionExact;
    Orientation m_playerOrientation;
    float m_playerRotation;
    unsigned m_playerRoom;

    struct StackEntry {
        StackEntry() {}
        StackEntry(const Position& a) : pos(a), prev(a) {}
        StackEntry(const Position& a, const Position& b) : pos(a), prev(b) {}

        Position pos;
        Position prev;
    };
    std::vector<StackEntry> dfsStack;
    std::vector<unsigned> doorStack;
    std::unordered_set<Position, Position::Hash> tilesMet;
    std::unordered_map<Position, unsigned, Position::Hash> doorsT;

    void loadRoom(const Position& pos);
};

} // end of Wolf3D

#endif