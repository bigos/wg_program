/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#ifndef Reserver_h
#define Reserver_h

#include "Buffer.h"
#include "Vertex.h"

class Reserver {
public:
    explicit Reserver(Buffer::Buffer& b) :
        buffer(b), offset(0), data(nullptr) {}

    Buffer::Buffer& buffer;

    size_t reserve(size_t bytes) {
        size_t r = offset;
        offset += bytes;
        return r;
    }

    template <typename T>
    T* map(size_t off) { return (T*)(data + off); }

    bool mapped() const { return data != 0; }

    template <GLenum Point>
    void allocate(GLenum usage = GL_STATIC_DRAW) {
        Buffer::Bind<Point> b(buffer);
        b.allocate(offset, usage, 0);
    }

    template <GLenum Point>
    void allocate(Buffer::Bind<Point>& b, GLenum usage = GL_STATIC_DRAW) {
        b.allocate(offset, usage, 0);
    }

    template <GLenum Point>
    void map(Buffer::Map<Point>& m) {
        data = m;
    }

private:
    size_t offset;
    char* data;
};

template <typename Renderer, typename Options>
class ReserveRenderer {
    static const bool PrimitiveRestart = Options::PrimitiveRestart;
    typedef typename Options::Object Object;
    typedef typename Options::Vertex VertexStruct;
    static const unsigned NumVertices = Options::NumVertices;
    static const unsigned NumIndices = Options::NumIndices;

public:
    void reserve(Reserver& vertices, Reserver& indices, size_t num) {
        this->vertices = &vertices;
        this->indices = &indices;
        this->num = num;

        chooseIndexSize(num * NumVertices - 1);

        vertexOffset = vertices.reserve(num * NumVertices * sizeof(VertexStruct));
        indexOffset = indices.reserve(num * NumIndices * indexSize);
    }

    void init(const Object* object) {
        size_t (ReserveRenderer::*addIndices)(unsigned char*, unsigned) = 0;
        ((Renderer*)this)->initSpecification();
        {
            Vertex::Bind array(this->array);
            switch (indexSize) {
            case 2:
                addIndices = &ReserveRenderer::addIndicesProxy<unsigned short>;
                array.setPrimitiveRestartIndex(0xFFFFu);
                break;
            case 4:
                addIndices = &ReserveRenderer::addIndicesProxy<unsigned int>;
                array.setPrimitiveRestartIndex(0xFFFFFFFFu);
                break;
            }
        }

        VertexStruct* V = vertices->map<VertexStruct>(vertexOffset);
        unsigned char* I = indices->map<unsigned char>(indexOffset);
        unsigned n = 0;

        for (size_t i = 0; i < num; ++i) {
            ((Renderer*)this)->initObject(object[i], V);

            V += NumVertices;
            I += (this->*addIndices)(I, n);
            n += NumVertices;
        }
    }

protected:
    void chooseIndexSize(unsigned maxIndex) {
        if (PrimitiveRestart) {
            ++maxIndex;
        }

        // GL_UNSIGNED_BYTE is allegedly slow, so don't use it.
        if (maxIndex <= 0xFFFF) {
            indexSize = 2;
        } else {
            indexSize = 4;
        }
    }

    GLenum indexType() {
        switch (indexSize) {
        case 2:
            return GL_UNSIGNED_SHORT;
        case 4:
            return GL_UNSIGNED_INT;
        default:
            assert(!"Impossible");
            return 0;
        }
    }

    Reserver* vertices;
    Reserver* indices;
    Vertex::Array array;
    size_t num, vertexOffset, indexOffset;
    int indexSize;

    template <typename Number>
    size_t addIndicesProxy(unsigned char* _buf, unsigned num) {
        const size_t Size = sizeof(Number);
        Number* buf = (Number*)_buf;
        ((Renderer*)this)->addIndices(buf, num);

        return NumIndices*Size;
    }
};

#endif
