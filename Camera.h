/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#ifndef Camera_h
#define Camera_h

#include "Matrix.h"

class Camera {
public:
    void setViewMatrix(const Matrix<float, 4, 4>& m) {
        viewMatrix = m;
    }
    void setProjectionMatrix(const Matrix<float, 4, 4>& m) {
        projectionMatrix = m;
    }

    void recomputeMatrices();

    const Matrix<float, 4, 4>& matrix() const { return viewProjectionMatrix; }

protected:
    // View matrix transforms world-coordinates into camera-coordinates.
    Matrix<float, 4, 4> viewMatrix;

    // Projection matrix transforms camera-coordinates into homogeneous-coordinates.
    Matrix<float, 4, 4> projectionMatrix;

    Matrix<float, 4, 4> viewProjectionMatrix;
};

class FreeCamera : public Camera {
public:
    FreeCamera() : position(0.0f), rotation(0.0f), zoom(1) {}

    void moveTo(const Matrix<float, 1, 3>& p) {
        position = -p;
    }
    void moveBy(const Matrix<float, 1, 3>& p) {
        position -= p;
    }

    void rotateTo(const Matrix<float, 1, 3>& r) {
        rotation = -r;
    }
    void rotateBy(const Matrix<float, 1, 3>& r) {
        rotation -= r;
    }

    void setZoom(float z) {
        zoom = z;
    }

    void recomputeMatrices();

protected:
    Matrix<float, 1, 3> position;
    Matrix<float, 1, 3> rotation;
    float zoom;
};

#endif
