/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#include "Application.h"
#include "Exceptions.h"
#include "FileSystem.h"
#include "GL.h"
#include "Log.h"
#include "Matrix.h"
#include "Message.h"
#include "Shader.h"

#include "resource.h"

#include <string>
#include <sstream>
#include <assert.h>
#include <math.h>
#include <string.h>

#include <windowsx.h>

const wchar_t Application::className[] = L"WG_Program_Class";
const char Application::windowName[] = "WG Program";

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                   LPSTR lpCmdLine, int nCmdShow) {
    Scene* scene = 0;
    try {
        Application app(hInstance, lpCmdLine);
        app.init();
        scene = app.sceneFactory().create(app.options().scene.value());
        app.setScene(*scene);

        if (!app.execute()) {
            return 5;
        }
    } catch (Exception& e) {
        global() << e;
        Message(e.title) << e;
        return 1;
    } catch (std::exception& e) {
        global() << "Uncaught exception: " << e.what();
        Message("Uncaught exception") << "Exception:\n" << e.what();
        return 2;
    } catch (const char* e) {
        global() << "Uncaught exception: " << e;
        Message("Uncaught exception") << "Exception:\n" << e;
        return 3;
    } catch (...) {
        global() << "Unknown exception.";
        Message("Uncaught exception") << "Unknown exception.";
        return 4;
    }
    delete scene;
    return 0;
}

static LRESULT __stdcall
WndProcedure(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
    if (Application::instance().state().wnd != hWnd) {
        return DefWindowProc(hWnd, uMsg, wParam, lParam);
    }
    try {
        return Application::instance().windowProcedure(uMsg, wParam, lParam);
    } catch (Exception& e) {
        global() << e;
        Message(e.title) << e;
    } catch (std::exception& e) {
        global() << "Uncaught exception: " << e.what();
        Message("Uncaught exception") << "Exception:\n" << e.what();
    } catch (const char* e) {
        global() << "Uncaught exception: " << e;
        Message("Uncaught exception") << "Exception:\n" << e;
    } catch (...) {
        global() << "Unknown exception.";
        Message("Uncaught exception") << "Unknown exception.";
    }
    
    Application::quit(1);
    return 0;
}

Application* Application::m_instance = 0;

void Application::State::clear() {
    wnd = nullptr;
    dc = nullptr;
    glrc = nullptr;
    active = true;
    width = height = 0;
    aspectRatio = 0;
    memset(keys, 0, sizeof keys);
}

void Application::State::reset(const Scene::Configuration* conf) {
    if (wnd) {
        if (dc) {
            if (glrc) {
                unmakeCurrent();
                destroyContext(glrc);
            }
            destroyDC(wnd, dc);
        }

        if (conf &&
            conf->fullscreen == configuration.fullscreen &&
            conf->resizable == configuration.resizable) {

            // Reuse window.
            app.resizeWindow(conf->width, conf->height);
        } else {
            destroyWindow(wnd);
            wnd = nullptr;
        }
    }

    if (conf) {
        configuration = *conf;
        width = conf->width;
        height = conf->height;
        aspectRatio = float(width)/float(height);

        if (!wnd) {
            //TODO implement fullscreen
            assert(!conf->fullscreen);

            style = (WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX);
            if (conf->resizable) {
                style |= WS_THICKFRAME | WS_MAXIMIZEBOX;
            }
            exStyle = WS_EX_OVERLAPPEDWINDOW;
            wnd = app.createWindow(width, height, style, exStyle);
        }
        dc = app.configureDC(wnd);

        app.extensionWrangler.use();
        RendererManager::Version contextVersion = app.rendererVersion;

        glrc = app.createContext(dc, &contextVersion, conf->compatibilityProfile);
        if (contextVersion < app.rendererVersion) {
            //TODO
        }

        app.makeCurrent();
        app.initRenderer(app.rendererVersion);
    } else {
        configuration = Scene::Configuration();
        width = height = 0;
        aspectRatio = 0;
    }

    active = true;
    memset(keys, 0, sizeof keys);
}

#pragma warning(push)
#pragma warning(disable: 4355)

Application::Application(HINSTANCE hInstance, const char* commandLine) :
  cmdLineParser(commandLine),
  m_state(*this),
  classRegistered(false),
  m_scene(nullptr) {
    m_instance = this;

    m_options.disableChecks.shortName('D')
                           .longName("disable-checks")
                           .byDefault(false)
             .registerIn(cmdLineParser);
    m_options.root.longName("root")
             .registerIn(cmdLineParser);
    m_options.renderer.longName("renderer")
                      .byDefault("auto")
             .registerIn(cmdLineParser);
    m_options.debug.shortName('g')
                   .longName("debug")
                   .byDefault(false)
             .registerIn(cmdLineParser);
    m_options.scene.longName("scene")
                   .byDefault("")
             .registerIn(cmdLineParser);

    m_state.instance = hInstance;
    m_state.clear();
}

#pragma warning(pop)

Application::~Application() {
    Shader::db.finit();
    m_state.reset();
    extensionWrangler.finit();
    unregisterClass();
}

void Application::init() {
    try {
        cmdLineParser.parse();
    } catch (Exception&) {
        Message("Command line") << "Command line looks like this:\n" << cmdLineParser.line;
        throw;
    }

    fs.init();
    global.init("Output.log");
    Shader::db.init();
    m_sceneFactory.init();

    rendererVersion = chooseRenderer();
    
    registerClass();
    extensionWrangler.init(*this);
}

void Application::setScene(Scene& scene) {
    if (m_scene) {
        if (state().active) {
            m_scene->suspend(*this);
        }
        m_scene->unload(*this);
    }

    m_scene = &scene;
    const Scene::Configuration* conf = &scene.getConfiguration();

    m_state.reset(conf);

    scene.load(*this);
    resized();
    scene.activate(*this);
    
    ShowWindow(m_state.wnd, SW_SHOW);
    SetForegroundWindow(m_state.wnd);
    SetFocus(m_state.wnd);

    SetTimer(m_state.wnd, 0, 1000/60, NULL);
    lastTick = GetTickCount64();
}

bool Application::execute() {
    MSG msg;
    BOOL ret;
    while ((ret = GetMessage(&msg, NULL, 0, 0))) {
        if (ret == -1) {
            throw WinApiException("GetMessage");
        }

        switch (msg.message) {
        case WM_TIMER:
            nextFrame();
            break;
        default:
            TranslateMessage(&msg);
            DispatchMessage(&msg);
            break;
        }
    }

    return msg.wParam == 0 ? true : false;
}

void Application::quit(int code) {
    PostQuitMessage(code);
}

void Application::changeWindowName(const std::string& name) {
    SetWindowText(m_state.wnd, widen(name).c_str());
}

LRESULT Application::windowProcedure(UINT uMsg, WPARAM wParam, LPARAM lParam) {
    switch (uMsg) {
    case WM_ACTIVATE:
        if (!HIWORD(wParam)) {
            m_state.active = true;
        } else {
            m_state.active = false;
        }
        activated();
        return 0;
    case WM_DESTROY:
        m_state.wnd = nullptr;
        quit();
        return 0;
    case WM_KEYDOWN:
        m_state.keys[wParam & 255] = true;
        keyPressed(int(wParam));
        return 0;
    case WM_KEYUP:
        m_state.keys[wParam & 255] = false;
        keyReleased(int(wParam));
        return 0;
    case WM_LBUTTONDOWN:
        mouseButtonPressed(MB_Left, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
        return 0;
    case WM_LBUTTONUP:
        mouseButtonReleased(MB_Left, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
        return 0;
    case WM_RBUTTONDOWN:
        mouseButtonPressed(MB_Right, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
        return 0;
    case WM_RBUTTONUP:
        mouseButtonReleased(MB_Right, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
        return 0;
    case WM_MBUTTONDOWN:
        mouseButtonPressed(MB_Middle, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
        return 0;
    case WM_MBUTTONUP:
        mouseButtonReleased(MB_Middle, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
        return 0;
    case WM_MOUSEMOVE:
        mouseMoved(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
        return 0;
    case WM_SIZE:
        m_state.width = LOWORD(lParam);
        m_state.height = HIWORD(lParam);
        m_state.aspectRatio = float(m_state.width)/float(m_state.height);
        resized();
        break;
    }
    return DefWindowProc(m_state.wnd, uMsg, wParam, lParam);
}

void Application::registerClass() {
    WNDCLASSEX windowClass;
    memset(&windowClass, 0, sizeof windowClass);

    windowClass.cbSize = sizeof(WNDCLASSEX);
    windowClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    windowClass.lpfnWndProc = &WndProcedure;
    windowClass.hInstance = m_state.instance;
    windowClass.hIcon = windowClass.hIconSm = LoadIcon(m_state.instance, MAKEINTRESOURCE(IDI_ICON1));
    if (windowClass.hIcon == 0) {
        throw WinApiException("LoadIcon");
    }
    windowClass.hbrBackground = (HBRUSH)(COLOR_BACKGROUND+1);
    windowClass.lpszClassName = className;

    if (!RegisterClassEx(&windowClass)) {
        throw WinApiException("RegisterClassEx");
    }
    classRegistered = true;
}

void Application::unregisterClass() {
    if (!classRegistered) {
        return;
    }
    if (!UnregisterClass(className, m_state.instance)) {
        throw WinApiException("UnregisterClass");
    }
}

HWND Application::createWindow(int width, int height, DWORD style, DWORD exStyle) {
    RECT rect;
    HWND wnd;
    rect.left = 0;
    rect.right = width;
    rect.top = 0;
    rect.bottom = height;

    AdjustWindowRectEx(&rect, style, false, exStyle);

    wnd = CreateWindowEx(exStyle, className, widen(windowName).c_str(), style,
                        CW_USEDEFAULT, CW_USEDEFAULT, rect.right-rect.left, rect.bottom-rect.top,
                        NULL, NULL, m_state.instance, NULL);
    if (!wnd) {
        throw WinApiException("CreateWindowEx");
    }

    return wnd;
}

void Application::destroyWindow(HWND wnd) {
    if (!wnd) {
        return;
    }
    if (!DestroyWindow(wnd)) {
        throw WinApiException("DestroyWindow");
    }
}

HDC Application::configureDC(HWND wnd) {
    const static PIXELFORMATDESCRIPTOR pfd = {
        sizeof(PIXELFORMATDESCRIPTOR),
        1,
        PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,
        PFD_TYPE_RGBA,
        32,
        0, 0, 0, 0, 0, 0,                       // Color Bits Ignored
        0,                                      // No Alpha Buffer
        0,
        0,
        0, 0, 0, 0,
        24,
        8,
        0,
        PFD_MAIN_PLANE,
        0,
        0, 0, 0
    };
    const static int attribList[] = {
        WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
        WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
        WGL_DOUBLE_BUFFER_ARB, GL_TRUE,
        WGL_STEREO_ARB, GL_FALSE,
        WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
        WGL_COLOR_BITS_ARB, 32,
        WGL_RED_BITS_ARB, 8,
        WGL_GREEN_BITS_ARB, 8,
        WGL_BLUE_BITS_ARB, 8,
        WGL_ALPHA_BITS_ARB, 8,
        WGL_ACCUM_BITS_ARB, 0,
        WGL_ACCUM_RED_BITS_ARB, 0,
        WGL_ACCUM_GREEN_BITS_ARB, 0,
        WGL_ACCUM_BLUE_BITS_ARB, 0,
        WGL_ACCUM_ALPHA_BITS_ARB, 0,
        WGL_DEPTH_BITS_ARB, 24,
        WGL_STENCIL_BITS_ARB, 8,
        WGL_AUX_BUFFERS_ARB, 0,
        0
    };
    int pixelFormat = -1;
    const PIXELFORMATDESCRIPTOR* ppfd = nullptr;
    
    HDC dc;
    if (!(dc = GetDC(wnd))) {
        throw WinApiException("GetDC");
    }

    if (extensionWrangler && extensionWrangler._wglChoosePixelFormatARB) {
        GLuint numFormats = 0;
        if ((*extensionWrangler._wglChoosePixelFormatARB)
            (dc, attribList, NULL, 1, &pixelFormat, &numFormats) == FALSE) {

            pixelFormat = -1;

            global() << WinApiException("wglChoosePixelFormatARB");
        }
    }
    if (pixelFormat == -1) {
        ppfd = &pfd;
        if (!(pixelFormat = ChoosePixelFormat(dc, ppfd))) {
            throw WinApiException("ChoosePixelFormat");
        }
    }
    
    if (!SetPixelFormat(dc, pixelFormat, ppfd)) {
        throw WinApiException("SetPixelFormat");
    }

    return dc;
}

void Application::destroyDC(HWND wnd, HDC dc) {
    ReleaseDC(wnd, dc);
}

HGLRC Application::createContextAttribs(HDC dc, bool compatibilityProfile, int major, int minor, bool debug) {
    int flags = 0;
    if (debug) {
        flags |= WGL_CONTEXT_DEBUG_BIT_ARB;
    }
    if (major >= 3 && !compatibilityProfile) {
        flags |= WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB;
    }
    int attribList[] = {
        WGL_CONTEXT_MAJOR_VERSION_ARB, major,
        WGL_CONTEXT_MINOR_VERSION_ARB, minor,
        WGL_CONTEXT_FLAGS_ARB, flags,
        WGL_CONTEXT_PROFILE_MASK_ARB, (compatibilityProfile ?
                                       WGL_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB :
                                       WGL_CONTEXT_CORE_PROFILE_BIT_ARB),
        0
    };
    if (major < 3 || (major == 3 && minor < 2)) {
        attribList[6] = 0;
    }

    HGLRC glrc = (*extensionWrangler._wglCreateContextAttribsARB)
        (dc, nullptr, attribList);
    if (!glrc) {
        global() << WinApiException("wglCreateContextAttribsARB");
    }
    return glrc;
}

HGLRC Application::createContext(HDC dc, RendererManager::Version* version, bool compatibilityProfile) {
    HGLRC glrc = nullptr;
    RendererManager::Version defaultVersion = RendererManager::Version_1_1;
    if (!version) {
        version = &defaultVersion;
    }

    if (extensionWrangler && extensionWrangler._wglCreateContextAttribsARB) {
        bool debug = options().debug.value();
        switch (*version) {
        case RendererManager::Version_Auto:
        case RendererManager::Version_4_3:
            glrc = createContextAttribs(dc, compatibilityProfile, 4, 2, debug);
            if (glrc) {
                *version = RendererManager::Version_4_3;
                break;
            }
            if (debug) {
                glrc = createContextAttribs(dc, compatibilityProfile, 4, 2, false);
                if (glrc) {
                    *version = RendererManager::Version_4_3;
                    break;
                }
            }
            // FALLTHROUGH
        case RendererManager::Version_1_1:
            glrc = createContextAttribs(dc, compatibilityProfile, 1, 1, debug);
            if (glrc) {
                *version = RendererManager::Version_1_1;
                break;
            }
            if (debug) {
                glrc = createContextAttribs(dc, compatibilityProfile, 1, 1, false);
                if (glrc) {
                    *version = RendererManager::Version_1_1;
                    break;
                }
            }
            break;
        }
    }
    if (!glrc) {
        if (!(glrc = wglCreateContext(dc))) {
            throw WinApiException("wglCreateContext");
        }
        *version = RendererManager::Version_1_1;
    }

    return glrc;
}

void Application::destroyContext(HGLRC glrc) {
    if (!wglDeleteContext(glrc)) {
        throw WinApiException("wglDeleteContext");
    }
}

void Application::resizeWindow(int w, int h) {
    RECT rect;
    rect.left = 0;
    rect.right = w;
    rect.top = 0;
    rect.bottom = h;

    AdjustWindowRectEx(&rect, state().style, false, state().exStyle);

    if (!SetWindowPos(state().wnd, nullptr, 0, 0,
            rect.right-rect.left, rect.bottom-rect.top, SWP_NOMOVE)) {

        throw WinApiException("SetWindowPos");
    }
}

Application::ExtensionWrangler::ExtensionWrangler() {
    memset(this, 0, sizeof *this);
}

void Application::ExtensionWrangler::finit() {
    if (!fakeContext) {
        return;
    }

    destroyContext(fakeContext);
    destroyDC(fakeWindow, fakeDC);
    destroyWindow(fakeWindow);
}

void Application::ExtensionWrangler::init(Application& app) {
    fakeWindow = app.createWindow(1, 1);
    try {
        fakeDC = app.configureDC(fakeWindow);
        try {
            fakeContext = app.createContext(fakeDC);
        } catch (...) {
            destroyDC(fakeWindow, fakeDC);
            throw;
        }
    } catch (...) {
        destroyWindow(fakeWindow);
        fakeContext = nullptr;
        throw;
    }

    use();
    initExtensions();
    unuse();
}

void Application::ExtensionWrangler::initExtensions() {
    wglExtensions = (const char*)glGetString(GL_EXTENSIONS);
    _wglGetExtensionsStringARB = (PFNWGLGETEXTENSIONSSTRINGARBPROC)
        wglGetProcAddress("wglGetExtensionsStringARB");
    _wglGetExtensionsStringEXT = (PFNWGLGETEXTENSIONSSTRINGEXTPROC)
        wglGetProcAddress("wglGetExtensionsStringEXT");
    if (_wglGetExtensionsStringARB) {
        wglExtensions = (*_wglGetExtensionsStringARB)(fakeDC);
    } else if (_wglGetExtensionsStringEXT) {
        wglExtensions = (*_wglGetExtensionsStringEXT)();
    }

    if (isSupported("WGL_ARB_pixel_format")) {
        _wglGetPixelFormatAttribivARB = (PFNWGLGETPIXELFORMATATTRIBIVARBPROC)
            wglGetProcAddress("wglGetPixelFormatAttribivARB");
        _wglGetPixelFormatAttribfvARB = (PFNWGLGETPIXELFORMATATTRIBFVARBPROC)
            wglGetProcAddress("wglGetPixelFormatAttribfvARB");
        _wglChoosePixelFormatARB = (PFNWGLCHOOSEPIXELFORMATARBPROC)
            wglGetProcAddress("wglChoosePixelFormatARB");
    }

    if (isSupported("WGL_ARB_create_context")) {
        _wglCreateContextAttribsARB = (PFNWGLCREATECONTEXTATTRIBSARBPROC)
            wglGetProcAddress("wglCreateContextAttribsARB");
    }
}

void Application::ExtensionWrangler::use() {
    makeCurrent(fakeDC, fakeContext);
}

void Application::ExtensionWrangler::unuse() {
    unmakeCurrent();
}

bool Application::ExtensionWrangler::isSupported(const char* name) {
    size_t len = strlen(name);
    const char* base = wglExtensions;
    while (true) {
        const char* ptr = strstr(base, name);
        if (!ptr) {
            return false;
        }
        if (ptr != base && ptr[-1] != ' ') {
            base = ptr + len;
            continue;
        }
        if (ptr[len] != '\0' && ptr[len] != ' ') {
            base = ptr + len;
            continue;
        }
        return true;
    }
}

RendererManager::Version Application::chooseRenderer() {
    const std::string& rendererStr = options().renderer.value();
    RendererManager::Version version;
    //TODO make auto work properly.
    if (rendererStr == "auto") {
        version = RendererManager::Version_Auto;
    } else if (rendererStr == "gl-1.1") {
        version = RendererManager::Version_1_1;
    } else if (rendererStr == "gl-4.3") {
        version = RendererManager::Version_4_3;
    } else {
        // TODO
        Message("Wrong renderer") << "'" << rendererStr << "'";
        throw "Wrong renderer.";
    }

    return version;
}

static void APIENTRY debugCallback(GLenum source, GLenum type, GLuint id, GLenum severity,
                                   GLsizei length, const char* message, void*) {
    const char* sourceString = 0;
    const char* typeString = 0;
    const char* severityString = 0;
    switch (source) {
    case GL_DEBUG_SOURCE_API_ARB:
        sourceString = "GL";
        break;
    case GL_DEBUG_SOURCE_SHADER_COMPILER_ARB:
        sourceString = "GLSL compiler";
        break;
    case GL_DEBUG_SOURCE_WINDOW_SYSTEM_ARB:
        sourceString = "Window system";
        break;
    case GL_DEBUG_SOURCE_THIRD_PARTY_ARB:
        sourceString = "3rd party";
        break;
    case GL_DEBUG_SOURCE_APPLICATION_ARB:
        sourceString = "Application";
        break;
    case GL_DEBUG_SOURCE_OTHER_ARB:
    default:
        sourceString = "Other";
        break;
    }
    switch (type) {
    case GL_DEBUG_TYPE_ERROR_ARB:
        typeString = "Error";
        break;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR_ARB:
        typeString = "Deprecated";
        break;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR_ARB:
        typeString = "Undefined behaviour";
        break;
    case GL_DEBUG_TYPE_PERFORMANCE_ARB:
        typeString = "Performance warning";
        break;
    case GL_DEBUG_TYPE_PORTABILITY_ARB:
        typeString = "Portability warning";
        break;
    case GL_DEBUG_TYPE_OTHER_ARB:
    default:
        typeString = "Other";
        break;
    }
    switch (severity) {
    case GL_DEBUG_SEVERITY_HIGH_ARB:
        severityString = "High";
        break;
    case GL_DEBUG_SEVERITY_MEDIUM_ARB:
        severityString = "Medium";
        break;
    case GL_DEBUG_SEVERITY_LOW_ARB:
        severityString = "Low";
        break;
    default:
        severityString = "Other";
        break;
    }
    global() << "Severity " << severityString << ", "
        << sourceString << " " << typeString << ": " << message;
}

void Application::initRenderer(RendererManager::Version version) {
    GLenum err = glewInit();
    if (GLEW_OK != err) {
        throw GlewError(err);
    }

    if (GLEW_ARB_debug_output) {
        glDebugMessageControlARB(GL_DONT_CARE, GL_DONT_CARE, GL_DEBUG_SEVERITY_LOW_ARB, 0, 0, GL_TRUE);
        glDebugMessageCallbackARB(&debugCallback, nullptr);
    }

    if (options().disableChecks.value()) {
        m_state.rendererManager.init(version, true);
    } else {
        m_state.rendererManager.init(version);
    }
}

void Application::activated() {
    if (m_state.active) {
        scene().activate(*this);
    } else {
        scene().suspend(*this);
    }
}

void Application::resized() {
    glViewport(0, 0, m_state.width, m_state.height);

    scene().resized(*this);
}

void Application::keyPressed(int which) {
    scene().keyPressed(*this, which);
}

void Application::keyReleased(int which) {
    scene().keyReleased(*this, which);
}

void Application::mouseButtonPressed(MouseButton which, int x, int y) {
    scene().mouseButtonPressed(*this, which, x, y);
}

void Application::mouseButtonReleased(MouseButton which, int x, int y) {
    scene().mouseButtonReleased(*this, which, x, y);
}

void Application::mouseMoved(int x, int y) {
    scene().mouseMoved(*this, x, y);
}

void Application::nextFrame() {
    ULONGLONG now = GetTickCount64();
    long diff = long(now - lastTick);
    lastTick = now;
    scene().update(*this, diff);
    
    if (!m_state.active) {
        return;
    }
    scene().repaint(*this);
    
    if (GLEW_GREMEDY_frame_terminator) {
        glFrameTerminatorGREMEDY();
    }
    SwapBuffers(m_state.dc);
}

void Application::makeCurrent(HDC dc, HGLRC glrc) {
    if (!wglMakeCurrent(dc, glrc)) {
        throw WinApiException("wglMakeCurrent");
    }
}

void Application::unmakeCurrent() {
    wglMakeCurrent(NULL, NULL);
}

/*
czytanie obrazkow (LoadImage/LoadBitmap)
*/
