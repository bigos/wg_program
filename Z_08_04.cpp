/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#include "Z_08_04.h"
#include "Buffer.h"
#include "FileSystem.h"
#include "Reserver.h"
#include "Shader.h"
#include "Vertex.h"

#include "Log.h"

const Scene::Configuration Z_08_04::TheConfiguration = {
    // 800x600 unresizable window. Compatibility profile.
    800, 600,
    false, false,
    true
};

template <>
class Renderer_1_1<Z_08_04::Side> : public Renderer<Z_08_04::Side> {
public:
    virtual ~Renderer_1_1() {}

    virtual void init(const Z_08_04::Side* object, size_t num) {
        side = object;
        this->num = num;
    }

    virtual void draw(DefaultContext& context) {
        glMatrixMode(GL_PROJECTION);
        glLoadMatrixf(context.camera->matrix().data);

        glLineWidth(1.0f);
        for (size_t i = 0; i < num; ++i) {
            const Z_08_04::Side& side = this->side[i];
            glColor3f(side.color[0], side.color[1], side.color[2]);
            
            Matrix<float, 1, 3> base = side.dimen * 10.0f;
            Matrix<float, 1, 3> rest1 = 1.0f, rest2;
            rest1 -= abs(side.dimen);
            rest1 *= 10.0f;
            rest2 = rest1;
            if (side.dimen[0] != 0) {
                rest1[1] = 0;
                rest2[2] = 0;
            } else {
                rest1[1] = rest1[2] = 0;
                rest2[0] = 0;
            }

            Matrix<float, 1, 3> from, to;
            glBegin(GL_LINES);
                for (size_t i = 0; i < 21; ++i) {
                    from = base + rest1 + rest2 * (0.1f * i - 1.0f);
                    to = base - rest1 + rest2 * (0.1f * i - 1.0f);
                    glVertex3f(from[0], from[1], from[2]);
                    glVertex3f(to[0], to[1], to[2]);
                    
                    from = base + rest2 + rest1 * (0.1f * i - 1.0f);
                    to = base - rest2 + rest1 * (0.1f * i - 1.0f);
                    glVertex3f(from[0], from[1], from[2]);
                    glVertex3f(to[0], to[1], to[2]);
                }
            glEnd();
        }
    }

private:
    const Z_08_04::Side* side;
    size_t num;
};

template <>
class Renderer_1_1<Z_08_04::HUD> : public Renderer<Z_08_04::HUD> {
public:
    virtual ~Renderer_1_1() {}

    virtual void init(const Z_08_04::HUD* object, size_t num) {
        hud = object;
        this->num = num;
    }

    virtual void draw(DefaultContext& context) {
        glLineWidth(1.0f);
        glPointSize(3.0f);
        for (size_t i = 0; i < num; ++i) {
            const Z_08_04::HUD& hud = this->hud[i];

            switch (hud.camera->mode) {
            case Z_08_04::Camera::FPP: {
                static const Matrix<float, 4, 4> projMatrix = orthogonalMatrix(-400, 400, -300, 300, -1, 0);
                static const Matrix<float, 4, 4> rotXMatrix = translationMatrix(-350, 250, 0);
                static const Matrix<float, 4, 4> rotYMatrix = translationMatrix(-250, 250, 0);
                static const Matrix<float, 2, 2> rot = rotationMatrix(2*M_PI/24);
                glMatrixMode(GL_PROJECTION);
                glLoadMatrixf(projMatrix.data);
                glMatrixMode(GL_MODELVIEW);
                glPushMatrix();
                glLoadIdentity();

                glColor3f(0.8f, 0.0f, 0.0f);

                glBegin(GL_LINES);
                    glVertex2f(-20, 0);
                    glVertex2f(20, 0);
                    glVertex2f(0, -20);
                    glVertex2f(0, 20);
                glEnd();

                Matrix<float, 1, 2> pos = CMatrix<float, 1, 2>::create(0, 15);
                glBegin(GL_LINE_LOOP);
                for (int i = 0; i < 24; ++i) {
                    glVertex2f(pos[0], pos[1]);
                    pos = rot * pos;
                }
                glEnd();

                glLoadMatrixf(rotXMatrix.data);
                glColor3f(0.0f, 0.8f, 0.0f);
                pos = CMatrix<float, 1, 2>::create(0, 45);
                glBegin(GL_POINTS);
                for (int i = 0; i < 24; ++i) {
                    glVertex2f(pos[0], pos[1]);
                    pos = rot * pos;
                }
                glEnd();

                glMultMatrixf(rotationMatrix(hud.camera->rotX * (M_PI/180), 0, 0, -1).data);
                glBegin(GL_LINE_LOOP);
                    glVertex2f(-35, 0);
                    glVertex2f(35, 0);
                    glVertex2f(-35, 20);
                glEnd();

                glLoadMatrixf(rotYMatrix.data);
                glColor3f(0.0f, 0.0f, 0.8f);
                pos = CMatrix<float, 1, 2>::create(0, 45);
                glBegin(GL_POINTS);
                for (int i = 0; i < 24; ++i) {
                    glVertex2f(pos[0], pos[1]);
                    pos = rot * pos;
                }
                glEnd();

                glMultMatrixf(rotationMatrix(hud.camera->rotY * (M_PI/180), 0, 0, 1).data);
                glBegin(GL_LINE_LOOP);
                    glVertex2f(-25, 0);
                    glVertex2f(-35, 20);
                    glVertex2f(35, 0);
                    glVertex2f(-35, -20);
                glEnd();

                glPopMatrix();
                } break;
            case Z_08_04::Camera::TPP:
            case Z_08_04::Camera::TPP_Behind: {
                static const Matrix<float, 4, 4> scaleMatrix = scalingMatrix(0.004f, 0.004f, 0.004f);
                Matrix<float, 4, 4> rotMatrix = rotationMatrix(hud.camera->rotY * (M_PI/180), 0, 1, 0) *
                                                rotationMatrix(hud.camera->rotX * (M_PI/180), 0, 0, -1);
                Matrix<float, 4, 4> trMatrix = translationMatrix(hud.camera->position);

                glMatrixMode(GL_PROJECTION);
                glLoadMatrixf(context.camera->matrix().data);
                glMatrixMode(GL_MODELVIEW);
                glPushMatrix();
                glLoadMatrixf((trMatrix * rotMatrix * scaleMatrix).data);

                drawShip();
                
                glPopMatrix();
                } break;
            }
        }
    }

private:
    const Z_08_04::HUD* hud;
    size_t num;

    void drawShip() {
        glPolygonOffset(1.0f, 1.0f);
        glEnable(GL_POLYGON_OFFSET_FILL);

        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glColor3f(0.0f, 0.0f, 0.0f);
        drawShipVertices();
        
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glColor3f(0.8f, 0.8f, 0.8f);
        drawShipVertices();

        glDisable(GL_POLYGON_OFFSET_FILL);
    }

    void drawShipVertices() {
        glBegin(GL_TRIANGLES);
            glVertex3f( 50.0f,   0.0f,   0.0f);
            glVertex3f(-20.0f,  23.0f,   0.0f);
            glVertex3f(-40.0f,  30.0f, -30.0f);
            
            glVertex3f( 50.0f,   0.0f,   0.0f);
            glVertex3f(-20.0f,  23.0f,   0.0f);
            glVertex3f(-40.0f,  30.0f,  30.0f);
            
            glVertex3f( 50.0f,   0.0f,   0.0f);
            glVertex3f(-20.0f,   0.0f,   0.0f);
            glVertex3f(-40.0f,   0.0f, -30.0f);
            
            glVertex3f( 50.0f,   0.0f,   0.0f);
            glVertex3f(-20.0f,   0.0f,   0.0f);
            glVertex3f(-40.0f,   0.0f,  30.0f);

            glVertex3f( 50.0f,   0.0f,   0.0f);
            glVertex3f(-40.0f,   0.0f, -30.0f);
            glVertex3f(-40.0f,  30.0f, -30.0f);
            
            glVertex3f( 50.0f,   0.0f,   0.0f);
            glVertex3f(-40.0f,   0.0f,  30.0f);
            glVertex3f(-40.0f,  30.0f,  30.0f);
        glEnd();
        glBegin(GL_QUADS);
            glVertex3f(-40.0f,   0.0f, -30.0f);
            glVertex3f(-40.0f,  30.0f, -30.0f);
            glVertex3f(-20.0f,  23.0f,   0.0f);
            glVertex3f(-20.0f,   0.0f,   0.0f);
            
            glVertex3f(-40.0f,   0.0f,  30.0f);
            glVertex3f(-40.0f,  30.0f,  30.0f);
            glVertex3f(-20.0f,  23.0f,   0.0f);
            glVertex3f(-20.0f,   0.0f,   0.0f);
        glEnd();
    }
};

void Z_08_04::Camera::init() {
    position = 0.0f;
    rotX = rotY = 0;
    mode = FPP;
}

void Z_08_04::Camera::recomputeMatrices() {
    const float scale = 200.0f;
    Application& app = Application::instance();
    float w = app.state().width/scale;
    float h = app.state().height/scale;

    projectionMatrix = perspectiveMatrix(atan(w/h), w/h, 0.01f, 50.0f);
    
    Matrix<float, 1, 4> cen4;
    Matrix<float, 1, 4> up4;
    switch (mode) {
    case FPP:
        cen4 = rotationMatrix(rotY * (M_PI/180), 0, 1, 0) *
               rotationMatrix(rotX * (M_PI/180), 0, 0, -1) *
               CMatrix<float, 1, 4>::create(1, 0, 0, 1);
        up4 = rotationMatrix(rotY * (M_PI/180), 0, 1, 0) *
              rotationMatrix(rotX * (M_PI/180), 0, 0, -1) *
              CMatrix<float, 1, 4>::create(0, 1, 0, 1);
        viewMatrix = lookAtMatrix(position,
                                  position+Matrix<float, 1, 3> (cen4.data),
                                  Matrix<float, 1, 3> (up4.data));
        break;
    case TPP:
        cen4 = rotationMatrix(rotY * (M_PI/180), 0, 1, 0) *
               rotationMatrix(20 * (M_PI/180), 0, 0, -1) *
               CMatrix<float, 1, 4>::create(1, 0, 0, 1);
        viewMatrix = lookAtMatrix(position-Matrix<float, 1, 3> (cen4.data),
                                  position,
                                  CMatrix<float, 1, 3>::create(0, 1, 0));
        break;
    case TPP_Behind:
        cen4 = rotationMatrix(rotY * (M_PI/180), 0, 1, 0) *
               rotationMatrix((rotX+20) * (M_PI/180), 0, 0, -1) *
               CMatrix<float, 1, 4>::create(1, 0, 0, 1);
        up4 = rotationMatrix(rotY * (M_PI/180), 0, 1, 0) *
              rotationMatrix(rotX * (M_PI/180), 0, 0, -1) *
              CMatrix<float, 1, 4>::create(0, 1, 0, 1);
        viewMatrix = lookAtMatrix(position-Matrix<float, 1, 3> (cen4.data),
                                  position,
                                  Matrix<float, 1, 3> (up4.data));
        break;
    }
    
    ::Camera::recomputeMatrices();
}

void Z_08_04::Camera::rotateX(float degrees) {
    if (rotX + degrees > 90) {
        rotX = 90;
        return;
    }
    if (rotX + degrees < -90) {
        rotX = -90;
        return;
    }
    rotX += degrees;
}

void Z_08_04::Camera::rotateY(float degrees) {
    rotY += degrees;
    if (rotY > 180) {
        rotY -= 360;
        return;
    }
    if (rotY < -180) {
        rotY += 360;
        return;
    }
}

void Z_08_04::Camera::move(float t) {
    const Matrix<float, 1, 4> v = rotationMatrix(rotY * (M_PI/180), 0, 1, 0) *
                                  rotationMatrix(rotX * (M_PI/180), 0, 0, -1) *
                                  CMatrix<float, 1, 4>::create(t, 0, 0, 1);
    const Matrix<float, 1, 3> npos = position + CMatrix<float, 1, 3>::create(v[0], v[1], v[2]);
    if (npos[0] < -9.9f || npos[0] > 9.9f) {
        return;
    }
    if (npos[1] < -9.9f || npos[1] > 9.9f) {
        return;
    }
    if (npos[2] < -9.9f || npos[2] > 9.9f) {
        return;
    }
    position = npos;
}

void Z_08_04::Camera::nextMode() {
    mode = Mode((int(mode) + 1) % int(NumModes));
}

const Scene::Configuration& Z_08_04::getConfiguration() const {
    return TheConfiguration;
}

void Z_08_04::load(Application& app) {
    camera.init();

    hud.init(camera);
    hudRenderer = app.state().rendererManager.create<HUD>();
    hudRenderer->init(&hud);

    sides[0].init(CMatrix<float, 1, 3>::create(1.0f, 0.0f, 0.0f),
                  CMatrix<float, 1, 3>::create(0.5f, 0.0f, 0.0f));
    sides[1].init(CMatrix<float, 1, 3>::create(-1.0f, 0.0f, 0.0f),
                  CMatrix<float, 1, 3>::create(0.5f, 0.5f, 0.0f));
    sides[2].init(CMatrix<float, 1, 3>::create(0.0f, 1.0f, 0.0f),
                  CMatrix<float, 1, 3>::create(0.0f, 0.5f, 0.0f));
    sides[3].init(CMatrix<float, 1, 3>::create(0.0f, -1.0f, 0.0f),
                  CMatrix<float, 1, 3>::create(0.0f, 0.5f, 0.5f));
    sides[4].init(CMatrix<float, 1, 3>::create(0.0f, 0.0f, 1.0f),
                  CMatrix<float, 1, 3>::create(0.0f, 0.0f, 0.5f));
    sides[5].init(CMatrix<float, 1, 3>::create(0.0f, 0.0f, -1.0f),
                  CMatrix<float, 1, 3>::create(0.5f, 0.0f, 0.5f));
    sideRenderer = app.state().rendererManager.create<Side>();
    sideRenderer->init(sides, 6);

    glClearColor(0.9f, 0.9f, 0.9f, 1.0f);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glDepthMask(GL_TRUE);

    image = new unsigned[100*100];
    if (false) {
        memset(image, 0, 100*100*sizeof(unsigned));
        for (int i = 0; i < 20; ++i) {
            for (int j = 0; j < 20; ++j) {
                image[i*100+j] = 0xFF0000FF;
            }
            for (int j = 20; j < 100; ++j) {
                image[i*100+j] = 0x7F0000FF;
            }
        }
        for (int i = 20; i < 40; ++i) {
            for (int j = 0; j < 20; ++j) {
                image[i*100+j] = 0x00FF00FF;
            }
            for (int j = 20; j < 100; ++j) {
                image[i*100+j] = 0x007F00FF;
            }
        }
        for (int i = 40; i < 100; ++i) {
            for (int j = 0; j < 100; ++j) {
                image[i*100+j] = 0x0000FFFF;
            }
        }
    } else {
        HBITMAP img = (HBITMAP)LoadImage(GetModuleHandle(nullptr), widen(fs.root() + "Image.bmp").c_str(), IMAGE_BITMAP, 100, 100, LR_LOADFROMFILE);
        if (!img) {
            throw WinApiException("LoadImage");
        }
        //GetBitmapBits(img, 100*100*sizeof(unsigned), image);
        BITMAPINFO info = {{
            sizeof(BITMAPINFOHEADER),
            100,
            100,
            1,
            32,
            BI_RGB,
            0,
            0,
            0,
            0,
            0
        }};
        if (!GetDIBits(app.state().dc, img, 0, 100, image, &info, DIB_RGB_COLORS)) {
            throw WinApiException("GetDIBits");
        }
    }
}

void Z_08_04::unload(Application& app) {
    delete[] image;
}

void Z_08_04::activate(Application&) {
}

void Z_08_04::suspend(Application&) {
}

void Z_08_04::resized(Application&) {
}

void Z_08_04::keyPressed(Application& app, int which) {
    const Application::State& state = app.state();

    switch (which) {
    case VK_ESCAPE:
        app.quit();
        break;
    case VK_LEFT:
        camera.rotateY(1);
        break;
    case VK_RIGHT:
        camera.rotateY(-1);
        break;
    case VK_DOWN:
        if (state.keys[VK_SHIFT]) {
            camera.rotateX(1);
        } else {
            camera.move(-0.1f);
        }
        break;
    case VK_UP:
        if (state.keys[VK_SHIFT]) {
            camera.rotateX(-1);
        } else {
            camera.move(0.1f);
        }
        break;
    case VK_TAB:
        camera.nextMode();
        break;
    }
}

void Z_08_04::keyReleased(Application&, int) {
}

void Z_08_04::update(Application&, long) {
}

void Z_08_04::repaint(Application&) {
    camera.recomputeMatrices();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    DefaultContext ctx = {&camera};
    hudRenderer->draw(ctx);
    sideRenderer->draw(ctx);

    glRasterPos3f(0, 0, 0);
    glDrawPixels(100, 100, GL_BGRA, GL_UNSIGNED_INT_8_8_8_8_REV, image);
}
