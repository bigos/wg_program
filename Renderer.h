/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#ifndef Renderer_h
#define Renderer_h

#include "Camera.h"

struct DefaultContext {
    Camera* camera;
};

template <class Object>
class Renderer {
public:
    typedef DefaultContext Context;

    virtual ~Renderer() {};

    virtual void init(const Object* object, size_t num = 1) = 0;
    virtual void draw(Context& context) = 0;
};

template <class Object>
class Renderer_1_1 : public Renderer<Object> {
public:
    static const bool Available = false;

    virtual void init(const Object* object, size_t num = 1) {}
    virtual void draw(Context& context) {}
};

template <class Object>
class Renderer_4_3 : public Renderer<Object> {
public:
    static const bool Available = false;

    virtual void init(const Object* object, size_t num = 1) {}
    virtual void draw(Context& context) {}
};

class RendererManager {
    struct GetAvailable {
        typedef char _false[1];
        typedef char _true[2];
        
        template <class Renderer>
        static bool value(decltype(Renderer::Available)*) {
            return Renderer::Available;
        }

        template <class>
        static bool value(...) {
            return true;
        }
    };

public:
    enum Version {
        Version_Auto = -1,
        Version_1_1 = 0, ///< Base version, always supported.
        Version_4_3      ///< Currently it's only 4.2 with a few extensions.
    };
    static const char* getVersionName(Version);
    
    RendererManager() : m_version() {}
    void init(Version required, bool force = false);

    template <class Object>
    Renderer<Object>* create(Version which) const {
        switch (which) {
        case Version_4_3:
            if (GetAvailable::value<Renderer_4_3<Object> >(0)) {
                return new Renderer_4_3<Object>();
            }
            // FALLTHROUGH
        case Version_1_1:
            if (GetAvailable::value<Renderer_1_1<Object> >(0)) {
                return new Renderer_1_1<Object>();
            }
        default:
            return nullptr;
        }
    }

    template <class Object>
    Renderer<Object>* create() const {
        return create<Object>(version());
    }

    Version version() const { return m_version; }

private:
    Version m_version;

    bool check1_1();
    bool check4_3();
};

#endif
