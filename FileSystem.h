/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#ifndef FileSystem_h
#define FileSystem_h

#include "System.h"

#include <string>

class FileSystem {
public:
    void init();

    const std::string& root() const { return m_root; }
    const std::string& shaders() const { return m_shaders; }
    std::string shader(const std::string& name) const { return m_shaders+name; }

private:
    void getRootDirectory();
    void getOtherDirectories();

    std::string m_root;
    std::string m_shaders;
};

std::string readFile(const std::string& name);

extern FileSystem fs;

#endif
