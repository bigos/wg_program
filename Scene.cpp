/**
  * Copyright (C) 2013 Gustaw Smolarczyk <wielkiegie@gmail.com>
  *
  * This file is part of WG_Program.
  *
  * WG_Program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * WG_Program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with WG_Program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

#include "Scene.h"

#include "Asteroids.h"
#include "Wolf3D_Scene.h"
#include "Z1_18_03.h"
#include "Z2_18_03.h"
#include "Z_25_03.h"
#include "Z_08_04.h"
#include "Z_22_04.h"
#include "Z_13_05.h"
#include "Z_27_05.h"

typedef Z_22_04 DefaultScene;

const Scene::Configuration Scene::DefaultConfiguration = {
    // 800x600 resizable window. Compatibility profile.
    800, 600,
    false, true,
    true
};

const Scene::Configuration& Scene::getConfiguration() const {
    return DefaultConfiguration;
}

void Scene::mouseButtonPressed(Application&, int, int, int) {}
void Scene::mouseButtonReleased(Application&, int, int, int) {}
void Scene::mouseMoved(Application&, int, int) {}

void Scene::Factory::init() {
}

Scene* Scene::Factory::create(const std::string& name) const {
    if (name == "") {
        return new DefaultScene;
    }
    if (name == "Asteroids") {
        return new Asteroids;
    }
    if (name == "Wolf3D") {
        return new Wolf3D::Scene;
    }
    if (name == "Z1_18_03") {
        Z1_18_03* scene = new Z1_18_03;
        scene->nextScene = new Z2_18_03;
        return scene;
    }
    if (name == "Z2_18_03") {
        return new Z2_18_03;
    }
    if (name == "Z_25_03") {
        return new Z_25_03;
    }
    if (name == "Z_08_04") {
        return new Z_08_04;
    }
    if (name == "Z_22_04") {
        return new Z_22_04;
    }
    if (name == "Z_13_05") {
        return new Z_13_05;
    }
    if (name == "Z_27_05") {
        return new Z_27_05;
    }
    throw "Wrong scene";
}
